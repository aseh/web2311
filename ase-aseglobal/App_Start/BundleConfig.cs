﻿using System.Web.Optimization;

namespace web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));
                 


            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            #region elFinder bundles

            bundles.Add(new ScriptBundle("~/Scripts/elfinder").Include(
                "~/Scripts/elfinder/elfinder.full.js",
                "~/Scripts/elfinder/i18n/elfinder.zh_TW.js"
            ));

            // Themes:
            bundles.Add(new StyleBundle("~/Content/elfinder").Include(
                "~/Content/elfinder/css/elfinder.full.css",
                "~/Content/elfinder/themes/windows-10/css/theme.css"));

            #endregion elFinder bundles

            // 使用開發版本的 Modernizr 進行開發並學習。然後，當您
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/themes/base/jquery-ui.min.css"));
        }
    }
}
