﻿using System;
using System.Web;
using System.Web.Mvc;

namespace web
{
  public class FilterConfig
  {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters)
    {
      //filters.Add(new HandleErrorAttribute());
      filters.Add(new CustomRequireHttpsFilter());
    }
    public class CustomRequireHttpsFilter : RequireHttpsAttribute
    {
      protected override void HandleNonHttpsRequest(AuthorizationContext filterContext)
      {
        //TODO: Kenny: Devlopment 中 關閉SSL轉址

        //// If the request does not have a GET or HEAD verb then we throw it away.
        //if (!String.Equals(filterContext.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase)
        //    && !String.Equals(filterContext.HttpContext.Request.HttpMethod, "HEAD", StringComparison.OrdinalIgnoreCase))
        //{
        //  base.HandleNonHttpsRequest(filterContext);
        //}

        //// Update the URL to use https instead of http
        //string url = "https://" + filterContext.HttpContext.Request.Url.Host + filterContext.HttpContext.Request.RawUrl;

        //// If it's localhost we are using IIS Express which has two different ports.
        //// update the url with the IIS Express port for redirect on local machine.
        //if (string.Equals(filterContext.HttpContext.Request.Url.Host, "localhost", StringComparison.OrdinalIgnoreCase))
        //{
        //  url = "https://" + filterContext.HttpContext.Request.Url.Host + ":44356" + filterContext.HttpContext.Request.RawUrl;
        //}

        //// Build the httpContext Response
        //// Setting a status of 301 and then telling the browser what the correct route is.
        //filterContext.HttpContext.Response.StatusCode = 301;
        //filterContext.HttpContext.Response.AppendHeader("Location", url);
        //filterContext.HttpContext.Response.End();
      }
    }
  }

}
