﻿using System.Configuration;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            string default_language = ConfigurationManager.AppSettings["default_language"];
            routes.LowercaseUrls = true;

            routes.MapRoute(
                name: "SESHA",
                url: "{lang}/sip-enabling-smart-hearable-applications/{action}",
                defaults: new { lang = default_language, controller = "SESHA", action = "Index" },
                constraints: new { action = new MyRouteConstraint() }, //Route constraints
                namespaces: new[] { "web.Controllers" }
            );

            #region Blog Route Mapping
            routes.MapRoute(
                name: "BlogAuthor",
                url: "{lang}/blog/author/{Id}",
                defaults: new { lang = default_language, controller = "Blog", action = "Author" },
                namespaces: new[] { "web.Controllers" }
            );

            routes.MapRoute(
                name: "BlogContent",
                url: "{lang}/blog/{Id}",
                defaults: new { lang = default_language, controller = "Blog", action = "Detail" },
                namespaces: new[] { "web.Controllers" }
            );

            routes.MapRoute(
                name: "BlogList",
                url: "{lang}/blog",
                defaults: new { lang = default_language, controller = "Blog", action = "Index" },
                namespaces: new[] { "web.Controllers" }
            );

            #endregion


            //Kenny Proejct D Fix: 將原來的 constraints 移除掉，用不到。
            routes.MapRoute(
                name: "param2",
                url: "{lang}/{controller}/{action}/{param1}/{param2}",
                defaults: new { lang = default_language, controller = "Home", action = "Index", param1 = UrlParameter.Optional, param2 = UrlParameter.Optional },
                namespaces: new[] { "web.Controllers" }
            );


            //string default_language = ConfigurationManager.AppSettings["default_language"];
            //routes.MapRoute(
            //    name: "point",
            //    url: "{lang}/csr/supply_chain_development/conflict_minerals_compliance",
            //    defaults: new { lang = default_language, controller = "csr", action = "supply_chain_development", param1 = "conflict_minerals_compliance" },
            //    namespaces: new[] { "web.Controllers" }
            //);

            //routes.MapRoute(
            //    name: "simple",
            //    url: "{lang}/{controller}",
            //    defaults: new { lang = default_language, controller = "Home", action = "Index" },
            //    constraints: new { action = new MyRouteConstraint() }, //Route constraints
            //    namespaces: new[] { "web.Controllers" }
            //);

            

            routes.MapRoute(
                name: "default",
                url: "{lang}/{controller}/{action}/{param1}/{param2}",
                defaults: new { lang = default_language, controller = "Home", action = "Index", param1 = UrlParameter.Optional, param2 = UrlParameter.Optional },
                constraints: new { action = new MyRouteConstraint() }, //Route constraints
                namespaces: new[] { "web.Controllers" }
            );

            

            //routes.MapRoute(
            //    name: "error",
            //    url: "{lang}/{controller}/{action}/",
            //    defaults: new { lang = default_language, controller = "Errorpage", action = "Index" },
            //    namespaces: new[] { "web.Controllers" }
            //);



            //routes.MapRoute(
            //    name: "point",
            //    url: "{lang}/csr/supply_chain_development/conflict_minerals_compliance",
            //    defaults: new { lang = default_language, controller = "csr", action = "supply_chain_development", param1 = "conflict_minerals_compliance" },
            //    namespaces: new[] { "web.Controllers" }
            //);
            //     routes.MapRoute(
            //      name: "en",
            //      url: default_language + "/{controller}",
            //     defaults: new { lang = default_language, controller = "Home", action = "Index" },
            //    constraints: new { action = new MyRouteConstraint() }, //Route constraints
            //     namespaces: new[] { "web.Controllers" }
            //    );
            //routes.MapRoute(
            //    name: "param1",
            //    url: "{lang}/{controller}/{action}/{param1}",
            //    defaults: new { lang = default_language, controller = "Home", action = "Index", param1 = UrlParameter.Optional },

            //    namespaces: new[] { "web.Controllers" }
            //);


            //routes.MapRoute(
            //  name: "press_room",
            //  url: "{lang}/press_room/content/{param1}",
            //  defaults: new { lang = default_language, controller = "Press_Room", action = "Content", param1 = (UrlParameter.Optional) },
            //  constraints: new { action = new MyRouteConstraint() }, //Route constraints
            //  namespaces: new[] { "web.Controllers" }
            //);

            // routes.MapRoute(
            //    name: "simple",
            //    url: "{lang}/{controller}",
            //    defaults: new { controller = "Home", action = "Index" },
            //    constraints: new { action = new MyRouteConstraint() }, //Route constraints
            //    namespaces: new[] { "web.Controllers" }
            //);



            //routes.MapRoute(
            //    name: "error",
            //    url: "{lang}/{controller}/{action}/",
            //    defaults: new { lang = default_language, controller = "Errorpage", action = "Index" },
            //    namespaces: new[] { "web.Controllers" }
            //);
        }
    }

    public class MyRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var lang = values["lang"] as string;
            var action = values["action"] as string;
            var controller = values["controller"] as string;

            var controllerFullName = string.Format("web.Controllers.{0}Controller", controller);

            var cont = Assembly.GetExecutingAssembly().GetType(controllerFullName);

            return cont != null && cont.GetMethod(action) != null;
        }
    }
}