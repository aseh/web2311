﻿using System.Web.Mvc;

namespace web.Areas.Backend
{
    public class BackendAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Backend";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "backend2",
                "backend/{controller}/{action}/{id}/{param1}/{param2}",
                new { controller = "Login", action = "Index", id = UrlParameter.Optional, param1 = UrlParameter.Optional, param2 = UrlParameter.Optional }
            );
            context.MapRoute(
                "backend_default",
                "backend/{controller}/{action}",
                new { controller = "Login", action = "Index" }
            );

            // Commands
            context.MapRoute("Connector", "ELFinderConnector",
                new { controller = "ELFinderConnector", action = "Main" });

            // Thumbnails
            context.MapRoute("Thumbnauls", "Thumbnails/{target}",
                new { controller = "ELFinderConnector", action = "Thumbnails" });

            // Index view
            context.MapRoute("connector_default", "backend/{controller}/{action}",
                new { controller = "ELFinder", action = "Index" }
            );
        }
    }
}