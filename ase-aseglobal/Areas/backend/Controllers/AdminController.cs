﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Text;
using System.Security.Cryptography;
using web.Models;
using web.Areas.backend.Models;
using web.Common;
using System.Linq;

namespace web.Areas.backend.Controllers
{
    public class AdminController : BackendController
    {
        private admin_emp_model admin_model;
        private admin_emp_group_model group_model;
        private admin_emp_belong_group_model admin_belong_group_model;
        private admin_emp_group_belong_menu_model admin_group_belong_menu_model;
        private web_menu_model menu_model;

        public AdminController()
        {
            admin_model = new admin_emp_model();
            group_model = new admin_emp_group_model();
            admin_belong_group_model = new admin_emp_belong_group_model();
            admin_group_belong_menu_model = new admin_emp_group_belong_menu_model();
            menu_model = new web_menu_model();
        }

        // GET: backend/Admin
        public ActionResult Index()
        {
            return RedirectToAction("AdminList");
        }

        #region Admin
        public ActionResult AdminList()
        {
            Init();

            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "Admin";

            List<admin_emp> list = admin_model.GetList();

            return View(list);
        }

        public ActionResult AdminContent(long param1 = 0)
        {
            Init();

            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Admin";

            admin_emp data;
            if (param1 == 0)
            {
                data = new admin_emp();
                data.ae_launch = 1;
            }
            else
            {
                data = admin_model.GetData(param1);
            }

            List<admin_emp_group> group_list = group_model.GetList();
            ViewBag.group_list = group_list;

            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult AdminContent(FormCollection fc)
        {
            Init();

            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Admin";

            admin_emp data = FcToAdminData(fc);
            if (UpdateAdminData(data))
            {
                UpdateAdminBelongGroupData(fc, data);
                ShowUpdateMessage();
                return RedirectToAction("AdminList");
            }
            else
            {
                ShowUpdateFail();
            }

            List<admin_emp_group> group_list = group_model.GetList();
            ViewBag.group_list = group_list;

            return View(data);
        }

        private admin_emp FcToAdminData(FormCollection fc)
        {
            this.fc = fc;
            admin_emp data = new admin_emp();

            long ae_id = Tool.TryToParseLong(fc["ae_id"]);
            if (ae_id > 0)
            {
                data = admin_model.GetData(ae_id);
                if (data == null || data.ae_id == 0)
                {
                    data.ae_creator = web_admin.ae_id;
                    data.ae_creation_time = DateTime.Now;
                }
            }
            else
            {
                data.ae_creator = web_admin.ae_id;
                data.ae_creation_time = DateTime.Now;
            }

            data.ae_empnm = fc["ae_empnm"].Trim();
            data.ae_name = fc["ae_name"].Trim();
            if (fc["ae_password"] != null && ! string.IsNullOrEmpty(fc["ae_password"].Trim()))
            {
                data.ae_password = fc["ae_password"].Trim();
            }
            data.ae_description = fc["ae_description"].Trim();

            data.ae_mdyer = web_admin.ae_id;
            data.ae_last_update_time = DateTime.Now;
            data.ae_launch = Tool.TryToParseByte(fc["ae_launch"]);

            return data;
        }

        // update admin belong group
        private void UpdateAdminBelongGroupData(FormCollection fc, admin_emp admin)
        {
            admin_emp_belong_group temp_data;

            string selected_group = fc["arr_group[]"].ToString();

            // all group
            List<admin_emp_group> group_list = group_model.GetList();
            // admin belong group
            List<admin_emp_belong_group> admin_belong_group_list = admin_belong_group_model.GetList(admin.ae_id);
            // selected group
            List<string> str_selected_group_list = selected_group.Split(',').ToList();
            List<long> selected_group_list = new List<long>();
            foreach (string str in str_selected_group_list)
            {
                long temp_group_id = Tool.TryToParseLong(str);
                selected_group_list.Add(temp_group_id);
            }

            // check all group
            foreach (admin_emp_group group in group_list)
            {
                // current group is not selected
                if (selected_group_list.Where(x => x == group.aeg_id).Count() == 0)
                {
                    // but in original selected group => delete it
                    if (admin_belong_group_list.Where(x => x.aeg_id == group.aeg_id).Count() > 0)
                    {
                        admin_belong_group_model.DeleteData(admin_belong_group_list.Where(x => x.aeg_id == group.aeg_id).FirstOrDefault().aebg_id);
                    }
                }
                else // current group is selected
                {
                    // but not in original selected group => add it
                    if (admin_belong_group_list.Where(x => x.aeg_id == group.aeg_id).Count() == 0)
                    {
                        temp_data = new admin_emp_belong_group();
                        temp_data.ae_id = admin.ae_id;
                        temp_data.aeg_id = group.aeg_id;
                        admin_belong_group_model.SaveData(temp_data);
                    }
                }
            }
        }

        private bool UpdateAdminData(admin_emp data)
        {
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            #region 驗證
            if (fc["ae_empnm"] != null && string.IsNullOrEmpty(data.ae_empnm))
            {
                is_valid = false;
                error.Add("ae_empnm", "Necessary");
            }
            if (fc["ae_name"] != null)
            {
                if (string.IsNullOrEmpty(data.ae_name))
                {
                    is_valid = false;
                    error.Add("ae_name", "Necessary");
                } else if(!Validator.CheckStringLength(data.ae_name, 2, 50))
                {
                    is_valid = false;
                    error.Add("ae_name", "長度不可小於2，不可大於50");
                }
            }
            if (fc["arr_group[]"] == null)
            {
                is_valid = false;
                error.Add("arr_group", "Necessary");
            }
            if (fc["ae_password"] != null && !string.IsNullOrEmpty(fc["ae_password"].Trim()))
            {
                if (!Validator.CheckStringLength(data.ae_password, 5, 50))
                {
                    is_valid = false;
                    error.Add("ae_password", "長度不可小於5，不可大於50");
                }
            }
            #endregion

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                if (fc["ae_password"] != null && !string.IsNullOrEmpty(fc["ae_password"].Trim()))
                    data.ae_password = CryptographyPassword(data.ae_password);
                affected_row += admin_model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
            }
            return false;
        }

        protected string CryptographyPassword(string password)
        {
            SHA256 sha256 = new SHA256CryptoServiceProvider();//建立一個SHA256
            byte[] source = Encoding.Default.GetBytes(password);//將字串轉為Byte[]
            byte[] crypto = sha256.ComputeHash(source);//進行SHA256加密
            string result = Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串
            return result;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAdminData(long param1)
        {
            if (param1 > 0)
            {
                Init();

                Dictionary<string, object> result = admin_model.DeleteData(param1, Server.MapPath("~/" + website_path));

                int affected_row = (int)result["affected_row"];
                if (affected_row > 0)
                {
                    ShowUpdateMessage();
                }
                else
                {
                    ShowUpdateFail();
                }
                SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "admin_emp", param1);
            }
            return RedirectToAction("AdminList");
        }

        //ajax update launch, return value = 1:enable, 0:disable
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxUpdateAdminLaunch(string param1)
        {
            Init();
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            if (id == 0)
            {
                return Json("");
            }
            else
            {
                return Json(admin_model.ToggleLaunch(id));
            }

        }
        #endregion


        #region Group
        public ActionResult GroupList()
        {
            Init();

            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "Group";

            List<admin_emp_group> list = group_model.GetList();

            return View(list);
        }

        public ActionResult GroupContent(long param1 = 0)
        {
            Init();

            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Group";

            admin_emp_group data;
            if (param1 == 0)
            {
                data = new admin_emp_group();
                data.aeg_launch = 1;
            }
            else
            {
                data = group_model.GetData(param1);
            }

            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult GroupContent(FormCollection fc)
        {
            Init();

            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Group";

            admin_emp_group data = FcToGroupData(fc);
            if (UpdateGroupData(data))
            {
                ShowUpdateMessage();
                return RedirectToAction("GroupList");
            }
            else
            {
                ShowUpdateFail();
            }

            return View(data);
        }

        private admin_emp_group FcToGroupData(FormCollection fc)
        {
            this.fc = fc;
            admin_emp_group data = new admin_emp_group();

            long aeg_id = Tool.TryToParseLong(fc["aeg_id"]);
            if (aeg_id > 0)
            {
                data = group_model.GetData(aeg_id);
                if (data == null || data.aeg_id == 0)
                {
                    data.aeg_creator = web_admin.ae_id;
                    data.aeg_creation_time = DateTime.Now;
                }
            }
            else
            {
                data.aeg_creator = web_admin.ae_id;
                data.aeg_creation_time = DateTime.Now;
            }

            data.aeg_name = fc["aeg_name"].Trim();
            data.aeg_launch = Tool.TryToParseByte(fc["aeg_launch"]);

            data.aeg_mdyer = web_admin.ae_id;
            data.aeg_last_update_time = DateTime.Now;

            return data;
        }

        private bool UpdateGroupData(admin_emp_group data)
        {
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            #region 驗證
            if (fc["aeg_name"] != null && string.IsNullOrEmpty(data.aeg_name))
            {
                is_valid = false;
                error.Add("aeg_name", "Necessary");
            }
            #endregion

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                affected_row += group_model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteGroupData(long param1)
        {
            if (param1 > 0)
            {
                Init();

                Dictionary<string, object> result = group_model.DeleteData(param1, Server.MapPath("~/" + website_path));

                int affected_row = (int)result["affected_row"];
                if (affected_row > 0)
                {
                    ShowUpdateMessage();
                }
                else
                {
                    ShowUpdateFail();
                }
                SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "admin_emp_group", param1);
            }
            return RedirectToAction("GroupList");
        }

        //ajax update launch, return value = 1:enable, 0:disable
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxUpdateGroupLaunch(string param1)
        {
            Init();
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            if (id == 0)
            {
                return Json("");
            }
            else
            {
                return Json(group_model.ToggleLaunch(id));
            }
        }
        #endregion

        public ActionResult PermissionContent(long param1 = 0)
        {
            Init();

            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Group";

            List<web_menu> menu_list = menu_model.GetList();
            ViewBag.menu_list = menu_list;

            List<admin_emp_group_belong_menu> list = admin_group_belong_menu_model.GetList(param1);
            ViewBag.group_id = param1;

            return View(list);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult PermissionContent(FormCollection fc)
        {
            Init();

            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Group";

            if (UpdatePermissionData(fc))
            {
                ShowUpdateMessage();
                return RedirectToAction("GroupList");
            }
            else
            {
                ShowUpdateFail();
            }

            return View();
        }

        public bool UpdatePermissionData(FormCollection fc)
        {
            admin_emp_group_belong_menu temp_data;
            long temp_menu_id;
            bool is_valid = true;

            string selected_menu = fc["wm_id[]"] == null ? "" : fc["wm_id[]"].ToString();
            long group_id = Tool.TryToParseLong(fc["group_id"]);

            // all menu
            List<web_menu> menu_list = menu_model.GetList();
            // group belong menu
            List<admin_emp_group_belong_menu> group_belong_menu_list = admin_group_belong_menu_model.GetList(group_id);
            // selected menu
            List<string> str_selected_menu_list = selected_menu.Split(',').Distinct().ToList();
            List<long> selected_menu_list = new List<long>();
            foreach (string str in str_selected_menu_list)
            {
                temp_menu_id = Tool.TryToParseLong(str);
                selected_menu_list.Add(temp_menu_id);
            }

            try
            {
                if (string.IsNullOrEmpty(selected_menu))
                {
                    foreach (admin_emp_group_belong_menu item in group_belong_menu_list)
                    {
                        admin_group_belong_menu_model.DeleteData(item.aegbm_id);
                    }
                }
                else
                {
                    // check all menu
                    foreach (web_menu menu in menu_list)
                    {
                        // current menu is not selected
                        if (selected_menu_list.Where(x => x == menu.wm_id).Count() == 0)
                        {
                            // but in original selected menu => delete it
                            if (group_belong_menu_list.Where(x => x.wm_id == menu.wm_id).Count() > 0)
                            {
                                admin_group_belong_menu_model.DeleteData(group_belong_menu_list.Where(x => x.wm_id == menu.wm_id).FirstOrDefault().aegbm_id);
                            }
                        }
                        else // current menu is selected
                        {
                            // but not in original selected menu => add it
                            if (group_belong_menu_list.Where(x => x.aeg_id == menu.wm_id).Count() == 0)
                            {
                                temp_data = new admin_emp_group_belong_menu();
                                temp_data.aeg_id = group_id;
                                temp_data.wm_id = menu.wm_id;
                                admin_group_belong_menu_model.SaveData(temp_data);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                is_valid = false;
                throw;
            }
            return is_valid;
        }
    }
}