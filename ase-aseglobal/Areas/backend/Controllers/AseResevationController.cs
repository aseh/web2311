﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace web.Areas.backend.Controllers
{
    public class AseResevationController : BackendController
    {
        // GET: backend/AseResevation
        public ActionResult Index()
        {
            Init();
            return Redirect("/ase_resevation/#/backstage/view");
        }
    }
}