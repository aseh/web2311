﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI.WebControls;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using web.Areas.backend.Models;
using web.Common;
using web.Models;

namespace web.Areas.backend.Controllers
{
    abstract public class BackendController : BaseController
    {
        public String media_bank_path = "public/uploads/";  //媒體庫路徑(如要改為程式讀入，可寫在BackendController建構子)
        public String website_path = "public/media/";

        protected ModelEntities db = new ModelEntities();
        protected FormCollection fc = null;

        protected int admin_id;
        // 登入的管理者
        protected admin_emp web_admin;
        // 當前單元
        protected web_menu node;
        // 管理者權限單元
        protected List<web_menu> auth_web_menu_list;

        public BackendController()
        {
            ViewBag.media_bank_path = media_bank_path;
            ViewBag.website_path = website_path;
        }

        public void Init()
        {
            // 判斷管理者登入 // 判斷當前單元是否為權限的單元
            if (!CheckLogon())
            {
                //停用瀏覽器快取
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.Cache.SetNoStore();

                Response.Cookies.Clear();
                Session.Abandon();
                Response.Redirect(Url.Action("Index", "Login"), true);
            }
            else if (!CheckAuthority())
            {
                //停用瀏覽器快取
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.Cache.SetNoStore();

                Response.Cookies.Clear();
                Session.Abandon();
                Response.Redirect(Url.Action("Index", "Login") + "?r=" + Guid.NewGuid(), true);
            }
            else
            {
                admin_id = Convert.ToInt32(Session["adminId"]);
                // 取得管理者資訊
                web_admin = getAdmin(admin_id);
                ViewBag.web_admin = web_admin;
                // 取得授權的單元列
                auth_web_menu_list = getAdminMenuList(admin_id);
                List<web_menu> unit_menu_list = getAdminMenuList(admin_id);
                ViewBag.auth_web_menu_list = auth_web_menu_list;
                ViewBag.unit_menu_list = unit_menu_list;
                //取得實體路徑&虛擬路徑
                //---------------------------------------------------------------------------------------
                //Media Bank 實體路徑
                ViewBag.media_bank_physical_path = Server.MapPath(Url.Content("~/" + media_bank_path)).Replace("\\", "\\\\");
                //Media Bank 虛擬路徑
                ViewBag.media_bank_virtual_path = Url.Content("~/" + media_bank_path);
                //Media Bank 存檔後的虛擬路徑
                ViewBag.website_virtual_path = Url.Content("~/" + website_path);
                //---------------------------------------------------------------------------------------
                ViewBag.content_header_title = this.node.wm_title;
                if (this.node.web_menu2 != null)
                {
                    ViewBag.content_header_title = this.node.web_menu2.wm_title;
                }
                ViewBag.node = this.node;
            }
        }

        //檢查登入，未登入會清空Session
        private bool CheckLogon()
        {
            if (Session["adminId"] == null)
            {
                return false;
            }
            return true;
        }

        //檢查權限
        private bool CheckAuthority()
        {
            if (Session["adminId"] == null)
            {
                return false;
            }

            int adminId = Convert.ToInt32(Session["adminId"]);
            // 取得當前單元
            web_menu_model menu_model = new web_menu_model();
            string url_id = RouteData.Values["id"] == null ? "" : RouteData.Values["id"].ToString();
            node = menu_model.getCurrentMenuData(url_id, RouteData.Values["controller"].ToString());
            // 檢查管理者目前單元是否有權限
            List<web_menu> auth_menu_list = getAdminMenuList(adminId);
            if (auth_menu_list.Where(x => x.wm_id == node.wm_id).Count() > 0)
            {
                return true;
            }
            return false;
        }

        public admin_emp getAdmin(int adminId)
        {
            return db.admin_emp.Find(adminId);
        }

        public List<web_menu> getAdminMenuList(int adminId)
        {
            // 會員所屬權限群組
            List<admin_emp_belong_group> admin_group_belong_list = db.admin_emp_belong_group.Where(x => x.ae_id == adminId).ToList();
            List<long> admin_group_id_list = new List<long>();
            foreach (admin_emp_belong_group item in admin_group_belong_list)
            {
                admin_group_id_list.Add(item.aeg_id);
            }
            List<admin_emp_group_belong_menu> admin_menu_belong_list = db.admin_emp_group_belong_menu.Where(x => admin_group_id_list.Contains(x.aeg_id)).ToList();

            // 會員所屬權限單元
            List<long> admin_menu_id_list = new List<long>();
            foreach (admin_emp_group_belong_menu item in admin_menu_belong_list)
            {
                admin_menu_id_list.Add(item.wm_id);
            }
            auth_web_menu_list = db.web_menu.Where(x => admin_menu_id_list.Contains(x.wm_id) && x.wm_launch == 1 && x.wm_display == 1).ToList();

            return auth_web_menu_list;
        }

        // 設定登入權限
        public void SetLoginSession(admin_emp admin)
        {
            // 更新登入時間
            admin_emp_model model = new admin_emp_model();
            admin.ae_login_time = DateTime.Now;
            model.SaveData(admin);

            // 會員所屬權限群組
            List<admin_emp_belong_group> admin_group_belong_list = db.admin_emp_belong_group.Where(x => x.ae_id == admin.ae_id).ToList();
            List<long> admin_group_id_list = new List<long>();
            foreach (admin_emp_belong_group item in admin_group_belong_list)
            {
                admin_group_id_list.Add(item.aeg_id);
            }
            List<admin_emp_group_belong_menu> admin_menu_belong_list = db.admin_emp_group_belong_menu.Where(x => admin_group_id_list.Contains(x.aeg_id)).ToList();

            // 會員所屬權限單元
            List<long> admin_menu_id_list = new List<long>();
            foreach (admin_emp_group_belong_menu item in admin_menu_belong_list)
            {
                admin_menu_id_list.Add(item.wm_id);
            }
            auth_web_menu_list = db.web_menu.Where(x => admin_menu_id_list.Contains(x.wm_id) && x.wm_launch == 1 && x.wm_display == 1).ToList();

            ////設定登入者Session
            //Response.Cookies.Clear();
            Session.RemoveAll();
            Session["adminId"] = admin.ae_id;
            //Session["admin"] = admin;
            //Session["authMenuList"] = auth_web_menu_list;
        }

        public void SecurityLog(string category, string content, string table_name = "", long table_id = 0)
        {
            system_log_model model = new system_log_model();
            switch (category)
            {
                case "login":
                    content = content + " has login.";
                    break;
                case "loginFail":
                    break;
                default:
                    content = content + " " + category + " a row in the table:「" + table_name + "」, ID: " + table_id;
                    break;
            }

            admin_emp admin = (admin_emp)Session["admin"];
            if (admin == null || admin.ae_id == 0)
            {
                model.addData(0, category, content);
            }
            else
            {
                model.addData(admin.ae_id, category, content);
            }
        }

        public HttpPostedFileBase getFile(string name)
        {
            if (this.Request.Files[name].FileName != "")
            {
                return this.Request.Files[name];
            }
            else {
                return null;
            }
            
        }

        public string moveFile(HttpPostedFileBase fromFile)
        {
            string oldFilePath = Path.GetFileName(fromFile.FileName);
            FileInfo fi = new FileInfo(oldFilePath);
            Guid guid = Guid.NewGuid();
            //"~/" + this.website_path + "/" + newImgName
            string toPath = String.Format("/public/uploads/{0}{1}", Tool.Md5Convert(String.Concat(DateTime.Now.ToString("yyyyMMddHHmmss"), guid.ToString())), fi.Extension);
            fromFile.SaveAs(Server.MapPath(toPath));
            return toPath;
        }

        public void moveFile(string fromPath, string toPath)
        {
           if( new FileInfo(fromPath).Exists )
           {
                System.IO.File.Move(Server.MapPath(fromPath), Server.MapPath(toPath));
            }
        }

        public string resizeImage(string filePath, int maxWidth, int maxHeight)
        {
            Guid guid = Guid.NewGuid();
            FileInfo fi = new FileInfo(filePath);
            string toPath = String.Format("/public/uploads/{0}{1}", Tool.Md5Convert(String.Concat(DateTime.Now.ToString("yyyyMMddHHmmss"), guid.ToString())), fi.Extension);
            CropImageUtility ciu = new CropImageUtility(Server.MapPath(filePath), Server.MapPath(toPath), Server.MapPath(filePath));
            ciu.ImageCrop(null, null, null, null, maxWidth, maxHeight);//copy & delete old file [小圖, 用在 List]
            return toPath;
        }

        //圖片處理
        public string dealImage(string filename = "img_filename", int? max_width = null, int? max_height = null, string preFileName = "ase_")
        {
            string old_filename = "old_" + filename;

            string img_filename = "";

            if (fc[filename] == null || fc[filename] == string.Empty)
            {
                CropImageUtility iu = new CropImageUtility(Server.MapPath(fc[filename]), null, Server.MapPath(fc[old_filename]));
                iu.Delete();
                return img_filename;
            }

            

            System.IO.FileInfo fi = new System.IO.FileInfo(Server.MapPath(fc[filename]));

            

            if (fi.Exists)
            {
                
                if (Server.MapPath(fc[filename]) != Server.MapPath(fc[old_filename]))
                {
                    
                    
                    Guid guid = Guid.NewGuid();
                    //"~/" + this.website_path + "/" + newImgName
                    string newImgName = preFileName + Tool.Md5Convert(String.Concat(DateTime.Now.ToString("yyyyMMddHHmmss"), guid.ToString()));
                    System.IO.FileInfo fi2 = new System.IO.FileInfo(Server.MapPath(Url.Content(String.Format("~/{0}/{1}{2}", this.website_path, newImgName, fi.Extension))));
                    while (fi2.Exists)
                    {
                        newImgName = preFileName + Tool.Md5Convert(String.Concat(DateTime.Now.ToString("yyyyMMddHHmmss"), guid.ToString()));
                        fi2 = new System.IO.FileInfo(Server.MapPath(Url.Content(String.Format("~/{0}/{1}{2}", this.website_path, newImgName, fi.Extension))));
                    }
                    //string img_filename_big = "B_" + newImgName + fi.Extension.ToLower();
                    img_filename = newImgName + fi.Extension.ToLower();
                    if (img_filename.Contains(".mp4"))
                    {
                        System.IO.File.Copy(fi.FullName, fi2.FullName);
                    } else
                    {
                        CropImageUtility ciu = new CropImageUtility(Server.MapPath(fc[filename]), Server.MapPath("~/" + website_path + img_filename), Server.MapPath(fc[old_filename]));
                        ciu.ImageCrop(null, null, null, null, max_width, max_height);//copy & delete old file [小圖, 用在 List]
                    }
                    
                }
                else
                {
                    img_filename = fi.Name;
                }
            }
            return img_filename;
        }

        /// <summary>選擇新檔時，刪除舊檔、複製新檔，並回傳新檔案名稱；若檔案未變更則回傳原檔案名稱</summary>
        /// <param name="filename">FormCollection的檔案name</param>
        /// <param name="preFileName">新檔案加入前置詞</param>
        /// <returns>新的檔案名稱</returns>
        /*
         * (1) 判斷是否包含檔案的物件
         * (2) 判斷「選擇的檔案」與「原選擇的檔案」是否相同
         * (2.1) 相同，不處理，回傳原選擇檔案的名稱
         * (2.2) 不同，進入步驟(3)
         * (3) 刪除「原選擇的檔案」
         * (4) 複製「選擇的檔案」，透過Guid產生新檔案名稱，並判斷是否新檔案名稱已存在於資料夾中，若存在則重新產生新檔案名稱 loop
         * (5) 回傳新檔案名稱
         */
        public string dealFile(string filename = "filename", string preFileName = "ase_")
        {
            string str_filename = ""; // 選擇的檔案名稱
            string str_old_filename = ""; // 原選擇的檔案名稱

            string old_file_path = ""; // 原選擇的檔案路徑

            string destination_path = ""; // 複製後，新檔案經放置的路徑
            string new_file_path = ""; // 選擇的檔案路徑
            string new_file_name = ""; // 新檔案名稱

            Random rd = new Random();

            // (1) 判斷是否包含檔案的物件
            if (fc[filename] != null && fc["old_" + filename] != null)
            {
                // 選擇的檔案名稱
                str_filename = fc[filename].Replace("/" + media_bank_path, "").Replace("/" + website_path, "");
                // 原選擇的檔案，若一開始還沒選過，則資料為空
                str_old_filename = fc["old_" + filename].Replace("/" + media_bank_path, "").Replace("/" + website_path, "");

                // (2) 判斷「選擇的檔案」與「原選擇的檔案」是否相同
                if (str_filename != str_old_filename)
                {
                    #region (3) 刪除「原選擇的檔案」
                    old_file_path = Server.MapPath(fc["old_" + filename]);
                    if (System.IO.File.Exists(old_file_path))
                    {
                        System.IO.File.Delete(old_file_path);
                    }
                    #endregion

                    #region (4) 複製「選擇的檔案」，透過Guid產生新檔案名稱
                    FileInfo fi = new FileInfo(Server.MapPath(fc[filename]));
                    if (fi.Exists)
                    {
                        new_file_path = Server.MapPath(fc[filename]); // 選擇檔案的路徑
                        new_file_name = preFileName + Guid.NewGuid() + Path.GetExtension(new_file_path); // 新的檔案名稱
                        FileInfo fi_copy = new FileInfo(Server.MapPath(Url.Content("~/" + this.website_path + new_file_name)));
                        while (fi_copy.Exists)
                        {
                            new_file_name = preFileName + Guid.NewGuid() + Path.GetExtension(new_file_path);
                            fi_copy = new FileInfo(Server.MapPath(Url.Content("~/" + this.website_path + new_file_name)));
                        }

                        destination_path = Server.MapPath(Url.Content("~/" + this.website_path)) + new_file_name; // 複製檔的儲存路徑

                        // 執行複製
                        System.IO.File.Copy(new_file_path, destination_path, true);
                    }
                    #endregion
                }
                else
                {
                    new_file_name = str_filename;
                }
            }
            return new_file_name;
        }



        public MemoryStream CreateExcel(int RowCount, Dictionary<string, List<string>> Columns, List<int> Widths)
        {
            IWorkbook Workbook = new XSSFWorkbook();
            ISheet Sheet = Workbook.CreateSheet("Sheet1");

            var style = Workbook.CreateCellStyle();
            style.WrapText = true;
            

            var HeaderIndex = 0;
            var HeaderRow = Sheet.CreateRow(0);
            foreach (var Column in Columns)
            {
                var HeaderCell = HeaderRow.CreateCell(HeaderIndex);
                HeaderCell.SetCellValue(Column.Key);
                HeaderCell.CellStyle = style;
                HeaderIndex++;
            }

            for (var i = 1; i <= RowCount; i++)
            {
                var Row = Sheet.CreateRow(i);
                var j = 0;
                foreach (var Column in Columns)
                {
                    var Cell = Row.CreateCell(j);
                    Cell.SetCellValue(Column.Value[i - 1]);
                    Cell.CellStyle = style;
                    j++;
                }
            }

            int ColumnIndex = 0;
            foreach (var Column in Columns)
            {
                Sheet.SetColumnWidth(ColumnIndex, 16 * 256);
                //Sheet.AutoSizeColumn(ColumnIndex);
                ColumnIndex++;
            }

            

            var Stream = new MemoryStream();

            Workbook.Write(Stream);

            return new MemoryStream(Stream.ToArray());

        }

        public MemoryStream CreateVerticalExcel(int RowCount, Dictionary<string, List<string>> Columns, int freezeCol = 0, int freezeRow = 0)
        {
            IWorkbook Workbook = new XSSFWorkbook();
            ISheet Sheet = Workbook.CreateSheet("Sheet1");

            var style = Workbook.CreateCellStyle();
            style.WrapText = true;
            style.VerticalAlignment = VerticalAlignment.Top;

            int RowIndex = 0;
            foreach ( var item in Columns )
            {
                var Row = Sheet.CreateRow(RowIndex);
                var HeaderCell = Row.CreateCell(0);
                HeaderCell.SetCellValue(item.Key);
                for ( var ColumnIndex = 0; ColumnIndex < RowCount; ColumnIndex ++)
                {
                    var Cell = Row.CreateCell(ColumnIndex + 1);
                    Cell.SetCellValue(item.Value[ColumnIndex]);
                    Cell.CellStyle = style;
                }
                
                RowIndex++;
            }

            Sheet.SetColumnWidth(0, 20 * 256);
            for (var ColumnIndex = 0; ColumnIndex < RowCount; ColumnIndex ++ ) 
            {
                Sheet.SetColumnWidth(ColumnIndex + 1, 40 * 256);
            }

            Sheet.CreateFreezePane(freezeCol, freezeRow);

            var Stream = new MemoryStream();

            Workbook.Write(Stream);

            return new MemoryStream(Stream.ToArray());
        }
    }
}