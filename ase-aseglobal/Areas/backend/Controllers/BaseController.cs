﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Collections;
using System.Web.Caching;

namespace web.Areas.backend.Controllers
{
    public class BaseController : Controller
    {
        public String action { set; get; }

        //---------------------------------------------------------------------------------------------------------------
        // Base
        //---------------------------------------------------------------------------------------------------------------
        #region
        /// <summary>
        /// 設定Action
        /// </summary>
        /// <param name="action"></param>
        protected void SetAction(String action)
        {
            this.action = action;

            ViewData["action"] = this.action;
        }

        public Dictionary<string, int> GetRatioSize(int width, int height)
        {
            Dictionary<string, int> ratio;

            ratio = new Dictionary<string, int>();
            ratio.Add("width", width);
            ratio.Add("height", height);
            return ratio;
        }

        public string GetActionUrl(string action)
        {
            return GetActionUrl(null, null, null, action);
        }

        public string GetActionUrl(string lang, string controller, string web_menu_id, string action)
        {
            lang = lang ?? RouteData.Values["lang"].ToString();
            controller = controller ?? RouteData.Values["controller"].ToString(); ;
            web_menu_id = web_menu_id ?? RouteData.Values["id"].ToString();
            action = action ?? RouteData.Values["action"].ToString();

            if (Url == null)
            {
                return "/backend/" + lang + "/" + controller + "/" + web_menu_id + "/" + action;
            }
            else
            {
                return Url.Content("~/backend/" + lang + "/" + controller + "/" + web_menu_id + "/" + action);
            }
            
        }
        #endregion

        //---------------------------------------------------------------------------------------------------------------
        // Show Message
        //---------------------------------------------------------------------------------------------------------------
        #region
        public void ShowUpdateMessage(string msg = "Update Success!!")
        {
            ShowMessage(msg, "success");
        }

        public void ShowUpdateFail(string msg = "Update Failed!!")
        {
            ShowMessage(msg, "error");
        }

        public void ShowMessage(string message, string type)
        {
            string msg  = "<script>swal({title:'"+ message + "', timer: 3000, type:'"+ type + "'})</script>";
            TempData["mesage"] = msg;
        }
        #endregion

        //---------------------------------------------------------------------------------------------------------------
        // Cache
        //---------------------------------------------------------------------------------------------------------------
        #region
        /// <summary>
        /// 取得cache資料
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public object GetCacheData(string cacheKey)
        {
            var cache = HttpContext.Cache;
            var content = cache.Get(cacheKey);
            return content;
        }
        /// <summary>
        /// 設定cache
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="data"></param>
        /// <param name="seconds"></param>
        public void SetCacheData(string cacheKey, object data, int seconds = 60)
        {
            var cache = HttpContext.Cache;
            cache.Insert(cacheKey, data, null, DateTime.Now.AddSeconds(seconds), Cache.NoSlidingExpiration);
        }

        /// <summary>
        /// 刪除cache 資料
        /// </summary>
        /// <param name="cacheKey"></param>
        public void RemoveCacheData(string cacheKey)
        {
            var cache = HttpContext.Cache;
            cache.Remove(cacheKey);
        }

        /// <summary>
        /// 刪除所有cache
        /// </summary>
        public void RemoveAllCache()
        {
            foreach (DictionaryEntry dEntry in HttpContext.Cache)
            {
                HttpContext.Cache.Remove(dEntry.Key.ToString());
            }
        }
        #endregion
        
        public void AddJs(string js_path = "", bool default_path = true)
        {
            if (default_path)
            {
                ViewBag.js += "<script type=\"text/javascript\" src=\"" + Url.Content("~/public/frontend/js/" + js_path) + "\"></script>\r";
            }
            else
            {
                ViewBag.js += "<script type=\"text/javascript\" src=\"" + js_path + "\"></script>\r";
            }
        }

        public void AddCss(string css_path = "")
        {
            ViewBag.css += "<link href=\"" + Url.Content("~/public/frontend/css/" + css_path) + "\" rel=\"stylesheet\" type=\"text/css\" />\r";
        }
    }
}
