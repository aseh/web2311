﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.UI;
using web.Areas.backend.Models;
using web.Common;
using web.Models;

namespace web.Areas.backend.Controllers
{
    public class BlogController : BackendController
    {
        private blog_model model;
        protected List<Dictionary<string, int>> ratio = new List<Dictionary<string, int>>();

        public BlogController()
        {
            model = new blog_model();
        }

        // GET: backend/Blog
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        //ddlAuthor=0&ddlType=0&keyword=
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult List(int ddlAuthor = 0, int ddlType = 0, string keyword = "", int sort = 0)
        {
            Init();

            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "Blog";

            List<blog_dtl> list = model.GetDtlList(ddlAuthor,ddlType, keyword, sort);
            //add author and type list item
            blog_author_model amodel = new blog_author_model();
            blog_type_model tmodel = new blog_type_model();
            List<SelectListItem> Authorlist = amodel.GetAuthorList("ch", 0);
            List<SelectListItem> Typelist = tmodel.GetTypeList("ch", 0);
            SelectListItem tl = new SelectListItem();tl.Text = "選擇分類";tl.Value = "0";
            Typelist.Insert(0, tl);
            //set select ddl
            if (ddlAuthor != 0)
            {
                foreach(var li in Authorlist)
                {
                    if (li.Value == ddlAuthor.ToString())
                    {
                        li.Selected = true;
                        break;
                    }
                }
            }
            if (ddlType != 0)
            {
                foreach (var li in Typelist)
                {
                    if (li.Value == ddlType.ToString())
                    {
                        li.Selected = true;
                        break;
                    }
                }
            }
            //
            ViewBag.Authorlist = Authorlist;
            ViewBag.Typelist = Typelist;
            ViewBag.keyword = keyword;
            return View(list);
        }

        public ActionResult Messagelist(long param1 = 0 )
        {
            Init();

            ViewBag.content_header_sub_title = "留言管理";
            ViewBag.current_tab = "Blog";

            List<blog_message> list = model.GetData(param1).blog_message.ToList();
            //ViewBag.type = type;

            return View(list);
        }

        public ActionResult Content(long param1 = 0, string lang = "ch")
        {
            Init();
            TempData["mesage"] = "";
            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Blog";
            ViewBag.lang = lang;
            long authorid = 0;
            long typeid = 0;
            blog data;
            if (param1 == 0)
            {
                data = new blog();
                data.apply_date = DateTime.Today;
                blog_dtl dtl = new blog_dtl();
                dtl.lang = "en";
                data.blog_dtl.Add(dtl);
                dtl = new blog_dtl();
                dtl.lang = "ch";
                data.blog_dtl.Add(dtl);
            }
            else
            {
                data = model.GetData(param1);
                authorid = data.authorid;
                typeid = data.typeid;
            }
            //add author and type list item
            blog_author_model amodel = new blog_author_model();
            blog_type_model tmodel = new blog_type_model();
            List<SelectListItem> Authorlist = amodel.GetAuthorList(lang, authorid);
            List<SelectListItem> Typelist = tmodel.GetTypeList(lang, typeid);
            ViewBag.Authorlist = Authorlist;
            ViewBag.Typelist = Typelist;

            return View(data);
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Content(FormCollection fc,string lang="ch")
        {
            Init();

            blog data = FcToData(fc);
            if (UpdateData(data))
            {
                ShowUpdateMessage();
                return RedirectToAction("List", new { id = node.wm_url_id });
            }
            else
            {
                ShowUpdateFail();
                this.ratio.Add(this.GetRatioSize(1280, 1280));
                ViewBag.ratio = ratio;

                //add author and type list item
                blog_author_model amodel = new blog_author_model();
                blog_type_model tmodel = new blog_type_model();
                List<SelectListItem> Authorlist = amodel.GetAuthorList(lang, 0);
                List<SelectListItem> Typelist = tmodel.GetTypeList(lang, 0);
                ViewBag.Authorlist = Authorlist;
                ViewBag.Typelist = Typelist;
                return View(data);
            }

            
        }

        private blog FcToData(FormCollection fc)
        {
            this.fc = fc;
            bool isadd = false;
            blog data = new blog();

            long id = Tool.TryToParseLong(fc["id"]);
            long authorid = Tool.TryToParseLong(fc["authorid"]);
            long typeid = Tool.TryToParseLong(fc["typeid"]);
            int toporder = Tool.TryToParseInt(fc["toporder"]);
            byte allowmsg= Tool.TryToParseByte(fc["allowmsg"]);
            byte launch = Tool.TryToParseByte(fc["launch"]);
            byte isseo = Tool.TryToParseByte(fc["isseo"]);
            string urlid = fc["urlid"];
            
            if (id > 0)
            {
                data = model.GetData(id);
                if (data == null || data.id == 0)
                {
                    data.create_date = DateTime.Now;
                    isadd = true;
                }
            }
            else
            {
                data.create_date = DateTime.Now;
                isadd = true;
            }

            System.Web.HttpPostedFileBase file = getFile("picurl");
            if (file != null)
            {
                data.picurl = resizeImage(moveFile(file), 330, 240);
                data.pc_picurl = resizeImage(moveFile(file), 1920, 900);
                //
            }
            else
            {
                if (fc["tmppicurl"]!="" && fc["tmppicurl"] != null)
                {
                    data.picurl = fc["tmppicurl"];
                    data.pc_picurl = fc["tmppcpicurl"];
                }
            }
            System.Web.HttpPostedFileBase seofile = getFile("seopicurl");
            if (seofile != null)
            {
                data.seopicurl = moveFile(seofile);
            }
            else
            {
                if (fc["tmpseopicurl"] != "" && fc["tmpseopicurl"] != null)
                {
                    data.seopicurl = fc["tmpseopicurl"];
                }
            }
            data.authorid = authorid;
            data.typeid = typeid;
            data.toporder = toporder;
            data.allowmsg = allowmsg;
            data.launch = launch;
            data.isseo = isseo;
            data.urlid = urlid;
            data.update_date = DateTime.Now;
            if (fc["apply_date"]!=null && fc["apply_date"] != "")
            {
                try
                {
                    data.apply_date = Convert.ToDateTime(fc["apply_date"]);
                }
                catch (Exception ex)
                {

                }
                
            }
            else
            {
                data.apply_date = null;
            }

            if (isadd)
            {
                blog_dtl dtl = new blog_dtl();
                dtl.create_date = DateTime.Now;
                dtl = SetDetail(dtl, fc, "ch");
                data.blog_dtl.Add(dtl);
                dtl = new blog_dtl();
                dtl.create_date = DateTime.Now;
                dtl = SetDetail(dtl, fc, "en");
                data.blog_dtl.Add(dtl);

            }
            else
            {
                foreach (var dtl in data.blog_dtl)
                {
                    dtl.update_date = DateTime.Now;
                    SetDetail(dtl, fc, dtl.lang);
                }
            }

            return data;
        }

        private blog_dtl SetDetail(blog_dtl dtl, FormCollection fc, string lang)
        {
            dtl.lang = lang;
            dtl.title = fc[lang + "_title"];
            dtl.contents = fc[lang + "_content"];
            dtl.seodescription = fc[lang + "_seo"];
            dtl.contenttext = dtl.contents.StripHTML();
            return dtl;
        }

        private bool UpdateData(blog data)
        {
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            if (data.authorid == 0)
            {
                blog_author_model amodel = new blog_author_model();
                blog_author_dtl dftnone = amodel.DefaultNone();
                data.authorid = dftnone.blog_author.id; //未設定作者時  設為"(未設定)"
                //is_valid = false;
                //error.Add("authorid", "Necessary");
            }
            if (data.picurl == null || string.IsNullOrEmpty(data.picurl))
            {
                is_valid = false;
                error.Add("tmppicurl", "Neccesary"); //"必須輸入照片");
            }
            if (data.urlid == null || string.IsNullOrEmpty(data.urlid))
            {
                is_valid = false;
                error.Add("urlid", "Neccesary"); 
            }
            else //驗證urlid重複
            {
                var chkblog = model.GetDataByUrlid(data.urlid);
                if (chkblog!=null && chkblog.id != data.id)
                {
                    is_valid = false;
                    error.Add("urlid", "urlid duplication");
                }
            }
            if (data.isseo==1 && (data.seopicurl==null || string.IsNullOrEmpty(data.seopicurl)))
            {
                is_valid = false;
                error.Add("tmpseopicurl", "Neccesary");
            }
            if (data.isseo == 1)
            {
                if (!(data.seopicurl == null || string.IsNullOrEmpty(data.seopicurl)))
                {
                    string path = Server.MapPath(data.seopicurl);
                    try
                    {
                        Image image = Image.FromFile(path);
                        if (image != null)
                        {
                            string errmsg = "";
                            //檢查畫素不可小於200px
                            if (image.Height < 200 || image.Width < 200)
                            {
                                errmsg = "長寬畫素不可低於200";
                            }
                            //檢查長寬比例
                            //float ratio = (float)image.Height / (float)image.Width;
                            //if (ratio < 0.9 || ratio > 1.1)
                            //{
                            //    errmsg += ",長寬比例請用1:1";

                            //}
                            if (errmsg != "")
                            {
                                is_valid = false;
                                error.Add("tmpseopicurl", errmsg);
                            }
                        }
                    }
                    catch(Exception ex)
                    {

                    }
                  
                }
             
            }

            if (data.apply_date == null)
            {
                is_valid = false;
                error.Add("apply_date", "Neccesary");
            }

            //檢查中文內容是否有輸入?
            bool chkcontent = false; //檢查是否至少輸入中文或英文
            var dtl = data.blog_dtl.First(u => u.lang == "ch");
            var content = dtl.contents;
            if (!string.IsNullOrEmpty(content))
            {
                chkcontent = true;
                //必須輸入title
                if (string.IsNullOrEmpty(dtl.title))
                {
                    is_valid = false;
                    error.Add("ch_title", "Neccesary"); //"必須輸入"+langstr+"標題");
                }
                else
                {
                    //驗證標題重複
                    blog_dtl chkdtl = model.GetDtlDataByTitle(dtl.title, dtl.lang);
                    if (chkdtl != null)
                    {
                        if (chkdtl.id != dtl.id)
                        {
                            is_valid = false;
                            error.Add(dtl.lang + "_title", "title duplication");
                        }
                    }
                }
                //檢查seo
                if (data.isseo == 1 && (dtl.seodescription == null || string.IsNullOrEmpty(dtl.seodescription)))
                {
                    is_valid = false;
                    error.Add("ch_seo", "Neccesary");
                }
            }
            //檢查英文內容是否有輸入?
            dtl = data.blog_dtl.First(u => u.lang == "en");
            content = data.blog_dtl.First(u => u.lang == "en").contents;
            if (!string.IsNullOrEmpty(content))
            {
                chkcontent = true;
                //必須輸入title
                if (string.IsNullOrEmpty(dtl.title))
                {
                    is_valid = false;
                    error.Add("en_title", "Neccesary"); //"必須輸入"+langstr+"標題");
                }
                else
                {
                    //驗證標題重複
                    blog_dtl chkdtl = model.GetDtlDataByTitle(dtl.title, dtl.lang);
                    if (chkdtl != null)
                    {
                        if (chkdtl.id != dtl.id)
                        {
                            is_valid = false;
                            error.Add(dtl.lang + "_title", "title duplication");
                        }
                    }
                }
                //檢查seo
                if (data.isseo == 1 && (dtl.seodescription == null || string.IsNullOrEmpty(dtl.seodescription)))
                {
                    is_valid = false;
                    error.Add("en_seo", "Neccesary");
                }
            }
            if (!chkcontent)
            {
                is_valid = false;
                error.Add("ch_content", "Neccesary");
                error.Add("en_content", "Neccesary");
            }

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                affected_row += model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteData(string param1)
        {
            Init();
            string errmsg = "";
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            blog chk = model.GetData(id);
            errmsg = model.DeleteData(chk.id);

            if (errmsg == "")
            {
                ShowUpdateMessage();
            }
            else
            {
                ShowUpdateFail(errmsg);
            }
            SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "blog", id);

            return RedirectToAction("List");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxUpdateLaunch(string param1)
        {
            Init();
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            if (id == 0)
            {
                return Json("");
            }
            else
            {
                return Json(model.UpdateAllowmessage(id));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxUpdateLaunch1(string param1)
        {
            Init();
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            if (id == 0)
            {
                return Json("");
            }
            else
            {
                return Json(model.UpdateLauch(id));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxCheckTopOrder(string param1,int toporder)
        {
            Init();
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            return Json(model.ChkDupToporder(id, toporder));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteMessage(string param1)
        {
            Init();
            string errmsg = "";
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            blog_message chk = model.GetMessageById(id);
            long blogid = chk.blogid;
            BaseResponse res = model.DeleteMessage(id);

            if (res.code == 0)
            {
                ShowUpdateMessage();
            }
            else
            {
                ShowUpdateFail(errmsg);
            }
            SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "blog_message", id);

            return Json(res);
            //return RedirectToAction("Messagelist/blogs/"+blogid);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxPostReply(string param1,string reply)
        {
            Init();
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            
            if (id == 0)
            {
                return Json("");
            }
            else
            {
                return Json(model.UpdateReply(id, reply, web_admin.ae_id, web_admin.ae_empnm));
            }
        }

        [HttpGet]
        public ActionResult ScheduleJob()
        {
            BaseResponse res = new BaseResponse();
            model.UpdateBlogLaunch();
            return Json(res);
        }
            

    }

    public static class StringHelpers
    {
        public static string StripHTML(this string HTMLText)
        {
            var reg = new Regex("<[^>]+>", RegexOptions.IgnoreCase);
            var Result =  reg.Replace(HTMLText, "");
            Result = Result.Replace("&nbsp;", "");
            return Result;
        }
    }
}