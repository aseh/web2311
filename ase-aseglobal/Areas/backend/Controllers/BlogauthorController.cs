﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using web.Areas.backend.Models;
using web.Common;
using web.Models;

namespace web.Areas.backend.Controllers
{
    public class BlogauthorController : BackendController
    {
        private blog_author_model model;
        protected List<Dictionary<string, int>> ratio = new List<Dictionary<string, int>>();


        public BlogauthorController()
        {
            model = new blog_author_model();
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult List(int type = 0, string keyword = "", int sort = 0)
        {
            Init();

            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "Blogauthor";

            List<blog_author_dtl> list = model.GetDtlList(keyword, sort);//.Where(u=>u.name!="(none)" && u.name!="(未設定)").ToList();
            //ViewBag.type = type;

            return View(list);
        }

        public ActionResult Content(long param1 = 0)
        {
            Init();
            TempData["mesage"] = "";
            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Blogauthor";
            //ViewBag.lang = lang;

            blog_author data;
            if (param1 == 0)
            {
                data = new blog_author();
                blog_author_dtl dtl = new blog_author_dtl();
                dtl.lang = "en";
                data.blog_author_dtl.Add(dtl);
                dtl = new blog_author_dtl();
                dtl.lang = "ch";
                data.blog_author_dtl.Add(dtl);
            }
            else
            {
                data = model.GetData(param1);//.GetDtlData(param1).blog_author;
            }

            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Content(FormCollection fc)
        {
            Init();

            blog_author data = FcToData(fc);
            if (UpdateData(data))
            {
                ShowUpdateMessage();
                return RedirectToAction("List", new { id = node.wm_url_id });
            }
            else
            {
                ShowUpdateFail();
                this.ratio.Add(this.GetRatioSize(1280, 1280));
                ViewBag.ratio = ratio;
                return View(data);
            }

            //return RedirectToAction("List", new { id = node.wm_url_id });

        }

        private blog_author FcToData(FormCollection fc)
        {
            this.fc = fc;
            bool isadd = false; 
            blog_author data = new blog_author();

            long id = Tool.TryToParseLong(fc["id"]);
            if (id > 0)
            {
                data = model.GetData(id);
                if (data == null || data.id == 0)
                {
                    data.create_date = DateTime.Now;
                    isadd = true;
                }
            }
            else
            {
                data.create_date = DateTime.Now;
                isadd = true;
            }
            //string img_filename = fc["img_filename"].Replace("/" + media_bank_path, "").Replace("/" + website_path, "");
            System.Web.HttpPostedFileBase file = getFile("picurl");
            if (file != null)
            {
                data.picurl = resizeImage(moveFile(file), 256, 256);
            }
            else
            {
                if (fc["tmppicurl"] != "" && fc["tmppicurl"] != null)
                {
                    data.picurl = fc["tmppicurl"];
                }
            }
            data.update_date = DateTime.Now;

            if (isadd)
            {
                blog_author_dtl dtl = new blog_author_dtl();
                dtl.create_date = DateTime.Now;
                dtl = SetDetail(dtl, fc, "ch");
                data.blog_author_dtl.Add(dtl);
                dtl = new blog_author_dtl();
                dtl.create_date = DateTime.Now;
                dtl = SetDetail(dtl, fc, "en");
                data.blog_author_dtl.Add(dtl);

            }
            else
            {
                foreach(var dtl in data.blog_author_dtl)
                {
                    dtl.update_date = DateTime.Now;
                    SetDetail(dtl, fc, dtl.lang);
                }
            }

            return data;
        }

        private blog_author_dtl SetDetail(blog_author_dtl dtl,FormCollection fc, string lang)
        {
            dtl.lang = lang;
            dtl.name = fc[lang+"_name"];
            dtl.brief = fc[lang + "_brief"];
            dtl.brieftext = dtl.brief.StripHTML();
            return dtl;
        }

        private bool UpdateData(blog_author data)
        {
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            //if (data.picurl==null || string.IsNullOrEmpty(data.picurl))
            //{
            //    is_valid = false;
            //    error.Add("tmppicurl", "Neccesary"); //"必須輸入照片");
            //}
            foreach(blog_author_dtl dtl in data.blog_author_dtl)
            {
                string langstr = (dtl.lang == "ch") ? "中文" : "英文";
                #region 驗證
                if (fc[dtl.lang + "_name"] != null && string.IsNullOrEmpty(dtl.name))
                {
                    is_valid = false;
                    error.Add(dtl.lang + "_name", "Neccesary"); //"必須輸入作者"+langstr+"名");
                }
                //if (fc[dtl.lang + "_brief"] != null && string.IsNullOrEmpty(dtl.brief))
                //{
                //    is_valid = false;
                //    error.Add(dtl.lang + "_brief", "Neccesary");
                //}
                //驗證name重複
                blog_author_dtl chkdtl = model.GetDtlDataByName(dtl.name, dtl.lang);
                if (chkdtl != null)
                {
                    if (chkdtl.id != dtl.id)
                    {
                        is_valid = false;
                        error.Add(dtl.lang + "_name", "name duplication");
                    }
                }
                #endregion
            }


            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                affected_row += model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteData(string param1)
        {
            Init();
            string errmsg = "";
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            blog_author_dtl chk = model.GetDtlData(id);
            errmsg = model.DeleteData(chk.authorid);

            if (errmsg == "")
            {
                ShowUpdateMessage();
            }
            else
            {
                ShowUpdateFail(errmsg);
            }
            SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "blog_type", id);

            return RedirectToAction("List");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxUploadImage()
        {
            Init();

            string res = "";
            System.Web.HttpPostedFileBase file = getFile("picurl");
            if (file != null)
            {
                res = resizeImage(moveFile(file), 256, 256);
            }
            return Json(res);
        }
    }
}