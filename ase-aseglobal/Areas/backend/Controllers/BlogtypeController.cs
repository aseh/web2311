﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using web.Areas.backend.Models;
using web.Common;
using web.Models;

namespace web.Areas.backend.Controllers
{
    public class BlogtypeController : BackendController
    {
        private blog_type_model model;
        protected List<Dictionary<string, int>> ratio = new List<Dictionary<string, int>>();

        public BlogtypeController()
        {
            model = new blog_type_model();
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List(int type = 0, string keyword = "", int sort = 0)
        {
            Init();

            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "Blogtype";

            List<blog_type> list = model.GetList( keyword, sort);
            //ViewBag.type = type;

            return View(list);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteData(string param1)
        {
            Init();
            string errmsg = "";
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            blog_type chk = model.GetData(id);
            if (chk.name.ToLower().Contains(":\"未分類\",\"en\":"))
            {
                errmsg = "預設分類不可刪除";
            }
            else
            {
                errmsg = model.DeleteData(id);
            }

            if (errmsg == "")
            {
                ShowUpdateMessage();
            }
            else
            {
                ShowUpdateFail(errmsg);
            }
            SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "blog_type", id);

            return RedirectToAction("List");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxPostData(string param1, string ch_name,string en_name)
        {
            Init();
            BaseResponse res = new BaseResponse();
            try
            {
                //
                if (ch_name == "")
                {
                    res.code = 400; res.message = "中文分類不可空白";
                }
                else if (en_name == "")
                {
                    res.code = 400; res.message = "英文分類不可空白";
                }
                else
                {
                    long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
                    string str = "{\"ch\":\"" + ch_name + "\",\"en\":\"" + en_name + "\"}";
                    if (id == 0)
                    {
                        //檢查是否有重複名稱
                        if (model.GetDataByChname(ch_name) != null)
                        {
                            res.code = 400; res.message = "中文分類重覆";
                        }
                        if (model.GetDataByEnname(en_name) != null)
                        {
                            res.code = 400; res.message = "英文分類重覆";
                        }
                        if (res.code == 0) model.AddData(str);
                    }
                    else
                    {
                        //檢查是否有重複名稱
                        blog_type tmp = model.GetDataByChname(ch_name);
                        if (tmp != null && tmp.id != id)
                        {
                            res.code = 400; res.message = "中文分類重覆";
                        }
                        tmp = model.GetDataByEnname(en_name);
                        if (tmp != null && tmp.id != id)
                        {
                            res.code = 400; res.message = "英文分類重覆";
                        }
                        if (res.code == 0) model.UpdateData(id, str);
                    }
                }
               
            }
            catch (Exception ex)
            {
                res.code = 400; res.message = ex.Message;
            }

            if (res.code == 0)
            {
                ShowUpdateMessage();
            }
            else
            {
                ShowUpdateFail(res.message);
            }

            return Json(res);
            //return RedirectToAction("List");
        }
    }
}