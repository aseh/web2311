﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using web.Areas.backend.Models;
using web.Common;
using web.Models;

namespace web.Areas.backend.Controllers
{
    public class ContactController : BackendController
    {
        private Contact_model model;
        protected List<Dictionary<string, int>> ratio = new List<Dictionary<string, int>>();

        public ContactController()
        {
            model = new Contact_model();
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //useless
        public ActionResult List(int type = 0, string keyword = "", int sort = 0)
        {
            Init();

            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "Contact";

            List<contact> list = model.GetList(type, keyword, sort);
            //ViewBag.type = type;

            return View(list);
        }

        public ActionResult Content(long param1 = 0)
        {
            Init();
            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Contact";

            contact data;
            if (param1 == 0)
            {
                data = new contact();
                data.status = 0;
            }
            else
            {
                data = model.GetData(param1);
            }

            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Content(FormCollection fc)
        {
            Init();

            contact data = FcToData(fc);
            if (UpdateData(data))
            {
                ShowUpdateMessage();
                return RedirectToAction("List");
            }
            else
            {
                ShowUpdateFail();
            }

            return View(data);
        }

        private contact FcToData(FormCollection fc)
        {
            this.fc = fc;
            contact data = new contact();
            long id = Tool.TryToParseLong(fc["id"]);
            if (id > 0)
            {
                data = model.GetData(id);
            }

            data.status = Tool.TryToParseInt(fc["status"]);
            data.update_date = DateTime.Now;
            return data;
        }

        private bool UpdateData(contact data)
        {
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            #region 驗證
            #endregion

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                affected_row += model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
                string security_cate = data.id == 0 ? "insert" : "update";
                SecurityLog(security_cate, web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "contact", data.id);
            }
            return false;
        }



    }
}