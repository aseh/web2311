﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using web.Areas.backend.Models;
using web.Common;
using web.Models;

namespace web.Areas.backend.Controllers
{
    public class CsrNewsController : BackendController
    {
        private csr_news_model model;
        protected List<Dictionary<string, int>> ratio = new List<Dictionary<string, int>>();

        public CsrNewsController()
        {
            model = new csr_news_model();
        }
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        
        public ActionResult List(int sort = 1, DateTime? searchP = null, string keyword = "")
        {
            Init();
            ViewBag.content_header_sub_title = "List";
            List<csr_news> list = model.GetList(sort, searchP, keyword);
            ViewBag.sort = sort;
            ViewBag.keyword = keyword;
            ViewBag.searchP = searchP;
            return View(list);
        }

        public ActionResult Content(long param1 = 0)
        {
            Init();
            ViewBag.content_header_sub_title = "Content";
            csr_news data;
            if (param1 == 0)
            {
                data = new csr_news();
                data.publish_date = DateTime.Now;
                data.lang = "en";
                data.launch = 1;
            }
            else
            {
                data = model.GetData(param1);
            }

            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Content(FormCollection fc)
        {
            Init();

            csr_news data = FcToData(fc);
            if (UpdateData(data))
            {
                ShowUpdateMessage();
                return RedirectToAction("List", new { id = node.wm_url_id });
            }
            else
            {
                ShowUpdateFail();
            }
            
            return View(data);
        }

        private csr_news FcToData(FormCollection fc)
        {
            this.fc = fc;
            csr_news data = new csr_news();

            long id = Tool.TryToParseLong(fc["id"]);
            if (id > 0)
            {
                data = model.GetData(id);
                if (data == null || data.id == 0)
                {
                    data.create_date = DateTime.Now;
                }
            } else
            {
                data.create_date = DateTime.Now;
            }

            data.title = fc["title"].Trim();
            data.brief = fc["brief"].Trim();
            data.link = fc["link"].Trim();
            data.contents = fc["contents"];
            data.lang = fc["lang"].Trim();
            // 發佈日期
            if (!string.IsNullOrEmpty(fc["publish_date"]))
            {
                data.publish_date = Convert.ToDateTime(fc["publish_date"].Trim());
            }
            else
            {
                data.publish_date = null;
            }
            data.launch = Tool.TryToParseByte(fc["launch"]);
            data.update_date = DateTime.Now;

            return data;
        }

        private bool UpdateData(csr_news data)
        {
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            #region 驗證
            if (fc["title"] != null && string.IsNullOrEmpty(data.title))
            {
                is_valid = false;
                error.Add("title", "Necessary");
            }
            if (fc["brief"] != null && string.IsNullOrEmpty(data.brief))
            {
                is_valid = false;
                error.Add("brief", "Necessary");
            }
            if (data.publish_date == null)
            {
                is_valid = false;
                error.Add("publish_date", "Necessary");
            }
            #endregion

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                affected_row += model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteData(long param1)
        {
            if (param1 > 0)
            {
                Init();

                Dictionary<string, object> result = model.DeleteData(param1, Server.MapPath("~/" + website_path));

                int affected_row = (int)result["affected_row"];
                if (affected_row > 0)
                {
                    ShowUpdateMessage();
                }
                else
                {
                    ShowUpdateFail();
                }
                SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "csr_news", param1);
            }
            return RedirectToAction("List");
        }

        //ajax update launch, return value = 1:enable, 0:disable
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxUpdateLaunch(string param1)
        {
            Init();
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            if (id == 0)
            {
                return Json("");
            }
            else
            {
                return Json(model.ToggleLaunch(id));
            }
        }
        
    }
}