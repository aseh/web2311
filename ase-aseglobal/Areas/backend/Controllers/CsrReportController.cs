﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web.Mvc;
using web.Areas.backend.Models;
using web.Common;
using web.Models;

namespace web.Areas.backend.Controllers
{
    public class CsrReportController : BackendController
    {
        private Csr_report_model model;
        private Csr_report_questionnaire_model questionnair_model;
        protected List<Dictionary<string, int>> ratio = new List<Dictionary<string, int>>();

        public CsrReportController()
        {
            model = new Csr_report_model();
            questionnair_model = new Csr_report_questionnaire_model();
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        
        public ActionResult List()
        {
            Init();

            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "CsrReport";

            List<csr_report> list = model.GetList();

            return View(list);
        }

        public ActionResult Content(long param1 = 0)
        {
            Init();
            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "CsrReport";

            csr_report data;
            if (param1 == 0)
            {
                data = new csr_report();
                data.launch = 1;
                data.lang = "en";
            }
            else
            {
                data = model.GetData(param1);
            }
            
            this.ratio.Add(this.GetRatioSize(150, 150));
            ViewBag.ratio = ratio;
            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Content(FormCollection fc)
        {
            Init();
            ViewBag.current_tab = "CsrReport";

            csr_report data = FcToData(fc);
            if (UpdateData(data))
            {
                ShowUpdateMessage();
                return RedirectToAction("List");
            }
            else
            {
                ShowUpdateFail();
            }
            
            this.ratio.Add(this.GetRatioSize(150, 150));
            ViewBag.ratio = ratio;

            return View(data);
        }

        private csr_report FcToData(FormCollection fc)
        {
            this.fc = fc;
            csr_report data = new csr_report();

            long id = Tool.TryToParseLong(fc["id"]);
            if (id > 0)
            {
                data = model.GetData(id);
            }
            else
            {
                data.create_date = DateTime.Now;
            }

            data.title = fc["title"].Trim();
            string img_filename = fc["img_filename"].Replace("/" + media_bank_path, "").Replace("/" + website_path, "");
            string old_img_filename = fc["old_img_filename"].Replace("/" + media_bank_path, "").Replace("/" + website_path, "");
            if (!string.IsNullOrEmpty(img_filename) && img_filename != old_img_filename)
            {
                if (img_filename.LastIndexOf('/') >= 0)
                {
                    img_filename = img_filename.Substring(img_filename.LastIndexOf('/') + 1);
                }
            }
            data.img_filename = img_filename;
            
            string filename = fc["filename"].Replace("/" + media_bank_path, "").Replace("/" + website_path, "");
            string old_filename = fc["old_filename"].Replace("/" + media_bank_path, "").Replace("/" + website_path, "");
            if (!string.IsNullOrEmpty(filename) && filename != old_filename)
            {
                if (filename.LastIndexOf('/') >= 0)
                {
                    filename = filename.Substring(filename.LastIndexOf('/') + 1);
                }
            }
            data.filename = filename;
            data.launch = Tool.TryToParseByte(fc["launch"]);
            data.lang = fc["lang"].Trim();
            data.update_date = DateTime.Now;

            return data;
        }

        private bool UpdateData(csr_report data)
        {
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            #region 驗證
            if (fc["title"] != null && string.IsNullOrEmpty(data.title))
            {
                is_valid = false;
                error.Add("title", "Necessary");
            }
            if (fc["img_filename"] != null && string.IsNullOrEmpty(data.img_filename))
            {
                is_valid = false;
                error.Add("img_filename", "Necessary");
            }
            if (fc["filename"] != null && string.IsNullOrEmpty(data.filename))
            {
                is_valid = false;
                error.Add("filename", "Necessary");
            }
            #endregion

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                data.img_filename = dealImage();
                data.filename = dealFile();
                affected_row += model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
                string security_cate = data.id == 0 ? "insert" : "update";
                SecurityLog(security_cate, web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "csr_report", data.id);
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteData(long param1)
        {
            if (param1 > 0)
            {
                Init();

                Dictionary<string, object> result = model.DeleteData(param1, Server.MapPath("~/" + website_path));

                int affected_row = (int)result["affected_row"];
                if (affected_row > 0)
                {
                    ShowUpdateMessage();
                }
                else
                {
                    ShowUpdateFail();
                }
                SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "csr_report", param1);
            }
            return RedirectToAction("List");
        }
        
        //ajax update launch, return value = 1:enable, 0:disable
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxUpdateLaunch(string param1)
        {
            Init();
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            if (id == 0)
            {
                return Json("");
            }
            else
            {
                return Json(model.ToggleLaunch(id));
            }
        }
        
        public FileContentResult Export()
        {
            var csv = new StringBuilder();

            List<csr_report_questionnaire> list = questionnair_model.GetList();
 
            string stakeholder = "", q1 = "", q2 = "", q3 = "", q4 = "", q5 = "", q6 = "", q7 = "", q8 = "", q9 = "", q10 = "", q11 = "", q12 = "", q13 = "", q14 = "", q15 = "", q16 = "", q17 = "", q18 = "", q19 = "", q20 = "", q21 = "", q22 = "", q23 = "", q24 = "", create_date = "";
            
            string newLine = "#";
            int i = 1;
            foreach (csr_report_questionnaire item in list)
            {
                newLine = newLine + "," + i.ToString();
                i++;
            }
            csv.AppendLine(newLine);

            newLine = "Kind of Stakeholder";
            foreach (csr_report_questionnaire item in list)
            {
                stakeholder = item.stakeholder;
                if (stakeholder == "Other")
                {
                    stakeholder = item.other;
                }
                newLine = newLine + "," + stakeholder;
            }
            csv.AppendLine(newLine);

            newLine = "Corporate Governance";
            foreach (csr_report_questionnaire item in list)
            {
                q1 = GetTypeOfAnswer(item.q1);
                newLine = newLine + "," + q1;
            }
            csv.AppendLine(newLine);

            newLine = "Business Ethics";
            foreach (csr_report_questionnaire item in list)
            {
                q2 = GetTypeOfAnswer(item.q2);
                newLine = newLine + "," + q2;
            }
            csv.AppendLine(newLine);

            newLine = "Regulatory Compliance";
            foreach (csr_report_questionnaire item in list)
            {
                q3 = GetTypeOfAnswer(item.q3);
                newLine = newLine + "," + q3;
            }
            csv.AppendLine(newLine);

            newLine = "Stakeholder Communications";
            foreach (csr_report_questionnaire item in list)
            {
                q4 = GetTypeOfAnswer(item.q4);
                newLine = newLine + "," + q4;
            }
            csv.AppendLine(newLine);

            newLine = "Business Strategy and Performance";
            foreach (csr_report_questionnaire item in list)
            {
                q5 = GetTypeOfAnswer(item.q5);
                newLine = newLine + "," + q5;
            }
            csv.AppendLine(newLine);

            newLine = "Customer Relationship Management";
            foreach (csr_report_questionnaire item in list)
            {
                q6 = GetTypeOfAnswer(item.q6);
                newLine = newLine + "," + q6;
            }
            csv.AppendLine(newLine);

            newLine = "Taxation";
            foreach (csr_report_questionnaire item in list)
            {
                q7 = GetTypeOfAnswer(item.q7);
                newLine = newLine + "," + q7;
            }
            csv.AppendLine(newLine);

            newLine = "Sustainable Supply Chain";
            foreach (csr_report_questionnaire item in list)
            {
                q8 = GetTypeOfAnswer(item.q8);
                newLine = newLine + "," + q8;
            }
            csv.AppendLine(newLine);

            newLine = "Innovation Management";
            foreach (csr_report_questionnaire item in list)
            {
                q9 = GetTypeOfAnswer(item.q9);
                newLine = newLine + "," + q9;
            }
            csv.AppendLine(newLine);

            newLine = "Risk Management";
            foreach (csr_report_questionnaire item in list)
            {
                q10 = GetTypeOfAnswer(item.q10);
                newLine = newLine + "," + q10;
            }
            csv.AppendLine(newLine);

            newLine = "Climate Change";
            foreach (csr_report_questionnaire item in list)
            {
                q11 = GetTypeOfAnswer(item.q11);
                newLine = newLine + "," + q11;
            }
            csv.AppendLine(newLine);

            newLine = "Waste Management";
            foreach (csr_report_questionnaire item in list)
            {
                q12 = GetTypeOfAnswer(item.q12);
                newLine = newLine + "," + q12;
            }
            csv.AppendLine(newLine);

            newLine = "Water Resource Management";
            foreach (csr_report_questionnaire item in list)
            {
                q13 = GetTypeOfAnswer(item.q13);
                newLine = newLine + "," + q13;
            }
            csv.AppendLine(newLine);

            newLine = "Energy Management";
            foreach (csr_report_questionnaire item in list)
            {
                q14 = GetTypeOfAnswer(item.q14);
                newLine = newLine + "," + q14;
            }
            csv.AppendLine(newLine);

            newLine = "Green Solutions";
            foreach (csr_report_questionnaire item in list)
            {
                q15 = GetTypeOfAnswer(item.q15);
                newLine = newLine + "," + q15;
            }
            csv.AppendLine(newLine);
            
            newLine = "Talent Development";
            foreach (csr_report_questionnaire item in list)
            {
                q16 = GetTypeOfAnswer(item.q16);
                newLine = newLine + "," + q16;
            }
            csv.AppendLine(newLine);

            newLine = "Equality & Diversity";
            foreach (csr_report_questionnaire item in list)
            {
                q17 = GetTypeOfAnswer(item.q17);
                newLine = newLine + "," + q17;
            }
            csv.AppendLine(newLine);

            newLine = "Human Right Mechanism";
            foreach (csr_report_questionnaire item in list)
            {
                q18 = GetTypeOfAnswer(item.q18);
                newLine = newLine + "," + q18;
            }
            csv.AppendLine(newLine);

            newLine = "Employee Communication";
            foreach (csr_report_questionnaire item in list)
            {
                q19 = GetTypeOfAnswer(item.q19);
                newLine = newLine + "," + q19;
            }
            csv.AppendLine(newLine);

            newLine = "Employee Health & Safety";
            foreach (csr_report_questionnaire item in list)
            {
                q20 = GetTypeOfAnswer(item.q20);
                newLine = newLine + "," + q20;
            }
            csv.AppendLine(newLine);

            newLine = "Significant Community Development";
            foreach (csr_report_questionnaire item in list)
            {
                q21 = GetTypeOfAnswer(item.q21);
                newLine = newLine + "," + q21;
            }
            csv.AppendLine(newLine);

            newLine = "Environmental Conservation Charity";
            foreach (csr_report_questionnaire item in list)
            {
                q22 = GetTypeOfAnswer(item.q22);
                newLine = newLine + "," + q22;
            }
            csv.AppendLine(newLine);

            newLine = "Public Policy & Initiatives";
            foreach (csr_report_questionnaire item in list)
            {
                q23 = GetTypeOfAnswer(item.q23);
                newLine = newLine + "," + q23;
            }
            csv.AppendLine(newLine);

            newLine = "suggestoin";
            foreach (csr_report_questionnaire item in list)
            {
                q24 = GetTypeOfAnswer(item.additional);
                newLine = newLine + "," + q24;
            }
            csv.AppendLine(newLine);

            newLine = "Create date";
            foreach (csr_report_questionnaire item in list)
            {
                create_date = string.Format("{0:yyyy/MM/dd}", item.create_date);
                newLine = newLine + "," + create_date;
            }
            csv.AppendLine(newLine);
            
            return File(Encoding.Default.GetBytes(csv.ToString()), "text/csv", "Questionnaire.csv");
        }

        private string GetTypeOfAnswer(string data)
        {
            string type = "";
            switch (data)
            {
                case "VeryHigh":
                    type = "5";
                    break;

                case "High":
                    type = "4";
                    break;

                case "Neutral":
                    type = "3";
                    break;
                   
                case "Low":
                    type = "2";
                    break;

                case "VeryLow":
                    type = "1";
                    break;

                default:
                    type = data;
                    if (data.Contains(","))
                    {
                        type = type.Replace(',', '\0');
                    }
                    if (data.Contains("\n"))
                    {
                        type = type.Replace('\n', '\0');
                    }
                    if (data.Contains("\r"))
                    {
                        type = type.Replace('\r', '\0');
                    }
                    break;
            }

            return type;
        }
    }
}