﻿using System.Web.Mvc;

namespace web.Areas.backend.Controllers
{
    public class DashboardController : BackendController
    {
        // GET: backend/Dashboard
        public ActionResult Index()
        {
            Init();
            return View();
        }
    }
}