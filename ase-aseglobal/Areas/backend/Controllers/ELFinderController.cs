﻿using System.Web.Mvc;

namespace web.Areas.backend.Controllers
{

    /// <summary>
    /// ELFinder controller
    /// IMPORTANT: If you use this controller in your existing application, make sure to register the custom ELFinder model binder (ELFinderModelBinder) with ASP.NET MVC.
    ///            Look at the Application_Start() method in Global.asax.cs on how to do so.
    /// </summary>
    public class ELFinderController : BackendController
    {
        public ActionResult Index()
        {
            Init();
            return View();
        }       
    }
}

//using ElFinder;
//using System.IO;
//using System.Web.Mvc;
//namespace web.Areas.backend.Controllers
//{
//    /// <summary>
//    /// 
//    /// </summary>
//    public class ElFinderController : ElFinderControllerBase
//    {
//        /// <summary>
//        /// 
//        /// </summary>
//        public ElFinderController()
//        {
//            ViewBag.RootUrl = "/";
//            FileSystemDriver driver = new FileSystemDriver();
//            //string scriptsPath = @"C:\Users\Administrator";
//            //string thumbsPath = @"C:\Users\Administrator";
//            DirectoryInfo thumbsStorage = new DirectoryInfo(Server.MapPath("~/public/uploads"));
//            driver.AddRoot(new Root(new DirectoryInfo(Server.MapPath("~/public/uploads")), "/public/uploads/")
//            {
//                IsLocked = false,
//                IsReadOnly = false,
//                IsShowOnly = false,
//                ThumbnailsStorage = thumbsStorage,
//                ThumbnailsUrl = "/Thumbnails/",
//                MaxUploadSizeInMb = 10
//            });
//            InitDriver(driver);
//        }
//        // GET: ElFinder
//        /// <summary>
//        /// 
//        /// </summary>
//        /// <returns></returns>
//        public ActionResult ElFinder()
//        {
//            return View();
//        }
//    }
//}