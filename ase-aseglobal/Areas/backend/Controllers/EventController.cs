﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using web.Areas.backend.Models;
using web.Common;
using web.Models;

namespace web.Areas.backend.Controllers
{
    public class EventController : BackendController
    {
        private Event_model model;
        protected List<Dictionary<string, int>> ratio = new List<Dictionary<string, int>>();

        public EventController()
        {
            model = new Event_model();
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //useless
        public ActionResult List(int sort = 1, DateTime? searchS = null, DateTime? searchE = null, string keyword = "")
        {
            Init();

            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "Event";

            List<events> list = model.GetList(sort, searchS, searchE, keyword);

            ViewBag.sort = sort;
            ViewBag.searchS = searchS;
            ViewBag.searchE = searchE;
            ViewBag.keyword = keyword;

            return View(list);
        }

        public ActionResult Content(long param1 = 0)
        {
            Init();
            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Event";
            events data;
            if (param1 == 0)
            {
                data = new events();
                data.lang = "en";
                data.launch = 1;
                data.start_date = DateTime.Now;
            } else
            {
                data = model.GetData(param1);
            }
            
            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Content(FormCollection fc)
        {
            Init();

            events data = FcToData(fc);
            if (UpdateData(data))
            {
                ShowUpdateMessage();
                return RedirectToAction("List");
            }
            else
            {
                ShowUpdateFail();
            }
            
            return View(data);
        }

        private events FcToData(FormCollection fc)
        {
            this.fc = fc;
            events data = new events();

            long id = Tool.TryToParseLong(fc["id"]);
            if (id > 0)
            {
                data = model.GetData(id);
            }

            data.title = fc["title"].Trim();
            data.position = fc["position"].Trim();
            data.link = fc["link"].Trim();
            if (!string.IsNullOrEmpty(fc["start_date"]))
            {
                data.start_date = Convert.ToDateTime(fc["start_date"].Trim());
            }
            else
            {
                data.start_date = null;
            }
            if (!string.IsNullOrEmpty(fc["end_date"]))
            {
                data.end_date = Convert.ToDateTime(fc["end_date"].Trim());
            }
            else
            {
                data.end_date = null;
            }
            data.lang = fc["lang"].Trim();
            data.launch = Tool.TryToParseByte(fc["launch"]);
            data.update_date = DateTime.Now;
            return data;
        }

        private bool UpdateData(events data)
        {
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            #region 驗證
            if (fc["title"] != null && string.IsNullOrEmpty(data.title))
            {
                is_valid = false;
                error.Add("title", "Necessary");
            }
            if (data.start_date == null)
            {
                is_valid = false;
                error.Add("start_date", "Necessary");
            }
            #endregion

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                affected_row += model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
                string security_cate = data.id == 0? "insert":"update";
                SecurityLog(security_cate, web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "events", data.id);
            }
            return false;
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteData(long param1)
        {
            if (param1 > 0)
            {
                Init();

                Dictionary<string, object> result = model.DeleteData(param1, Server.MapPath("~/" + website_path));

                int affected_row = (int)result["affected_row"];
                if (affected_row > 0)
                {
                    ShowUpdateMessage();
                }
                else
                {
                    ShowUpdateFail();
                }
                SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "events", param1);
            }
            return RedirectToAction("List");
        }

        //ajax update launch, return value = 1:enable, 0:disable
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxUpdateLaunch(string param1)
        {
            Init();
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            if (id == 0)
            {
                return Json("");
            }
            else
            {
                return Json(model.ToggleLaunch(id));
            }
        }


    }
}