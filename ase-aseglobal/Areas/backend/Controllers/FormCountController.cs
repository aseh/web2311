﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using web.Areas.backend.Models;
using web.Models;

namespace web.Areas.backend.Controllers
{
    public class FormCountController : BackendController
    {
        private Csr_report_count_model model;
        private Csr_report_model reprot_model;
        protected List<Dictionary<string, int>> ratio = new List<Dictionary<string, int>>();

        public FormCountController()
        {
            model = new Csr_report_count_model();
            reprot_model = new Csr_report_model();
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        
        public ActionResult List(int year = 0, int month = 0)
        {
            Init();

            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "FormCount";

            List<csr_report_count> list = model.GetList(year, month);
            List<result> result_list = new List<result>();
            foreach (csr_report_count item in list)
            {
                result data = result_list.Where(x => x.report_id == item.report_id && x.year == item.create_date.Value.Year && x.month == item.create_date.Value.Month).FirstOrDefault();
                if (data == null)
                {
                    result temp = new result();
                    temp.report_id = item.report_id;
                    temp.title = reprot_model.GetData(item.report_id).title;
                    temp.year = item.create_date.Value.Year;
                    temp.month = item.create_date.Value.Month;
                    temp.count = 0;
                    if (item.questionnaire_id != null && item.questionnaire_id > 0)
                    {
                        temp.count = 1;
                    }
                    result_list.Add(temp);
                } else
                {
                    if (item.questionnaire_id != null)
                    {
                        data.count++;
                    }
                }
            }

            result_list = result_list.OrderBy(x => x.year).ThenBy(x => x.month).ToList();
            ViewBag.year = year;
            ViewBag.month = month;

            List<int> year_list = model.GetYearList();
            ViewBag.year_list = year_list;

            return View(result_list);
        }
    }
}