﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using web.Models;
using web.Areas.backend.Models;
using web.Common;
using System.Globalization;

namespace web.Areas.backend.Controllers
{
    public class HomeController : BackendController
    {
        private Home_page_model model;
        protected List<Dictionary<string, int>> ratio = new List<Dictionary<string, int>>();

        public HomeController()
        {
            model = new Home_page_model();
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //useless
        public ActionResult List()
        {
            Init();

            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "Banner";

            List<home_page> list = model.GetList();

            return View(list);
        }

        public ActionResult Content(long param1 = 0)
        {
            Init();
            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Banner";
            home_page data;
            if (param1 == 0)
            {
                data = new home_page();
                data.lang = "en";
                data.banner_web = "";
                data.banner_mobile = "";
            } else
            {
                data = model.GetData(param1);
            }

            this.ratio.Add(this.GetRatioSize(290, 252));
            ViewBag.ratio = ratio;

            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Content(FormCollection fc)
        {
            Init();
            ViewBag.current_tab = "Banner";

            home_page data = FcToData(fc);
            if (UpdateData(data))
            {
                ShowUpdateMessage();
                return RedirectToAction("List");
            }
            else
            {
                ShowUpdateFail();
            }

            this.ratio.Add(this.GetRatioSize(290, 252));
            ViewBag.ratio = ratio;

            return View(data);
        }

        private home_page FcToData(FormCollection fc)
        {
            
            this.fc = fc;
            home_page data = new home_page();

            long id = Tool.TryToParseLong(fc["id"]);
            if (id > 0)
            {
                data = model.GetData(id);
            }

            data.title = fc["title"].Trim();
            data.contents = fc["contents"].Trim();
            string img_filename = fc["img_filename"].Replace("/" + media_bank_path, "").Replace("/" + website_path, "");
            string old_img_filename = fc["old_img_filename"].Replace("/" + media_bank_path, "").Replace("/" + website_path, "");
            if (!string.IsNullOrEmpty(img_filename) && img_filename != old_img_filename)
            {
                if (img_filename.LastIndexOf('/') >= 0)
                {
                    img_filename = img_filename.Substring(img_filename.LastIndexOf('/') + 1);
                }
                
            }
            data.banner_web = img_filename;
            string img_filename2 = fc["img_filename2"].Replace("/" + media_bank_path, "").Replace("/" + website_path, "");
            string old_img_filename2 = fc["old_img_filename2"].Replace("/" + media_bank_path, "").Replace("/" + website_path, "");
            if (!string.IsNullOrEmpty(img_filename2) && img_filename != old_img_filename2)
            {
                if (img_filename2.LastIndexOf('/') >= 0)
                {
                    img_filename2 = img_filename2.Substring(img_filename2.LastIndexOf('/') + 1);
                }
                
            }
            data.banner_mobile = img_filename2;
            data.link = fc["link"].Trim();
            data.lang = fc["lang"].Trim();
            string test = fc["is_show_content"];
            data.is_show_content = fc["is_show_content"] != null;
            data.is_show_link = fc["is_show_link"] != null;
            data.is_show_title = fc["is_show_title"] != null;
            data.update_date = DateTime.Now;

            return data;
        }

        private bool UpdateData(home_page data)
        {
            var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            #region 驗證
            if (fc["title"] != null && string.IsNullOrEmpty(data.title))
            {
                is_valid = false;
                error.Add("title", "Necessary");
            }
            if (fc["contents"] != null && string.IsNullOrEmpty(data.contents))
            {
                is_valid = false;
                error.Add("contents", "Necessary");
            }
            if (fc["img_filename"] != null && string.IsNullOrEmpty(data.banner_web))
            {
                is_valid = false;
                error.Add("banner_web", "Necessary");
            }
            if (fc["img_filename2"] != null && string.IsNullOrEmpty(data.banner_mobile))
            {
                is_valid = false;
                error.Add("banner_mobile", "Necessary");
            }
            if (fc["link"] != null && string.IsNullOrEmpty(data.link))
            {
                is_valid = false;
                error.Add("link", "Necessary");
            }
            #endregion

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                //處理圖片，大小改變時記得也要將複製功能的大小改變！
                data.banner_web = dealImage("img_filename", 1920, 900);
                data.banner_mobile = dealImage("img_filename2", 900, 1024);
                if (data.banner_web.Contains(".mp4"))
                {
                    ffMpeg.GetVideoThumbnail(Server.MapPath("~/" + this.website_path + data.banner_web), 
                        Server.MapPath("~/" + this.website_path + data.banner_web.Replace(".mp4", ".png")));
                    data.video_thumbnail_web = data.banner_web.Replace(".mp4", ".png");
                }

                if (data.banner_mobile.Contains(".mp4"))
                {
                    ffMpeg.GetVideoThumbnail(
                        Server.MapPath("~/" + this.website_path + data.banner_mobile), 
                        Server.MapPath("~/" + this.website_path + data.banner_mobile.Replace(".mp4", ".png"))
                    );
                    data.video_thumbnail_mobile = data.banner_mobile.Replace(".mp4", ".png");
                }
                affected_row += model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
                string security_cate = data.id == 0? "insert":"update";
                SecurityLog(security_cate, web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "home_page", data.id);
            }
            return false;
        }
        
        [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult DeleteData(long param1)
        {
      if (param1 > 0)
      {
        Init();

        Dictionary<string, object> result = model.DeleteData(param1, Server.MapPath("~/" + website_path));

        int affected_row = (int)result["affected_row"];
        if (affected_row > 0)
        {
          ShowUpdateMessage();
        }
        else
        {
          ShowUpdateFail();
        }
        SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "home_page", param1);
      }
      return RedirectToAction("List");
        }
    }
}