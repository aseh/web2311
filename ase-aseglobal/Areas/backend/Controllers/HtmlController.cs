﻿using System.Web.Mvc;

namespace web.Areas.backend.Controllers
{
    public class HtmlController : BackendController
    {
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        
        public ActionResult List()
        {
            Init();
            return View();
        }

        public ActionResult Content()
        {
            Init();
            return View();
        }
    }
}