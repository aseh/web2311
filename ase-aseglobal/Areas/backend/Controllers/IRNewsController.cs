﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using web.Models;
using web.Areas.backend.Models;
using web.Common;

namespace web.Areas.backend.Controllers
{
    public class IRNewsController : BackendController
    {
        private Ir_news_model model;
        protected List<Dictionary<string, int>> ratio = new List<Dictionary<string, int>>();

        public IRNewsController()
        {
            model = new Ir_news_model();
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //useless
        public ActionResult List(int sort = 0, DateTime? searchS = null, DateTime? searchE = null, string keyword = "")
        {
            Init();

            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "IRNews";

            List<ir_news> list = model.GetList(sort, searchS, searchE, keyword);
            ViewBag.sort = sort;
            ViewBag.searchS = searchS;
            ViewBag.searchE = searchE;
            ViewBag.keyword = keyword;
            return View(list);
        }

        public ActionResult Content(long param1 = 0)
        {
            Init();
            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "IRNews";
            ir_news data;
            if (param1 == 0)
            {
                data = new ir_news();
                data.type = 1;
                data.lang = "en";
                data.start_date = DateTime.Now;
            } else
            {
                data = model.GetData(param1);
            }
            
            this.ratio.Add(this.GetRatioSize(290, 252));
            ViewBag.ratio = ratio;

            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Content(FormCollection fc)
        {
            Init();
            ViewBag.current_tab = "IRNews";

            ir_news data = FcToData(fc);
            if (UpdateData(data))
            {
                ShowUpdateMessage();
                return RedirectToAction("List");
            }
            else
            {
                ShowUpdateFail();
            }

            this.ratio.Add(this.GetRatioSize(290, 252));
            ViewBag.ratio = ratio;

            return View(data);
        }

        private ir_news FcToData(FormCollection fc)
        {
            this.fc = fc;
            ir_news data = new ir_news();

            long id = Tool.TryToParseLong(fc["id"]);
            if (id > 0)
            {
                data = model.GetData(id);
            }

            data.title = fc["title"].Trim();
            data.type = Tool.TryToParseInt(fc["type"]);
            data.link = fc["link"].Trim();
            if (!string.IsNullOrEmpty(fc["start_date"]))
            {
                data.start_date = Convert.ToDateTime(fc["start_date"].Trim());
            }
            else
            {
                data.start_date = null;
            }
            if (!string.IsNullOrEmpty(fc["end_date"]))
            {
                data.end_date = Convert.ToDateTime(fc["end_date"].Trim());
            }
            else
            {
                data.end_date = null;
            }
            data.lang = fc["lang"].Trim();
            data.update_date = DateTime.Now;

            return data;
        }

        private bool UpdateData(ir_news data)
        {
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            #region 驗證
            if (fc["title"] != null && string.IsNullOrEmpty(data.title))
            {
                is_valid = false;
                error.Add("title", "Necessary");
            }
            if (data.start_date == null)
            {
                is_valid = false;
                error.Add("start_date", "Necessary");
            }
            if (fc["link"] != null && string.IsNullOrEmpty(data.link))
            {
                is_valid = false;
                error.Add("link", "Necessary");
            }
            #endregion

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                //處理圖片，大小改變時記得也要將複製功能的大小改變！
                affected_row += model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
                string security_cate = data.id == 0 ? "insert" : "update";
                SecurityLog(security_cate, web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "ir_news", data.id);
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteData(long param1)
        {
            if (param1 > 0)
            {
                Init();

                Dictionary<string, object> result = model.DeleteData(param1, Server.MapPath("~/" + website_path));

                int affected_row = (int)result["affected_row"];
                if (affected_row > 0)
                {
                    ShowUpdateMessage();
                }
                else
                {
                    ShowUpdateFail();
                }
                SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "ir_news", param1);
            }
            return RedirectToAction("List");
        }
    }
}