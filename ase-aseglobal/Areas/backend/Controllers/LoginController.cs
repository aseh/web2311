﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;
using web.Models;
using web.Areas.backend.Controllers;
using web.Areas.backend.Models;
using web.Controllers;
using System.Collections.Generic;
using web.Common;
using System.Security;
using System.Runtime.InteropServices;

namespace web.Areas.Backend.Controllers
{
    public class LoginController : BackendController
    {
        public ActionResult Index()
        {
            //Response.Cookies.Clear();
            //Session.Abandon();
            //停用瀏覽器快取
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetNoStore();
            // 產生驗證碼

            if ( Session != null && Session["adminId"] != null )
            {
                return Redirect(Url.Action("index", "Dashboard"));
            }

            ImageVerificationCodeModel iv = new ImageVerificationCodeModel(HttpContext.Cache, Server.MapPath("/"));
            ImageVerificationCodeModel ivdata = iv.CreateIVCode();
            ViewData["key"] = ivdata.CacheKey;
            ViewData["img"] = ivdata.URL;

            return View();
        }

        [HttpPost, ValidateInput(true)]
        [ValidateAntiForgeryToken]
        public ActionResult Index(FormCollection fc)
        {
            Dictionary<string, string> error = new Dictionary<string, string>();
            string account = fc["account"] == null ? "" : Server.HtmlEncode(fc["account"]);
            SecureString securePwd = fc["password"] == null ? new SecureString() : GetPwdSecurity(fc["password"]);
            string key = fc["key"] == null ? "" : Server.HtmlEncode(fc["key"]);
            string vcode = fc["vcode"] == null ? "" : Server.HtmlEncode(fc["vcode"]);
            bool isvalid = true;
            admin_emp systemuser = new admin_emp();

            #region 驗證資料
            ImageVerificationCodeModel iv = new ImageVerificationCodeModel(HttpContext.Cache);
            if (!iv.isVerificationCodeMatch(key, vcode))
            {
                // 驗證未通過，產生新的驗證碼
                ImageVerificationCodeModel iv2 = new ImageVerificationCodeModel(HttpContext.Cache, Server.MapPath("/"));
                ImageVerificationCodeModel ivdata = iv2.CreateIVCode();
                ViewData["key"] = ivdata.CacheKey;
                ViewData["img"] = ivdata.URL;

                isvalid = false;
                error.Add("summary", "驗證碼錯誤!");
                this.SecurityLog("loginFail", "驗證碼錯誤. IP: " + Tool.GetCurrentIP());
            }
            else if (!Validator.CheckStringLength(account, 2, 50) || (securePwd.Length < 2 || securePwd.Length > 50))
            {
                isvalid = false;
                error.Add("summary", "請輸入正確的帳號或密碼!");
                this.SecurityLog("loginFail", "帳號或密碼格式錯誤. IP: " + Tool.GetCurrentIP());
            }
            if (isvalid)
            {
                admin_emp_model model = new admin_emp_model();
                systemuser = model.GetDataByAccount(account);
                if (systemuser == null || systemuser.ae_id == 0)
                {
                    isvalid = false;
                    error.Add("summary", "請輸入正確的帳號或密碼!");
                    this.SecurityLog("loginFail", "帳號或密碼錯誤. IP: " + Tool.GetCurrentIP());
                }
            }

            if (!isvalid)
            {
                Response.Cookies.Clear();
                Session.Abandon();
                //停用瀏覽器快取
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.Cache.SetNoStore();
              
                // 驗證未通過，產生新的驗證碼
                ImageVerificationCodeModel iv2 = new ImageVerificationCodeModel(HttpContext.Cache, Server.MapPath("/"));
                ImageVerificationCodeModel ivdata = iv2.CreateIVCode();
                ViewData["key"] = ivdata.CacheKey;
                ViewData["img"] = ivdata.URL;
                TempData["error"] = error;
                return View();
            }
            #endregion

            //加密後的密碼
            string pwd = CryptographyPassword(securePwd);
            if (systemuser != null && systemuser.ae_id > 0 && systemuser.ae_password.Equals(pwd))
            {
                this.LoginProcess(systemuser, true);
                this.SetLoginSession(systemuser);
                this.SecurityLog("login", systemuser.ae_empnm + "[" + systemuser.ae_id + "]");
                return RedirectToAction("Index", "Dashboard");
            }
            else
            {
                // 帳密未通過，產生新的驗證碼
                ImageVerificationCodeModel iv2 = new ImageVerificationCodeModel(HttpContext.Cache, Server.MapPath("/"));
                ImageVerificationCodeModel ivdata = iv2.CreateIVCode();
                ViewData["key"] = ivdata.CacheKey;
                ViewData["img"] = ivdata.URL;
                error.Add("summary", "請輸入正確的帳號或密碼!");
                TempData["error"] = error;
                return View();
            }
        }

        private static SecureString GetPwdSecurity(string vstrPassword)
        {
            var result = new SecureString();
            foreach (char c in vstrPassword)
            {
                result.AppendChar(c);
            }
            result.MakeReadOnly();
            return result;
        }

        private static string SecureStringToString(SecureString value)
        {
            var valuePtr = IntPtr.Zero;
            try
            {
                valuePtr = Marshal.SecureStringToGlobalAllocUnicode(value);
                return Marshal.PtrToStringUni(valuePtr);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(valuePtr);
            }
        }

        private string CryptographyPassword(SecureString inputPwd)
        {
            SHA256 sha256 = new SHA256CryptoServiceProvider();//建立一個SHA256
            byte[] source = Encoding.Default.GetBytes(SecureStringToString(inputPwd));//將字串轉為Byte[]
            byte[] crypto = sha256.ComputeHash(source);//進行SHA256加密
            string result = Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串
            return result;
        }

        private void LoginProcess(admin_emp user, bool isRemeber)
        {
            var now = DateTime.Now;
            var expiration = DateTime.Now.AddMinutes(30);

            var ticket = new FormsAuthenticationTicket(1,
                user.ae_name.ToString(),
                now,
                expiration,
                isRemeber,
                User.Identity.Name,
                FormsAuthentication.FormsCookiePath);

            var encryptedTicket = FormsAuthentication.Encrypt(ticket);

            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket)
            {
                Expires = expiration
            };
            cookie.HttpOnly = true;
            cookie.Secure = true;
            Response.Cookies.Add(cookie);
        }
    }
}