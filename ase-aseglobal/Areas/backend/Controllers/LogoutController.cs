﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace web.Areas.backend.Controllers
{
    public class LogoutController : Controller
    {
        public ActionResult Index()
        {
            //清除所有的 session
            Session.Abandon();
            Response.Cookies.Clear();

            Response.Cache.SetNoServerCaching();
            //
            HttpCookie myCookie = new HttpCookie("UserSettings");//取得cookie    
            myCookie.Expires = DateTime.Now.AddDays(-2d);//設定cookie的到期日為已過期　
            Response.Cookies.Add(myCookie);//將cookie變動寫回client端
                                           //

            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetNoStore();
            Response.Cache.SetExpires(new DateTime(1900, 1, 1, 0, 0, 0, 0));
            Response.Expires = 0;
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");

            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Login");
        }
    }
}