﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using web.Models;
using web.Areas.backend.Models;
using web.Common;
using System.Globalization;

namespace web.Areas.backend.Controllers
{
    public class MailController : BackendController
    {
        private Contact_email_model model;
        protected List<Dictionary<string, int>> ratio = new List<Dictionary<string, int>>();

        public MailController()
        {
            model = new Contact_email_model();
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //useless
        public ActionResult List()
        {
            Init();

            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "Mail";

            List<contact_email> list = model.GetList();

            return View(list);
        }

        public ActionResult Content(long param1 = 0)
        {
            Init();
            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Mail";
            contact_email data;
            if (param1 == 0)
            {
                data = new contact_email();
                data.lang = "en";
                data.launch = 1;
                data.type = 1;
            }
            else
            {
                data = model.GetData(param1);
            }

            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Content(FormCollection fc)
        {
            Init();
            ViewBag.current_tab = "Mail";

            contact_email data = FcToData(fc);
            if (UpdateData(data))
            {
                ShowUpdateMessage();
                return RedirectToAction("List");
            }
            else
            {
                ShowUpdateFail();
            }

            return View(data);
        }

        private contact_email FcToData(FormCollection fc)
        {
            this.fc = fc;
            contact_email data = new contact_email();

            long id = Tool.TryToParseLong(fc["id"]);
            if (id > 0)
            {
                data = model.GetData(id);
            }

            data.title = fc["title"].Trim();
            data.email = fc["email"].Trim();
            data.remark = fc["remark"].Trim();
            data.lang = fc["lang"].Trim();
            data.launch = Tool.TryToParseByte(fc["launch"]);
            data.type = Tool.TryToParseInt(fc["type"]);
            data.update_date = DateTime.Now;
            return data;
        }

        private bool UpdateData(contact_email data)
        {
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            #region 驗證
            if (fc["title"] != null && string.IsNullOrEmpty(data.title))
            {
                is_valid = false;
                error.Add("title", "Necessary");
            }
            if (fc["email"] != null && string.IsNullOrEmpty(data.email))
            {
                is_valid = false;
                error.Add("email", "Necessary");
            }
            #endregion

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                affected_row += model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
                string security_cate = data.id == 0 ? "insert" : "update";
                SecurityLog(security_cate, web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "contact_email", data.id);
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteData(long param1)
        {
            if (param1 > 0)
            {
                Init();

                Dictionary<string, object> result = model.DeleteData(param1, Server.MapPath("~/" + website_path));

                int affected_row = (int)result["affected_row"];
                if (affected_row > 0)
                {
                    ShowUpdateMessage();
                }
                else
                {
                    ShowUpdateFail();
                }
                SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "contact_email", param1);
            }
            return RedirectToAction("List");
        }

        //ajax update launch, return value = 1:enable, 0:disable
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxUpdateLaunch(string param1)
        {
            Init();
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            if (id == 0)
            {
                return Json("");
            }
            else
            {
                return Json(model.ToggleLaunch(id));
            }
        }

    }
}