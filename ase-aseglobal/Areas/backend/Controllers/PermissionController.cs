﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Text;
using System.Security.Cryptography;
using web.Models;
using web.Areas.backend.Models;
using web.Common;
using System.Linq;

namespace web.Areas.backend.Controllers
{
    public class PermissionController : BackendController
    {
        private admin_emp_group_model group_model;
        private admin_emp_group_belong_menu_model admin_group_belong_menu_model;
        private web_menu_model menu_model;

        public PermissionController()
        {
            group_model = new admin_emp_group_model();
            admin_group_belong_menu_model = new admin_emp_group_belong_menu_model();
            menu_model = new web_menu_model();
        }

        // GET: backend/Admin
        public ActionResult Index()
        {
            return RedirectToAction("GroupList");
        }

        #region Group
        public ActionResult GroupList()
        {
            Init();

            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "Group";

            List<admin_emp_group> list = group_model.GetList();

            return View(list);
        }

        public ActionResult GroupContent(long param1 = 0)
        {
            Init();

            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Group";

            admin_emp_group data;
            if (param1 == 0)
            {
                data = new admin_emp_group();
                data.aeg_launch = 1;
            }
            else
            {
                data = group_model.GetData(param1);
            }

            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult GroupContent(FormCollection fc)
        {
            Init();

            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Group";

            admin_emp_group data = FcToGroupData(fc);
            if (UpdateGroupData(data))
            {
                ShowUpdateMessage();
                return RedirectToAction("GroupList");
            }
            else
            {
                ShowUpdateFail();
            }

            return View(data);
        }

        private admin_emp_group FcToGroupData(FormCollection fc)
        {
            this.fc = fc;
            admin_emp_group data = new admin_emp_group();

            long aeg_id = Tool.TryToParseLong(fc["aeg_id"]);
            if (aeg_id > 0)
            {
                data = group_model.GetData(aeg_id);
                if (data == null || data.aeg_id == 0)
                {
                    data.aeg_creator = web_admin.ae_id;
                    data.aeg_creation_time = DateTime.Now;
                }
            }
            else
            {
                data.aeg_creator = web_admin.ae_id;
                data.aeg_creation_time = DateTime.Now;
            }

            data.aeg_name = fc["aeg_name"].Trim();
            data.aeg_launch = Tool.TryToParseByte(fc["aeg_launch"]);

            data.aeg_mdyer = web_admin.ae_id;
            data.aeg_last_update_time = DateTime.Now;

            return data;
        }

        private bool UpdateGroupData(admin_emp_group data)
        {
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            #region 驗證
            if (fc["aeg_name"] != null && string.IsNullOrEmpty(data.aeg_name))
            {
                is_valid = false;
                error.Add("aeg_name", "Necessary");
            }
            #endregion

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                affected_row += group_model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteGroupData(long param1)
        {
            if (param1 > 0)
            {
                Init();

                Dictionary<string, object> result = group_model.DeleteData(param1, Server.MapPath("~/" + website_path));

                int affected_row = (int)result["affected_row"];
                if (affected_row > 0)
                {
                    ShowUpdateMessage();
                }
                else
                {
                    ShowUpdateFail();
                }
                SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "admin_emp_group", param1);
            }
            return RedirectToAction("GroupList");
        }

        //ajax update launch, return value = 1:enable, 0:disable
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxUpdateGroupLaunch(string param1)
        {
            Init();
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            if (id == 0)
            {
                return Json("");
            }
            else
            {
                return Json(group_model.ToggleLaunch(id));
            }
        }
        #endregion

        public ActionResult PermissionContent(long param1 = 0)
        {
            Init();

            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Group";

            List<web_menu> menu_list = menu_model.GetList();
            ViewBag.menu_list = menu_list;

            List<admin_emp_group_belong_menu> list = admin_group_belong_menu_model.GetList(param1);
            ViewBag.group_id = param1;

            return View(list);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult PermissionContent(FormCollection fc)
        {
            Init();

            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Group";

            if (UpdatePermissionData(fc))
            {
                ShowUpdateMessage();
                return RedirectToAction("GroupList");
            }
            else
            {
                ShowUpdateFail();
            }

            return View();
        }

        public bool UpdatePermissionData(FormCollection fc)
        {
            admin_emp_group_belong_menu temp_data;
            long temp_menu_id;
            bool is_valid = true;

            string selected_menu = fc["wm_id[]"] == null ? "" : fc["wm_id[]"].ToString();
            long group_id = Tool.TryToParseLong(fc["group_id"]);

            // all menu
            List<web_menu> menu_list = menu_model.GetList();
            // group belong menu
            List<admin_emp_group_belong_menu> group_belong_menu_list = admin_group_belong_menu_model.GetList(group_id);
            // selected menu
            List<string> str_selected_menu_list = selected_menu.Split(',').Distinct().ToList();
            List<long> selected_menu_list = new List<long>();
            foreach (string str in str_selected_menu_list)
            {
                temp_menu_id = Tool.TryToParseLong(str);
                selected_menu_list.Add(temp_menu_id);
            }

            try
            {
                if (string.IsNullOrEmpty(selected_menu))
                {
                    foreach (admin_emp_group_belong_menu item in group_belong_menu_list)
                    {
                        admin_group_belong_menu_model.DeleteData(item.aegbm_id);
                    }
                }
                else
                {
                    // check all menu
                    foreach (web_menu menu in menu_list)
                    {
                        // current menu is not selected
                        if (selected_menu_list.Where(x => x == menu.wm_id).Count() == 0)
                        {
                            // but in original selected menu => delete it
                            if (group_belong_menu_list.Where(x => x.wm_id == menu.wm_id).Count() > 0)
                            {
                                admin_group_belong_menu_model.DeleteData(group_belong_menu_list.Where(x => x.wm_id == menu.wm_id).FirstOrDefault().aegbm_id);
                            }
                        }
                        else // current menu is selected
                        {
                            // but not in original selected menu => add it
                            if (group_belong_menu_list.Where(x => x.aeg_id == menu.wm_id).Count() == 0)
                            {
                                temp_data = new admin_emp_group_belong_menu();
                                temp_data.aeg_id = group_id;
                                temp_data.wm_id = menu.wm_id;
                                admin_group_belong_menu_model.SaveData(temp_data);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                is_valid = false;
                throw;
            }
            return is_valid;
        }
    }
}