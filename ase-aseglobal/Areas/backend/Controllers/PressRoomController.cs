﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using web.Areas.backend.Models;
using web.Common;
using web.Models;

namespace web.Areas.backend.Controllers
{
    public class PressRoomController : BackendController
    {
        private Press_room_model model;
        protected List<Dictionary<string, int>> ratio = new List<Dictionary<string, int>>();

        public PressRoomController()
        {
            model = new Press_room_model();
        }
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List(int sort = 1, DateTime? searchP = null, string keyword = "")
        {
            Init();
            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "PressRoom";
            List<press_room> list = model.GetList(sort, searchP, keyword);
            ViewBag.sort = sort;
            ViewBag.keyword = keyword;
            ViewBag.searchP = searchP;
            return View(list);
        }

        public ActionResult Content(long param1 = 0)
        {
            Init();
            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "PressRoom";
            press_room data;
            if (param1 == 0)
            {
                data = new press_room();
                data.publish_date = DateTime.Now;
                data.launch = 1;
                data.sign = 1;
            }
            else
            {
                data = model.GetData(param1);
            }
            this.ratio.Add(this.GetRatioSize(1280, 1280));
            ViewBag.ratio = ratio;
            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Content(FormCollection fc)
        {
            Init();

            press_room data = FcToData(fc);
            if (UpdateData(data))
            {
                ShowUpdateMessage();
                return RedirectToAction("List", new { id = node.wm_url_id });
            }
            else
            {
                ShowUpdateFail();
            }

            this.ratio.Add(this.GetRatioSize(1280, 1280));
            ViewBag.ratio = ratio;
            return View(data);
        }

        private press_room FcToData(FormCollection fc)
        {
            this.fc = fc;
            press_room data = new press_room();

            long id = Tool.TryToParseLong(fc["id"]);
            if (id > 0)
            {
                data = model.GetData(id);
                if (data == null || data.id == 0)
                {
                    data.create_date = DateTime.Now;
                }
            }
            else
            {
                data.create_date = DateTime.Now;
            }

            data.title = fc["title"].Trim();
            data.brief = fc["brief"].Trim();
            data.contents = fc["contents"];
            data.urlid = fc["urlid"].Trim();
            data.urlid2 = fc["urlid2"].Trim();

            HttpPostedFileBase file = getFile("og_image");
            if (file != null)
            {
                data.og_image = resizeImage(moveFile(file), 512, 512);
            } else
            {
                
            }
            
            // 發佈日期
            if (!string.IsNullOrEmpty(fc["publish_date"]))
            {
                data.publish_date = Convert.ToDateTime(fc["publish_date"].Trim());
            }
            else
            {
                data.publish_date = null;
            }
            data.sign = Tool.TryToParseByte(fc["sign"]);
            data.launch = Tool.TryToParseByte(fc["launch"]);
            data.lang = fc["lang"].Trim();
            data.update_date = DateTime.Now;

            return data;
        }

        private bool UpdateData(press_room data)
        {
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            #region 驗證
            if (fc["title"] != null && string.IsNullOrEmpty(data.title))
            {
                is_valid = false;
                error.Add("title", "Necessary");
            }
            if (fc["brief"] != null && string.IsNullOrEmpty(data.brief))
            {
                is_valid = false;
                error.Add("brief", "Necessary");
            }
            if (fc["contents"] != null && string.IsNullOrEmpty(data.contents))
            {
                is_valid = false;
                error.Add("contents", "Necessary");
            }
            if (data.publish_date == null)
            {
                is_valid = false;
                error.Add("publish_date", "Necessary");
            }
            //驗證urlid重複
            press_room checkUrl = model.checkUrlID(data.urlid);
            if (checkUrl != null)
            {
                if (data.id != checkUrl.id)
                {
                    is_valid = false;
                    error.Add("urlid", "Url Data duplication");
                }
            }
            if (fc["urlid"] != null && string.IsNullOrEmpty(data.urlid))
            {
                is_valid = false;
                error.Add("urlid", "Necessary");
            }
            #endregion

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                affected_row += model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteData(long param1)
        {
            if (param1 > 0)
            {
                Init();

                Dictionary<string, object> result = model.DeleteData(param1, Server.MapPath("~/" + website_path));

                int affected_row = (int)result["affected_row"];
                if (affected_row > 0)
                {
                    ShowUpdateMessage();
                }
                else
                {
                    ShowUpdateFail();
                }
                SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "press_room", param1);
            }
            return RedirectToAction("List");
        }

        //ajax update launch, return value = 1:enable, 0:disable
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxUpdateLaunch(string param1)
        {
            Init();
            long id = Tool.TryToParseLong(Server.HtmlEncode(param1));
            if (id == 0)
            {
                return Json("");
            }
            else
            {
                return Json(model.ToggleLaunch(id));
            }
        }

    }
}