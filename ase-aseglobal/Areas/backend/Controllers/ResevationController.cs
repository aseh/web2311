﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Models;
using web.ViewModels;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.IO;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;

namespace web.Areas.backend.Controllers
{
    public class ResevationController : BackendController
    {
        resevation_model serv = new resevation_model();


        private string renderHowKnow(ResevationQuest x)
        {
            if ( x.HowKnow == "網站" )
            {
                return string.Format("{0}：{1}", x.HowKnow, x.HowKnowSite);
            } else if (x.HowKnow == "其它管道")
            {
                return string.Format("{0}：{1}", x.HowKnow, x.HowKnowNote);
            }
            return x.HowKnow;
        }

        private string renderRecycle(ResevationQuest x)
        {
            if ( x.Recycle.Contains("其他"))
            {
                return string.Format("{0}：{1}", string.Join(",", x.Recycle), x.RecycleOther);
            }
            return string.Join(",", x.Recycle);
        }

        private string renderRecommend(ResevationQuest x)
        {
            if ( x.Recommend == "不願意" )
            {
                return string.Format("{0}：{1}", x.Recommend, x.RecommendOther);
            }
            return x.Recommend;
        }

        private string renderProject(ResevationQuest x )
        {
            if (x.Project == "其他")
            {
                return string.Format("{0}：{1}", x.Project, x.ProjectOther);
            }

            return x.Project;
        }

        // GET: backend/Resevation
        public ActionResult Export()
        {
            var List = serv.GetQuestList().Select(x => new { 
                CreatedAt = x.createdAt,
                Form = serv.Parse(x)
            }).ToList();
            var Excel = CreateExcel(
                List.Count,
                new Dictionary<string, List<string>>() {
                    { "姓名", List.Select(x => x.Form.Name).ToList() },
                    { "單位", List.Select(x => x.Form.Org).ToList() },
                    //"您如何得知『日月光綠科技教育館』"
                    { "您如何得知『日月光綠科技教育館』", List.Select(x => renderHowKnow(x.Form)).ToList() },

                    //請問您參訪過『日月光綠科技教育館』幾次？
                    { "請問您參訪過『日月光綠科技教育館』幾次？", List.Select(x => x.Form.Count).ToList() },

                    //"請問本次您參與的教案是：
                    { "請問本次您參與的教案是：", List.Select(x => renderProject(x.Form)).ToList() },

                    //透過這套教案，您有學習到環境保護或破壞與工廠發展息息相關
                    { "透過這套教案，您有學習到環境保護或破壞與工廠發展息息相關", List.Select(x => x.Form.ProjectA).ToList() },

                    //透過這套教案，您有認識到更多的環境事件
                    { "透過這套教案，您有認識到更多的環境事件", List.Select(x => x.Form.ProjectB).ToList() },

                    //請問何者為綠色工廠標章？
                    { "請問何者為綠色工廠標章？", List.Select(x => x.Form.ProjectC).ToList() },

                    //請問何者為節水標章？
                    { "請問何者為節水標章？", List.Select(x => x.Form.WaterMark).ToList() },

                    //課程及活動內容
                    { "課程及活動內容", List.Select(x => x.Form.ClassRank).ToList() },

                    //講師(含志工)及活動流程安排
                    { "講師(含志工)及活動流程安排", List.Select(x => x.Form.TeacherRank).ToList() },

                    //透過本次參訪，您學習到有哪些能資源可再循環利用？
                    { "透過本次參訪，您學習到有哪些能資源可再循環利用？", List.Select(x => renderRecycle(x.Form)).ToList() },

                    //透過本次參訪，您可以於日常採用何種方式來讓能資源再循環利用？
                    { "透過本次參訪，您可以於日常採用何種方式來讓能資源再循環利用？", List.Select(x => x.Form.RecycleNote).ToList() },

                    //請問本次參訪,您印象最深刻的設施、課程內容或工作人員..等，請簡單描述之。
                    { "請問本次參訪,您印象最深刻的設施、課程內容或工作人員..等，請簡單描述之。", List.Select(x => x.Form.Impression).ToList() },

                    //您是否願意再次或推薦親友來體驗參訪
                    { "您是否願意再次或推薦親友來體驗參訪", List.Select(x => renderRecommend(x.Form)).ToList() },

                    //若願意且方便的話，請留下您的聯繫email或電話：
                    { "若願意且方便的話，請留下您的聯繫email或電話：：", List.Select(x => x.Form.Contact).ToList() },

                    //課程或場域內容建議：
                    { "課程或場域內容建議：", List.Select(x => x.Form.Suggest).ToList() },

                    { "填表日期：", List.Select(x => x.CreatedAt.Value.ToString("yyyy/MM/dd")).ToList() }
                },
                new List<int>()
                {
                }
            ) ;

            return File(Excel, "application/octet-stream", "學習回饋單資料.xlsx");
        }


    
    }
}