﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using web.Areas.backend.Models;
using web.Common;
using web.Models;

namespace web.Areas.backend.Controllers
{
    public class SignController : BackendController
    {
        private sign_model model;
        protected List<Dictionary<string, int>> ratio = new List<Dictionary<string, int>>();

        public SignController()
        {
            model = new sign_model();
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        //useless
        public ActionResult List()
        {
            Init();

            ViewBag.content_header_sub_title = "List";
            ViewBag.current_tab = "Sign";

            List<sign> list = model.GetList();

            return View(list);
        }

        public ActionResult Content(long param1 = 0)
        {
            Init();
            ViewBag.content_header_sub_title = "Content";
            ViewBag.current_tab = "Sign";

            sign data;
            if (param1 == 0)
            {
                data = new sign();
            } else
            {
                data = model.GetData(param1);
            }
            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Content(FormCollection fc)
        {
            Init();

            sign data = FcToData(fc);
            if (UpdateData(data))
            {
                ShowUpdateMessage();
                return RedirectToAction("List");
            }
            else
            {
                ShowUpdateFail();
            }

            return View(data);
        }

        private sign FcToData(FormCollection fc)
        {
            this.fc = fc;
            sign data = new sign();

            long id = Tool.TryToParseLong(fc["id"]);
            if (id > 0)
            {
                data = model.GetData(id);
            }

            data.title = fc["title"].Trim();
            data.contents = fc["contents"].Trim();
            data.update_date = DateTime.Now;
            return data;
        }

        private bool UpdateData(sign data)
        {
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            #region 驗證
            if (fc["contents"] != null && string.IsNullOrEmpty(data.contents))
            {
                is_valid = false;
                error.Add("contents", "Necessary");
            }
            #endregion

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                affected_row += model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
                string security_cate = data.id == 0 ? "insert" : "update";
                SecurityLog(security_cate, web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "sign", data.id);
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteData(long param1)
        {
            if (param1 > 0)
            {
                Init();

                Dictionary<string, object> result = model.DeleteData(param1, Server.MapPath("~/" + website_path));

                int affected_row = (int)result["affected_row"];
                if (affected_row > 0)
                {
                    ShowUpdateMessage();
                }
                else
                {
                    ShowUpdateFail();
                }
                SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "sign", param1);
            }
            return RedirectToAction("List");
        }

    }
}