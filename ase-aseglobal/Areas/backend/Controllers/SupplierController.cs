﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Web.Mvc;
using web.Areas.backend.Models;
using web.Common;
using web.Models;

namespace web.Areas.backend.Controllers
{
    public class SupplierController : BackendController
    {
        private supplier_model model;
        protected List<Dictionary<string, int>> ratio = new List<Dictionary<string, int>>();

        public SupplierController()
        {
            model = new supplier_model();
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            Init();

            ViewBag.content_header_sub_title = "List";

            List<supplier> list = model.GetList();

            return View(list);
        }

        public ActionResult Content(long param1 = 0)
        {
            Init();
            ViewBag.content_header_sub_title = "Content";

            supplier data;
            if (param1 == 0)
            {
                data = new supplier();
                data.capital = 0;
                data.annual_revenue = 0;
                data.gender = 0;
                data.business_model = 0;
                data.business_varieties = 0;
            }
            else
            {
                data = model.GetData(param1);
            }

            ViewBag.country_list = _getCountry();

            this.ratio.Add(this.GetRatioSize(1920, 1920));
            ViewBag.ratio = ratio;



            return View(data);
        }

        private string GetBusinessModel(int index)
        {
            switch (index)
            {
                case 0:
                    return "製造商";
                case 1:
                    return "經銷商";
                case 2:
                    return "仲介";
            }
            return "";
        }
        private string GetBusinessVar(int index)
        {
            switch (index)
            {
                case 0:
                    return "材料";
                case 1:
                    return "機器設備";


                case 2:
                    return "建築/室內設計/修繕";

                case 3:
                    return "清潔環保/廢棄物處理";

                case 4:
                    return "資訊類";

                case 5:
                    return "運輸交通";

                case 6:
                    return "人力仲介";

                case 7:
                    return "其他";
                default:
                    return "";
            }
        }

        private Dictionary<string, List<string>> GetExportList(List<supplier> list)
        {
            Dictionary<string, List<string>> Result = new Dictionary<string, List<string>>();



            var keys = new List<string>(){
                "#",
                "公司名稱",
                "統一編號",
                "國別",
                "公司網頁",
                "電話(+國碼)",
                "傳真",
                "地址",
                "資本額(幣別)",
                "年營業額(幣別)",
                "姓名",
                "性別",
                "職銜",
                "電子郵件",
                "連絡電話",
                "廠商類別",
                "廠商類別備註",
                "營業類別",
                "營業類別備註",
                "主要客戶名稱",
                "QMS/Product",
                "Global Environment",
                "Safety/Health SA",
                "Security",
                "管理系統認證備註",
                "公司簡介",
                "建立日期"
            };

            foreach ( var key in keys )
            {
                Result[key] = new List<string>();
            }


            foreach (supplier item in list)
            {
                Result["#"].Add(item.id.ToString());
                Result["公司名稱"].Add(item.company);
                Result["統一編號"].Add(item.business_register_number);
                Result["國別"].Add(item.country);
                Result["公司網頁"].Add(item.website);
                Result["電話(+國碼)"].Add(item.country_code + item.tel);
                Result["傳真"].Add(item.fax);
                Result["地址"].Add(item.address);
                Result["資本額(幣別)"].Add(item.capital_currency + item.capital);
                Result["年營業額(幣別)"].Add(item.annual_revenue_currency + item.annual_revenue);
                Result["姓名"].Add(item.name);
                Result["性別"].Add(item.gender == 0 ? "女士" : "先生");
                Result["職銜"].Add(item.title);
                Result["電子郵件"].Add(item.email);
                Result["連絡電話"].Add(item.contact_tel);
                Result["廠商類別"].Add(GetBusinessModel(item.business_model));

                Result["廠商類別備註"].Add(item.business_model_remark);
                Result["營業類別"].Add(GetBusinessVar(item.business_varieties));
                Result["營業類別備註"].Add(item.business_varieties_remark);
                Result["主要客戶名稱"].Add(item.major_customers);
                //Result["管理系統認證"].Add(item.management_system);
                Result["QMS/Product"].Add(item.qms);
                Result["Global Environment"].Add(item.environment);
                Result["Safety/Health SA"].Add(item.health);
                Result["Security"].Add(item.securities);
                Result["管理系統認證備註"].Add(item.management_system_remark);
                Result["公司簡介"].Add(item.company_introduction);
                
                Result["建立日期"].Add(string.Format("{0:yyyy/MM/dd}", item.create_date));
            }

            return Result;
        }

        public ActionResult Export2()
        {
            List<supplier> list = model.GetList();
            var ExportList = GetExportList(list);
            int RowCount = list.Count;
            var ExcelStream = CreateVerticalExcel(RowCount, ExportList, 1, 2);


            var ZipFileName = DateTime.Now.ToString("yyyy_MM_dd HH_mm_ss") + ".zip";
            var TmpPath = Server.MapPath(string.Format("/temp/{0}", ZipFileName));

            using (var Zip = ZipFile.Open(TmpPath, ZipArchiveMode.Create))
            {
                var Excel = Zip.CreateEntry("Supplier.xlsx");
                using (var Stream = Excel.Open())
                {
                    ExcelStream.CopyTo(Stream);
                }
                foreach (var Item in list)
                {
                    try
                    {
                        var OriginalPath = Server.MapPath(Url.Content("~/" + this.website_path + "/")) + Item.filename;
                        var ZipFile = Zip.CreateEntryFromFile(OriginalPath, string.Format("{0}_{1}", Item.id, Item.filename));
                    } catch ( Exception )
                    {

                    }
                    
                }
            }

            var FS = new FileStream(TmpPath, FileMode.Open, FileAccess.Read);

            return File(FS, "application/octet-stream", "Suppliers.zip");
        }

        public FileContentResult Export()
        {
            var csv = new StringBuilder();

            
            var newLine = "#,Company Full Name,Government Business Registration Number,Country of Registration,Company Website,Telephone (+ Country Code),Fax,Registration Address,Capital,Annual Revenue,Name,Gender,Title,Email,Telephone,Business Model,Business Varieties,Major Customers,QMS/Product,Global Environment,Safety/Health,Security,Company Introduction,Create Date";
            csv.AppendLine(newLine);

            string company = "", business_register_number = "", country = "", website = "", tel = "", fax = "", address = "", name = "", gender = "", title = "", email = "", contact_tel = "", temp_business_model = "", temp_business_varieties = "", major_customers = "", qms = "", environment = "", health = "", securities = "", company_introduction = "";
            


            int i = 1;
            List<supplier> list = model.GetList();
            char[] charsToTrim = { ',', ' ' };
            foreach (supplier item in list)
            {
                company = string.IsNullOrEmpty(item.company) ? "" : item.company.Trim(charsToTrim);
                business_register_number = string.IsNullOrEmpty(item.business_register_number) ? "" : item.business_register_number.Trim(charsToTrim);
                country = string.IsNullOrEmpty(item.country) ? "" : item.country.Trim(charsToTrim);
                website = string.IsNullOrEmpty(item.website) ? "" : item.website.Trim(charsToTrim);
                tel = string.IsNullOrEmpty(item.tel) ? item.country_code + " " + item.tel : item.country_code + " " + item.tel.Trim(charsToTrim);
                fax = string.IsNullOrEmpty(item.fax) ? "" : item.fax.Trim(charsToTrim);
                address = string.IsNullOrEmpty(item.address) ? "" : item.address.Trim(charsToTrim);
                name = string.IsNullOrEmpty(item.name) ? "" : item.name.Trim(charsToTrim);
                gender = item.gender == 0 ? "Ms." : "Mr.";
                title = string.IsNullOrEmpty(item.title) ? "" : item.title.Trim(charsToTrim);
                email = string.IsNullOrEmpty(item.email) ? "" : item.email.Trim(charsToTrim);
                contact_tel = string.IsNullOrEmpty(item.contact_tel) ? "" : "+" + item.contact_tel_country_code + " " + item.contact_tel.Trim(charsToTrim);
                if (! string.IsNullOrEmpty(contact_tel) && ! string.IsNullOrEmpty(item.ext))
                {
                    contact_tel = contact_tel + "(" + item.ext.Trim(charsToTrim) + ")";
                }
                major_customers = string.IsNullOrEmpty(item.major_customers) ? "" : item.major_customers.Trim(charsToTrim);
                qms = string.IsNullOrEmpty(item.qms) ? "" : item.qms.Trim(charsToTrim);
                environment = string.IsNullOrEmpty(item.environment) ? "" : item.environment.Trim(charsToTrim);
                health = string.IsNullOrEmpty(item.health) ? "" : item.health.Trim(charsToTrim);
                securities = string.IsNullOrEmpty(item.securities) ? "" : item.securities.Trim(charsToTrim);
                company_introduction = string.IsNullOrEmpty(item.company_introduction) ? "" : item.company_introduction.Trim(charsToTrim);
                switch (item.business_model)
                {
                    case 1:
                        temp_business_model = "Agency- please list distributed brand:" + item.business_model_remark.Trim(charsToTrim);
                        break;

                    case 2:
                        temp_business_model = "Broker";
                        break;

                    default:
                        temp_business_model = "Manufacturer";
                        break;
                }
                switch (item.business_model)
                {
                    case 1:
                        temp_business_varieties = "Equipment/Machine- please list equipment:" + item.business_varieties_remark.Trim(charsToTrim);
                        break;

                    case 2:
                        temp_business_varieties = "Construction/Interior Design/Repair";
                        break;

                    case 3:
                        temp_business_varieties = "Environmental Protection or Sanitary/Waste Processor";
                        break;

                    case 4:
                        temp_business_varieties = "IT";
                        break;

                    case 5:
                        temp_business_varieties = "Transportation";
                        break;

                    case 6:
                        temp_business_varieties = "Human Resource Agency";
                        break;

                    case 7:
                        temp_business_varieties = "Others- please describe:" + item.business_varieties_remark.Trim(charsToTrim);
                        break;

                    default:
                        temp_business_varieties = "Material- please list material name:" + item.business_varieties_remark.Trim(charsToTrim);
                        break;
                }

                newLine = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23}", i,
                item.company,
                business_register_number,
                country,
                website,
                tel,
                fax,
                address,
                item.capital_currency + " " + item.capital,
                item.annual_revenue_currency + " " + item.annual_revenue,
                name,
                gender,
                title,
                email,
                contact_tel,
                temp_business_model,
                temp_business_varieties,
                major_customers,
                qms,
                environment,
                health,
                securities,
                company_introduction,
                string.Format("{0:yyyy/MM/dd}", item.create_date));
                
                csv.AppendLine(newLine);
                i++;
            }
            return File(Encoding.Default.GetBytes(csv.ToString()), "text/csv", "Suppliers.csv");
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Content(FormCollection fc)
        {
            Init();

            supplier data = FcToData(fc);
            if (UpdateData(data))
            {
                ShowUpdateMessage();
                return RedirectToAction("List");
            }
            else
            {
                ShowUpdateFail();
            }

            ViewBag.country_list = _getCountry();
            this.ratio.Add(this.GetRatioSize(1920, 1920));
            ViewBag.ratio = ratio;

            return View(data);
        }

        private supplier FcToData(FormCollection fc)
        {
            this.fc = fc;
            supplier data = new supplier();

            long id = Tool.TryToParseLong(fc["id"]);
            if (id > 0)
            {
                data = model.GetData(id);
            }
            else
            {
                data.create_date = DateTime.Now;
            }

            data.company = fc["company"].Trim();
            data.business_register_number = fc["business_register_number"].Trim();
            data.country = fc["country"].Trim();
            data.website = fc["website"].Trim();
            data.tel = fc["tel"].Trim();
            data.fax = fc["fax"].Trim();
            data.address = fc["address"].Trim();
            data.capital = Tool.TryToParseInt(fc["capital"]);
            data.annual_revenue = Tool.TryToParseInt(fc["annual_revenue"]);
            data.name = fc["name"].Trim();
            data.gender = Tool.TryToParseInt(fc["gender"]);
            data.title = fc["title"].Trim();
            data.email = fc["email"].Trim();
            data.contact_tel = fc["contact_tel"].Trim();
            data.business_model = Tool.TryToParseInt(fc["business_model"]);
            data.business_model_remark = fc["business_model_remark"].Trim();
            data.business_varieties = Tool.TryToParseInt(fc["business_varieties"]);
            data.business_varieties_remark = fc["business_varieties_remark"].Trim();
            data.major_customers = fc["major_customers"].Trim();
            data.management_system = fc["management_system"].Trim();
            data.management_system_remark = fc["management_system_remark"].Trim();
            data.company_introduction = fc["company_introduction"].Trim();
            string filename = fc["filename"].Replace("/" + media_bank_path, "").Replace("/" + website_path, "");
            string old_filename = fc["old_filename"].Replace("/" + media_bank_path, "").Replace("/" + website_path, "");
            if (!string.IsNullOrEmpty(filename) && filename != old_filename)
            {
                if (filename.LastIndexOf('/') >= 0)
                {
                    filename = filename.Substring(filename.LastIndexOf('/') + 1);
                }
            }
            data.filename = filename;
            data.update_date = DateTime.Now;

            return data;
        }

        private bool UpdateData(supplier data)
        {
            Init();
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            #region 驗證
            if (fc["company"] != null && string.IsNullOrEmpty(data.company))
            {
                is_valid = false;
                error.Add("company", "Necessary");
            }
            if (fc["business_register_number"] != null && string.IsNullOrEmpty(data.business_register_number))
            {
                is_valid = false;
                error.Add("business_register_number", "Necessary");
            }
            if (fc["name"] != null && string.IsNullOrEmpty(data.name))
            {
                is_valid = false;
                error.Add("name", "Necessary");
            }
            if (fc["contact_tel"] != null && string.IsNullOrEmpty(data.contact_tel))
            {
                is_valid = false;
                error.Add("contact_tel", "Necessary");
            }
            #endregion

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                data.filename = dealFile("filename");
                affected_row += model.SaveData(data);

                if (affected_row > 0)
                {
                    return true;
                }
                string security_cate = data.id == 0 ? "insert" : "update";
                SecurityLog(security_cate, web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "supplier", data.id);
            }
            return false;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteData(long param1)
        {
            if (param1 > 0)
            {
                Init();

                Dictionary<string, object> result = model.DeleteData(param1, Server.MapPath("~/" + website_path));

                int affected_row = (int)result["affected_row"];
                if (affected_row > 0)
                {
                    ShowUpdateMessage();
                }
                else
                {
                    ShowUpdateFail();
                }
                SecurityLog("delete", web_admin.ae_empnm + "[" + web_admin.ae_id + "]", "supplier", param1);
            }
            return RedirectToAction("List");
        }

        private List<string> _getCountry()
        {
            List<string> country_list = new List<string>();
            CultureInfo[] CInfoList = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo CInfo in CInfoList)
            {
                RegionInfo R = new RegionInfo(CInfo.LCID);
                if (!(country_list.Contains(R.EnglishName)))
                {
                    country_list.Add(R.EnglishName);
                }
            }
            country_list.Sort();
            return country_list;
        }

        public ActionResult Downloads(long param1)
        {
            Init();

            supplier data = model.GetData(param1);
            string file_path = Server.MapPath(Url.Content("~/" + this.website_path + "/")) + data.filename;

            FileInfo fi = new FileInfo(file_path);
            string extension = Path.GetExtension(file_path);
            Stream iStream = new FileStream(file_path, FileMode.Open, FileAccess.Read, FileShare.Read);
            return File(iStream, "application/unknown", data.title + extension);
        }

    }
}