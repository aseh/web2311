﻿using System.Collections.Generic;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class admin_emp_belong_group_model : base_model
    {
        public admin_emp_belong_group GetData(long aebg_id)
        {
            admin_emp_belong_group data = new admin_emp_belong_group();
            var query = from item in db.admin_emp_belong_group
                        where item.aebg_id == aebg_id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.aebg_id == 0)
            {
                data = new admin_emp_belong_group();
            }
            return data;
        }

        public List<admin_emp_belong_group> GetList(long admin_id)
        {
            List<admin_emp_belong_group> list = new List<admin_emp_belong_group>();
            var query = from item in db.admin_emp_belong_group
                        where item.ae_id == admin_id
                        select item;
            list = query.ToList();
            return list;
        }

        public int SaveData(admin_emp_belong_group update_data)
        {
            admin_emp_belong_group data = new admin_emp_belong_group();
            if (update_data.aebg_id == 0)
            {
                db.admin_emp_belong_group.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.aebg_id);
                data = update_data;
            }
            return db.SaveChanges();
        }

        public int DeleteData(long aebg_id)
        {
            admin_emp_belong_group data = this.GetData(aebg_id);
            db.admin_emp_belong_group.Remove(data);
            return db.SaveChanges();
        }
    }
}
