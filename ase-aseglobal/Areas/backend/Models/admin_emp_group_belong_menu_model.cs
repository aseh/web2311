﻿using System.Collections.Generic;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class admin_emp_group_belong_menu_model : base_model
    {
        public admin_emp_group_belong_menu GetData(long aegbm_id)
        {
            admin_emp_group_belong_menu data = new admin_emp_group_belong_menu();
            var query = from item in db.admin_emp_group_belong_menu
                        where item.aegbm_id == aegbm_id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.aegbm_id == 0)
            {
                data = new admin_emp_group_belong_menu();
            }
            return data;
        }

        public List<admin_emp_group_belong_menu> GetList(long group_id)
        {
            List<admin_emp_group_belong_menu> list = new List<admin_emp_group_belong_menu>();
            var query = from item in db.admin_emp_group_belong_menu
                        where item.aeg_id == group_id
                        select item;
            list = query.ToList();
            return list;
        }

        public int SaveData(admin_emp_group_belong_menu update_data)
        {
            admin_emp_group_belong_menu data = new admin_emp_group_belong_menu();
            if (update_data.aegbm_id == 0)
            {
                db.admin_emp_group_belong_menu.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.aegbm_id);
                data = update_data;
            }
            return db.SaveChanges();
        }

        public int DeleteData(long aegbm_id)
        {
            admin_emp_group_belong_menu data = this.GetData(aegbm_id);
            db.admin_emp_group_belong_menu.Remove(data);
            return db.SaveChanges();
        }
    }
}
