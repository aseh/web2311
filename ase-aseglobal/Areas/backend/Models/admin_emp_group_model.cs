﻿using System;
using System.Collections.Generic;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class admin_emp_group_model : base_model
    {
        public admin_emp_group GetData(long aeg_id)
        {
            admin_emp_group data = new admin_emp_group();
            var query = from item in db.admin_emp_group
                        where item.aeg_id == aeg_id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.aeg_id == 0)
            {
                data = new admin_emp_group();
            }
            return data;
        }

        public List<admin_emp_group> GetList()
        {
            List<admin_emp_group> list = new List<admin_emp_group>();
            var query = from item in db.admin_emp_group
                        select item;
            list = query.ToList();
            return list;
        }

        public int SaveData(admin_emp_group update_data)
        {
            admin_emp_group data = new admin_emp_group();
            if (update_data.aeg_id == 0)
            {
                db.admin_emp_group.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.aeg_id);
                data = update_data;
            }
            return db.SaveChanges();
        }
        
        public Dictionary<String, object> DeleteData(long id, string save_path)
        {
            Dictionary<String, object> result = new Dictionary<String, object>();
            int i_updateRows = 0;

            admin_emp_group data = this.GetData(id);

            db.admin_emp_group.Remove(data);
            i_updateRows = db.SaveChanges();
            result.Add("affected_row", i_updateRows);

            return result;
        }

        public byte ToggleLaunch(long id = 0)
        {
            admin_emp_group item = this.GetData(id);
            if (item.aeg_launch == 1)
            {
                item.aeg_launch = 0;
            }
            else
            {
                item.aeg_launch = 1;
            }
            db.SaveChanges();
            return item.aeg_launch;
        }
    }
}
