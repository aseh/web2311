﻿using System;
using System.Collections.Generic;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class admin_emp_model : base_model
    {
        public admin_emp GetData(long ae_id)
        {
            admin_emp data = new admin_emp();
            var query = from item in db.admin_emp
                        where item.ae_id == ae_id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.ae_id == 0)
            {
                data = new admin_emp();
            }
            return data;
        }
        public admin_emp GetDataByAccount(string account_id)
        {
            admin_emp data = new admin_emp();
            var query = from item in db.admin_emp
                        where item.ae_name == account_id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.ae_id == 0)
            {
                data = new admin_emp();
            }
            return data;
        }

        public List<admin_emp> GetList()
        {
            List<admin_emp> list = new List<admin_emp>();
            var query = from item in db.admin_emp
                        select item;
            list = query.ToList();
            return list;
        }

        public int SaveData(admin_emp update_data)
        {
            admin_emp data = new admin_emp();
            if (update_data.ae_id == 0)
            {
                db.admin_emp.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.ae_id);
                data = update_data;
            }
            return db.SaveChanges();
        }
        
        public Dictionary<String, object> DeleteData(long id, string save_path)
        {
            Dictionary<String, object> result = new Dictionary<String, object>();
            int i_updateRows = 0;

            admin_emp data = this.GetData(id);

            db.admin_emp.Remove(data);
            i_updateRows = db.SaveChanges();
            result.Add("affected_row", i_updateRows);

            return result;
        }

        public byte ToggleLaunch(long id = 0)
        {
            admin_emp item = this.GetData(id);
            if (item.ae_launch == 1)
            {
                item.ae_launch = 0;
            }
            else
            {
                item.ae_launch = 1;
            }
            db.SaveChanges();
            return item.ae_launch;
        }
    }
}
