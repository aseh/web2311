﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using web.Models;

namespace web.Areas.backend.Models
{
    public class blog_author_model:base_model
    {
        public blog_author_model()
        {

        }
        public blog_author GetData(long id)
        {
            return db.blog_author.Where(u => u.id == id).FirstOrDefault();
        }
        public blog_author_dtl GetDtlData(long id)
        {
            return db.blog_author_dtl.Where(u => u.id == id).FirstOrDefault();
        }

        public blog_author_dtl GetDtlDataByName(string name,string lang)
        {
            return db.blog_author_dtl.Where(u => u.name == name && u.lang==lang).FirstOrDefault();
        }

        public List<blog_author> GetList(string keyword = "", int sort = 0)
        {
            List<blog_author> list = new List<blog_author>();
            list = db.blog_author.Where(u => u.blog_author_dtl.Select(d=>d.name).Contains(keyword) || u.blog_author_dtl.Select(d => d.brieftext).Contains(keyword) || keyword == "").ToList();

            switch (sort)
            {
                case 1:
                    list = list.OrderBy(x => x.create_date).ToList();
                    break;

                    //default:
                    //    list = query;
                    //    break;
            }
            return list;
        }

        public List<blog_author_dtl> GetDtlList(string keyword = "", int sort = 0)
        {
            List<blog_author_dtl> list = new List<blog_author_dtl>();
            //排除掉未設定的author ==>先不排除
            //blog_author_dtl tmp = db.blog_author_dtl.Where(u => u.name == "(none)").FirstOrDefault();
            //
            var query = db.blog_author_dtl
                .Where(u => u.name.Contains(keyword) || u.brieftext.Contains(keyword) || keyword == "")
                .ToList();

            //if (tmp != null)
            //{
            //    query = query.Where(u => u.blog_author.id != tmp.blog_author.id).ToList();
            //}

            switch (sort)
            {
                case 1:
                    list = (query.Count > 0) ? query.OrderBy(x => x.authorid).OrderBy(x => x.lang).ToList() : list;
                    break;

                default:
                    list = query;
                    break;
            }
            return list;
        }

        public List<SelectListItem> GetAuthorList(string lang,long authorid)
        {
            List<SelectListItem> Authorlist = new List<SelectListItem>();
            List<blog_author_dtl> authordtl = GetDtlList("").Where(u => u.lang == lang).ToList();
            //排除掉未設定的author ==>先不排除
            //blog_author_dtl tmp = db.blog_author_dtl.Where(u => u.name == "(none)").FirstOrDefault();
            //if (tmp != null)
            //{
            //    authordtl = authordtl.Where(u => u.blog_author.id != tmp.blog_author.id).ToList();
            //}
            if (authordtl.Count > 0)
            {
                foreach (var a in authordtl)
                {
                    SelectListItem li = new SelectListItem();
                    li.Text = a.name;
                    li.Value = a.authorid.ToString();
                    li.Selected = (a.authorid == authorid);
                    Authorlist.Add(li);
                }
            }
            SelectListItem al = new SelectListItem(); al.Text = "選擇作者"; al.Value = "0";
            Authorlist.Insert(0, al);
            return Authorlist;
        }

        public int SaveData(blog_author update_data)
        {
            blog_author data = new blog_author();
            if (update_data.id == 0)
            {
                data = update_data;
                db.blog_author.Add(data);
                //foreach(var dtl in update_data.blog_author_dtl)
                //{
                //    dtl.authorid = data.id;
                //    dtl.create_date = DateTime.Now;
                //    db.blog_author_dtl.Add(dtl);
                //}
            }
            else
            {
                data = this.GetData(update_data.id);
                data = update_data;
                //為了修正disable的欄位 postback會成為空值
                blog_author_dtl tmp1 = db.blog_author_dtl.Where(u => u.name == "(未設定)").FirstOrDefault();
                string name1 = "(未設定)";
                blog_author_dtl tmp2 = db.blog_author_dtl.Where(u => u.name == "(none)").FirstOrDefault();
                string name2 = "(none)";
                foreach (var dtl in update_data.blog_author_dtl)
                {
                    blog_author_dtl data_dtl = GetDtlData(dtl.id);
                    data_dtl = dtl;
                    if (data_dtl.id == tmp1.id) data_dtl.name = name1;
                    if (data_dtl.id == tmp2.id) data_dtl.name = name2;
                    data_dtl.update_date= DateTime.Now;
                }
            }
            return db.SaveChanges();
        }

        public blog_author_dtl DefaultNone()
        {
            return db.blog_author_dtl.Where(u => u.name == "(none)").FirstOrDefault();
        }

        public string DeleteData(long id)
        {
            string errmsg = "";
            try
            {
                blog_author chk = GetData(id);
                //delete dtl ... blog如何處理? ==>指定為未設定的author 
                blog_author_dtl tmp = db.blog_author_dtl.Where(u => u.name == "(none)").FirstOrDefault();
                if (id == tmp.authorid) throw new Exception("預設類別不可刪除");
                if (chk.blog.Count > 0)
                {
                    foreach (var b in chk.blog)
                    {
                        b.authorid = tmp.authorid;
                    }
                }

                //
                foreach (var adtl in chk.blog_author_dtl.ToArray())
                {
                    db.blog_author_dtl.Remove(adtl);
                }

                db.blog_author.Remove(chk);
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                errmsg = "刪除作業失敗:" + ex.Message;
            }
            return errmsg;
        }
    }


}