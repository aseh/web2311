﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using web.Models;

namespace web.Areas.backend.Models
{
    public class blog_model : base_model
    {
        public blog_model()
        {

        }

        public blog GetData(long id)
        {
            blog data = new blog();
            var query = from item in db.blog
                        where item.id == id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new blog();
            }
            return data;
        }
        public blog GetDataByToporder(int order)
        {
            blog data = db.blog.Where(u => u.toporder == order).FirstOrDefault();
            return data;
        }

        public List<blog_dtl> GetDtlList(int ddlAuthor = 0, int ddlType = 0, string keyword = "", int sort = 0)
        {
            blog_author_model amodel = new blog_author_model();
            List<blog_dtl> list = new List<blog_dtl>();
            List<blog_dtl> res = new List<blog_dtl>();
            blog_type_model tmodel = new blog_type_model();
            //blog_type type = tmodel.GetDataByChname("未分類");
            //long[] alist = amodel.GetDtlList(keyword).Where(u => u.name.Contains(keyword) || keyword == "").Select(u => u.authorid).ToArray();
            list = db.blog_dtl.Where(u => u.contenttext != "" && (u.title.Contains(keyword) || u.contenttext.Contains(keyword) || keyword == ""))
                .Where(u => u.blog.authorid == ddlAuthor || ddlAuthor == 0)
                .Where(u => u.blog.typeid == ddlType || ddlType == 0).ToList();
            res = list.Where(u => u.blog.toporder > 0).OrderBy(u => u.blog.toporder).Concat(list.Where(u => u.blog.toporder == 0).OrderByDescending(u => u.blog.create_date)).ToList();
            //.Where(u => u.blog.typeid == ddlType || ddlType == 0 || ddlType == type.id).ToList();
            switch (sort)
            {
                case 1:
                    res = list.OrderBy(x => x.create_date).ToList();
                    break;

                    //default:
                    //    list = query;
                    //    break;
            }
            return res;
        }

        //public List<blog_message> GetMsgList(long id = 0)
        //{
        //    List<blog_message> list = new List<blog_message>();
        //    list = db.blog_message.Where(u => (u.name.Contains(keyword) || u.email.Contains(keyword) || u.message.Contains(keyword)
        //    || u.ae_empnm.Contains(keyword) || u.reply.Contains(keyword) || keyword == "") && (blogid == 0 || u.blog.id == blogid)).ToList();
        //    switch (sort)
        //    {
        //        case 1:
        //            list = list.OrderBy(x => x.create_date).ToList();
        //            break;

        //            //default:
        //            //    list = query;
        //            //    break;
        //    }
        //    return list;
        //}

        //move to front
        public void addLogData(long blogid)
        {
            blog_log data = new blog_log();
            data.blogid = blogid;
            data.ipaddress = HttpContext.Current.Request.UserHostAddress;
            data.create_date = DateTime.Now;
            db.blog_log.Add(data);
            db.SaveChanges();
        }
        public blog GetDataByUrlid(string urlid)
        {
            blog data = new blog();
            data = db.blog.Where(u => u.urlid==urlid).FirstOrDefault();
            return data;
        }

        public blog_dtl GetDtlDataByTitle(string title,string lang)
        {
            blog_dtl data = new blog_dtl();
            data = db.blog_dtl.Where(u => u.title == title && u.lang == lang).FirstOrDefault();
            return data;
        }

        public blog_message GetMessageById(long id)
        {
            blog_message data = new blog_message();
            data = db.blog_message.Where(u => u.id == id).FirstOrDefault();
            return data;
        }

        public blog_dtl GetDtlDataById(long id)
        {
            blog_dtl data = new blog_dtl();
            data = db.blog_dtl.Where(u => u.id == id).FirstOrDefault();
            return data;
        }

        public int SaveData(blog update_data)
        {
            blog data = new blog();
            
            if (update_data.toporder > 0)
            {
                //檢查目前是否有此排名的文章
                blog tmp = GetDataByToporder((int)update_data.toporder);
                if (tmp != null && tmp.id != update_data.id)
                {
                    tmp.toporder = 0;
                }
            }
            if (update_data.id == 0)
            {
                data = update_data;
                db.blog.Add(data);
            }
            else
            {
                data = this.GetData(update_data.id);
                data = update_data;
                foreach (var dtl in update_data.blog_dtl)
                {
                    blog_dtl data_dtl = GetDtlDataById(dtl.id);
                    data_dtl = dtl;
                    data_dtl.update_date = DateTime.Now;
                }
            }
            return db.SaveChanges();
        }

        public string DeleteData(long id)
        {
            string errmsg = "";
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    blog chk = GetData(id);
                    foreach(var l in chk.blog_log.ToArray())
                    {
                        db.blog_log.Remove(l);
                    }
                    foreach (var m in chk.blog_message.ToArray())
                    {
                        db.blog_message.Remove(m);
                    }
                    foreach (var d in chk.blog_dtl.ToArray())
                    {
                        db.blog_dtl.Remove(d);
                    }

                    db.blog.Remove(chk);
                    db.SaveChanges();
                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    errmsg = "刪除作業失敗:" + ex.Message;
                }
                
            }
           
            return errmsg;
        }

        public BaseResponse DeleteMessage(long id)
        {
            BaseResponse res = new BaseResponse();
            try
            {
                blog_message msg = GetMessageById(id);
                db.blog_message.Remove(msg);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                res.code = 400;res.message = "error:" + ex.Message;
            }

            return res;
        }

        public byte UpdateAllowmessage(long id)
        {
            blog item = this.GetData(id);
            if (item.allowmsg == 1)
            {
                item.allowmsg = 0;
            }
            else
            {
                item.allowmsg = 1;
            }
            db.SaveChanges();
            return (byte)item.allowmsg;
        }

        public byte UpdateLauch(long id)
        {
            blog item = this.GetData(id);
            if (item.launch == 1)
            {
                item.launch = 0;
            }
            else
            {
                item.launch = 1;
            }
            db.SaveChanges();
            return (byte)item.launch;
        }

        public bool ChkDupToporder(long id, int toporder)
        {
            bool res = false;
            if (toporder != 0)
            {
                blog item = this.GetDataByToporder(toporder);
                res = (item != null && id != item.id);
            }

            return res;
        }

        public BaseResponse UpdateReply(long id, string reply, long adminid, string ae_empnm)
        {
            BaseResponse res = new BaseResponse();
            if (reply==null || reply == "")
            {
                res.code = 400;res.message = "reply is necessary";// "回覆不可空白";
            }
            else
            {
                blog_message item = this.GetMessageById(id);
                if (item != null)
                {
                    item.reply = reply;
                    item.adminid = adminid;
                    item.ae_empnm = ae_empnm;
                    item.update_date = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    res.code = 400; res.message = "record not found!";
                }
            }
           
            return res;
        }

        public void UpdateBlogLaunch()
        {
            DateTime sd = DateTime.Now.AddHours(-1);
            DateTime ed = DateTime.Now.AddHours(1);
            var blogs = db.blog.Where(u => u.launch == 0 && u.apply_date != null && u.apply_date>sd && u.apply_date<ed).ToList();
            foreach (var b in blogs)
            {
                b.launch = 1;
            }
            db.SaveChanges();
        }
    }

    //public class blog_content:blog
    //{
    //    public List<blog_dtl> blog_Dtls { get; set; }
    //}
}