﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using web.Models;

namespace web.Areas.backend.Models
{
    public class blog_type_model : base_model
    {
        public blog_type_model()
        {

        }

        public List<blog_type> GetList(string keyword = "", int sort = 0)
        {
            List<blog_type> list = new List<blog_type>();
            var query = db.blog_type.Where(u => u.name.ToUpper().Contains(keyword.ToUpper()) || keyword == "").ToList();

            switch (sort)
            {
                case 1:
                    list =(query.Count>0)? query.OrderBy(x => x.create_date).ToList():list;
                    break;

                default:
                    list = query;
                    break;
            }
            return list;
        }

        public blog_type GetData(long id)
        {
            return db.blog_type.Where(u => u.id == id).FirstOrDefault();
        }

        public Dictionary<String, object> AddData(string name)
        {
            Dictionary<String, object> result = new Dictionary<String, object>();
            int i_affectedRows = 0;

            blog_type type = new blog_type();
            type.name = name;
            db.blog_type.Add(type);

            i_affectedRows = db.SaveChanges();
            result.Add("affected_row", i_affectedRows);

            return result;
        }

        public Dictionary<String, object> UpdateData(long id, string name)
        {
            Dictionary<String, object> result = new Dictionary<String, object>();
            int i_affectedRows = 0;

            blog_type type = db.blog_type.Where(u => u.id == id).FirstOrDefault();
            if (type != null)
            {
                type.name = name;
                i_affectedRows = db.SaveChanges();
            }
            result.Add("affected_row", i_affectedRows);

            return result;
        }

        public blog_type GetDataByChname(string ch_name)
        {
            return db.blog_type.Where(u => u.name.IndexOf("\"ch\":\""+ch_name+"\"")>0).FirstOrDefault();
        }
        public blog_type GetDataByEnname(string en_name)
        {
            return db.blog_type.Where(u => u.name.ToUpper().IndexOf("\"en\":\"" + en_name.ToUpper() + "\"") > 0).FirstOrDefault();
        }

        public string DeleteData(long id)
        {
            string errmsg = "";
            try
            {
                blog_type chk = GetData(id);
                //要刪除的分類底下所有文章改為 未分類
                if (chk.blog.Count > 0)
                {
                    blog_type all = GetList(":\"未分類\"").FirstOrDefault();
                    foreach (var p in chk.blog)
                    {
                        p.typeid = all.id;
                    }
                }
                db.blog_type.Remove(chk);
                db.SaveChanges();

            }
            catch (Exception ex)
            {
                errmsg = "刪除作業失敗:" + ex.Message;
            }
            return errmsg;
        }
        public List<adminblogtype> GetDatas(string keyword = "", int sort = 0)
        {
            List<adminblogtype> list = new List<adminblogtype>();
            var query = db.blog_type.Where(u => u.name.Contains(keyword) || keyword == "").ToList();
            if (query.Count > 0)
            {
                foreach (var q in query)
                {
                    adminblogtype at = new adminblogtype();
                    typename tn = JsonConvert.DeserializeObject<typename>(q.name);
                    at.ch_name = tn.ch;
                    at.en_name = tn.en;
                    at.id = q.id;
                    at.name = q.name;
                    at.create_date = q.create_date;
                    list.Add(at);
                }
            }
            switch (sort)
            {
                case 1:
                    list = list.OrderBy(x => x.create_date).ToList();
                    break;

                //default:
                //    list = query.ToList();
                //    break;
            }
            return list;
        }

        public List<SelectListItem> GetTypeList(string lang, long typeid)
        {
            List<SelectListItem> Typelist = new List<SelectListItem>();
            List<blog_type> types = GetList("").ToList();
            if (types.Count > 0)
            {
                foreach (var t in types)
                {
                    typename tn = JsonConvert.DeserializeObject<typename>(t.name);
                    SelectListItem li = new SelectListItem();
                    li.Text = (lang == "ch") ? tn.ch : tn.en;
                    li.Value = t.id.ToString();
                    li.Selected = (t.id == typeid);
                    Typelist.Add(li);
                }
            }
            return Typelist;
        }
    }

    public class adminblogtype:blog_type
    {
        public string en_name { get; set; }
        public string ch_name { get; set; }
    }
    public class typename
    {
        public string ch { get; set; }
        public string en { get; set; }
    }

    public class BaseResponse
    {
        public int code { get; set; } = 0;
        public string message { get; set; } = "OK";
    }
}