﻿using System;
using System.Collections.Generic;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class Contact_email_model : base_model
    {
        public contact_email GetData(long id)
        {
            contact_email data = new contact_email();
            var query = from item in db.contact_email
                        where item.id == id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new contact_email();
            }
            return data;
        }

        public List<contact_email> GetList()
        {
            List<contact_email> list = new List<contact_email>();
            var query = from item in db.contact_email
                        select item;
            list = query.ToList();
            return list;
        }

        public int SaveData(contact_email update_data)
        {
            contact_email data = new contact_email();
            if (update_data.id == 0)
            {
                db.contact_email.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.id);
                data = update_data;
            }
            return db.SaveChanges();
        }
        
        public Dictionary<String, object> DeleteData(long id, string save_path)
        {
            Dictionary<String, object> result = new Dictionary<String, object>();
            int i_updateRows = 0;

            contact_email data = this.GetData(id);
            
            db.contact_email.Remove(data);
            i_updateRows = db.SaveChanges();
            result.Add("affected_row", i_updateRows);

            return result;
        }

        public byte ToggleLaunch(long id = 0)
        {
            contact_email item = this.GetData(id);
            if (item.launch == 1)
            {
                item.launch = 0;
            }
            else
            {
                item.launch = 1;
            }
            db.SaveChanges();
            return item.launch;
        }
    }
}
