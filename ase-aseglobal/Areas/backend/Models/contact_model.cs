﻿using System.Collections.Generic;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class Contact_model : base_model
    {
        public Contact_model()
        {

        }

        public contact GetData(long id)
        {
            contact data = new contact();
            var query = from item in db.contact
                        where item.id == id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new contact();
            }
            return data;
        }

        public List<contact> GetList(int type = 0, string keyword = "", int sort = 0)
        {
            List<contact> list = new List<contact>();
            var query = from item in db.contact
                        where item.type == type || type == 0
                        where item.name.Contains(keyword) || keyword == ""
                        select item;

            switch (sort)
            {
                case 1:
                    list = query.OrderBy(x => x.type).ToList();
                    break;

                default:
                    list = query.ToList();
                    break;
            }
            return list;
        }

        public int SaveData(contact update_data)
        {
            contact data = new contact();
            if (update_data.id == 0)
            {
                db.contact.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.id);
                data = update_data;
            }
            return db.SaveChanges();
        }
        
    }
}
