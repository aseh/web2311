﻿using System;
using System.Collections.Generic;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class csr_news_model : base_model
    {
        public csr_news_model()
        {

        }
        
        public csr_news GetData(long id)
        {
            csr_news data = new csr_news();
            var query = from item in db.csr_news
                        where item.id == id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new csr_news();
            }
            return data;
        }

        public List<csr_news> GetList(int sort =1, DateTime? searchP = null, string keyword = "")
        {
            List<csr_news> list = new List<csr_news>();
            var query = from item in db.csr_news
                        where (searchP != null && searchP <= item.publish_date) || (searchP == null)
                        where item.title.Contains(keyword) || keyword == ""
                        select item;

            switch (sort)
            {
                case 1:
                    list = query.OrderByDescending(x => x.publish_date).ToList();
                    break;
                    
                default:
                    list = query.ToList();
                    break;
            }
            return list;
        }

        public int SaveData(csr_news update_data)
        {
            csr_news data = new csr_news();
            if (update_data.id == 0)
            {
                db.csr_news.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.id);
                data = update_data;
            }
            return db.SaveChanges();
        }
        
        public Dictionary<String, object> DeleteData(long id, string save_path)
        {
            Dictionary<String, object> result = new Dictionary<String, object>();
            int i_updateRows = 0;

            csr_news data = this.GetData(id);
            
            db.csr_news.Remove(data);
            i_updateRows = db.SaveChanges();
            result.Add("affected_row", i_updateRows);

            return result;
        }

        public byte ToggleLaunch(long id = 0)
        {
            csr_news item = this.GetData(id);
            if (item.launch == 1)
            {
                item.launch = 0;
            }
            else
            {
                item.launch = 1;
            }
            db.SaveChanges();
            return item.launch;
        }
    }
}
