﻿using System.Collections.Generic;
using System.Linq;
using web.Models;
using web.Common;

namespace web.Areas.backend.Models
{
    public class Csr_report_count_model : base_model
    {
        public List<csr_report_count> GetList(int year = 0, int month = 0)
        {
            List<csr_report_count> list = new List<csr_report_count>();
            var query = from item in db.csr_report_count
                        where item.create_date.Value.Year == year || year == 0
                        where item.create_date.Value.Month == month || month == 0
                        orderby item.report_id, item.create_date
                        select item;
            list = query.ToList();
            return list;
        }
        public List<int> GetYearList()
        {
            List<int> list = new List<int>();
            var query = from item in db.csr_report_count
                        group item by item.create_date.Value.Year;
            foreach (var item in query)
            {
                list.Add(item.Key);
            }
            return list;
        }
    }
    public class result
    {
        public long report_id { get; set; }
        public string title { get; set; }
        public int year { get; set; }
        public int month { get; set; }
        public int count { get; set; }
    }
}
