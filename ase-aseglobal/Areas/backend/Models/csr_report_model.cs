﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class Csr_report_model : base_model
    {
        public Csr_report_model()
        {

        }

        public csr_report GetData(long id)
        {
            csr_report data = new csr_report();
            var query = from item in db.csr_report
                        where item.id == id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new csr_report();
            }
            return data;
        }

        public List<csr_report> GetList()
        {
            List<csr_report> list = new List<csr_report>();
            var query = from item in db.csr_report
                        orderby item.create_date descending
                        select item;
            list = query.ToList();
            return list;
        }

        public int SaveData(csr_report update_data)
        {
            csr_report data = new csr_report();
            if (update_data.id == 0)
            {
                db.csr_report.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.id);
                data = update_data;
            }
            return db.SaveChanges();
        }

        public Dictionary<String, object> DeleteData(long id, string save_path)
        {
            Dictionary<String, object> result = new Dictionary<String, object>();
            int i_updateRows = 0;

            csr_report data = this.GetData(id);

            if (File.Exists(save_path + data.filename))
            {
                File.Delete(save_path + data.filename);
            }
            if (File.Exists(save_path + data.img_filename))
            {
                File.Delete(save_path + data.img_filename);
            }
            db.csr_report.Remove(data);
            i_updateRows = db.SaveChanges();
            result.Add("affected_row", i_updateRows);

            return result;
        }
        public byte ToggleLaunch(long id = 0)
        {
            csr_report item = this.GetData(id);
            if (item.launch == 1)
            {
                item.launch = 0;
            }
            else
            {
                item.launch = 1;
            }
            db.SaveChanges();
            return item.launch;
        }
    }
}
