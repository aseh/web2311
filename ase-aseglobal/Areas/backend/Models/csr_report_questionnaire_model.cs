﻿using System.Collections.Generic;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class Csr_report_questionnaire_model : base_model
    {

        public csr_report_questionnaire GetData(long id)
        {
            csr_report_questionnaire data = new csr_report_questionnaire();
            var query = from item in db.csr_report_questionnaire
                        where item.id == id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new csr_report_questionnaire();
            }
            return data;
        }

        public List<csr_report_questionnaire> GetList()
        {
            List<csr_report_questionnaire> list = new List<csr_report_questionnaire>();
            var query = from item in db.csr_report_questionnaire
                        select item;
            list = query.ToList();
            return list;
        }
        
    }
}
