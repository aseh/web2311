﻿using System;
using System.Collections.Generic;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class Event_model : base_model
    {
        public Event_model()
        {

        }

        public events GetData(long id)
        {
            events data = new events();
            var query = from item in db.events
                        where item.id == id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new events();
            }
            return data;
        }

        public List<events> GetList(int sort = 1, DateTime? searchS = null, DateTime? searchE = null, string keyword = "")
        {
            List<events> list = new List<events>();
            var query = from item in db.events
                        where (searchS != null && searchS <= item.start_date) || (searchS == null)
                        where (searchE != null && searchE >= item.end_date) || (searchE == null)
                        where item.title.Contains(keyword) || keyword == ""
                        select item;

            switch (sort)
            {
                case 1:
                    list = query.OrderByDescending(x => x.start_date).ToList();
                    break;

                case 2:
                    list = query.OrderByDescending(x => x.end_date).ToList();
                    break;

                default:
                    list = query.ToList();
                    break;
            }
            return list;
        }

        public int SaveData(events update_data)
        {
            events data = new events();
            if (update_data.id == 0)
            {
                db.events.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.id);
                data = update_data;
            }
            return db.SaveChanges();
        }
        
        public Dictionary<String, object> DeleteData(long id, string save_path)
        {
            Dictionary<String, object> result = new Dictionary<String, object>();
            int i_updateRows = 0;

            events data = this.GetData(id);
            
            db.events.Remove(data);
            i_updateRows = db.SaveChanges();
            result.Add("affected_row", i_updateRows);

            return result;
        }

        public byte ToggleLaunch(long id = 0)
        {
            events item = this.GetData(id);
            if (item.launch == 1)
            {
                item.launch = 0;
            }
            else
            {
                item.launch = 1;
            }
            db.SaveChanges();
            return item.launch;
        }
    }
}
