﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class Home_page_model : base_model
    {
        public Home_page_model()
        {

        }
        
        public home_page GetData(long id)
        {
            home_page data = new home_page();
            var query = from item in db.home_page
                        where item.id == id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new home_page();
            }
            return data;
        }

        public List<home_page> GetList()
        {
            List<home_page> list = new List<home_page>();
            var query = from item in db.home_page
                        select item;
            list = query.ToList();
            return list;
        }

        public int SaveData(home_page update_data)
        {
            home_page data = new home_page();
            if (update_data.id == 0)
            {
                db.home_page.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.id);
                data = update_data;
            }
            return db.SaveChanges();
        }

        public Dictionary<String, object> DeleteData(long id, string save_path)
        {
            Dictionary<String, object> result = new Dictionary<String, object>();
            int i_updateRows = 0;

            home_page data = this.GetData(id);

            if (File.Exists(save_path + data.banner_mobile))
            {
                File.Delete(save_path + data.banner_mobile);
            }
            if (File.Exists(save_path + data.banner_web))
            {
                File.Delete(save_path + data.banner_web);
            }
            db.home_page.Remove(data);
            i_updateRows = db.SaveChanges();
            result.Add("affected_row", i_updateRows);

            return result;
        }
    }
}
