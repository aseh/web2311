﻿using System;
using System.Collections.Generic;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class Ir_news_model : base_model
    {
        public Ir_news_model()
        {

        }
        
        public ir_news GetData(long id)
        {
            ir_news data = new ir_news();
            var query = from item in db.ir_news
                        where item.id == id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new ir_news();
            }
            return data;
        }

        public List<ir_news> GetList(int sort = 0, DateTime? searchS = null, DateTime? searchE = null, string keyword = "")
        {
            List<ir_news> list = new List<ir_news>();
            var query = from item in db.ir_news
                        where (searchS != null && searchS <= item.start_date) || (searchS == null)
                        where (searchE != null && searchE >= item.end_date) || (searchE == null)
                        where item.title.Contains(keyword) || keyword == ""
                        select item;
            
            switch (sort)
            {
                case 1:
                    list = query.OrderByDescending(x => x.start_date).ToList();
                    break;

                case 2:
                    list = query.OrderByDescending(x => x.end_date).ToList();
                    break;

                default:
                    list = query.ToList();
                    break;
            }
            return list;
        }

        public int SaveData(ir_news update_data)
        {
            ir_news data = new ir_news();
            if (update_data.id == 0)
            {
                db.ir_news.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.id);
                data = update_data;
            }
            return db.SaveChanges();
        }

        public Dictionary<String, object> DeleteData(long id, string save_path)
        {
            Dictionary<String, object> result = new Dictionary<String, object>();
            int i_updateRows = 0;

            ir_news data = this.GetData(id);

            db.ir_news.Remove(data);
            i_updateRows = db.SaveChanges();
            result.Add("affected_row", i_updateRows);

            return result;
        }
    }
}
