﻿using System;
using System.Collections.Generic;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class Press_room_model : base_model
    {
        public Press_room_model()
        {

        }

        public press_room GetData(long id)
        {
            press_room data = new press_room();
            var query = from item in db.press_room
                        where item.id == id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new press_room();
            }
            return data;
        }

        public press_room checkUrlID(string urlid)
        {
            press_room checkUrlData = new press_room();
            bool checkUrl = true;
            var query = from item in db.press_room
                        where item.urlid == urlid
                        select item;
            checkUrlData = query.FirstOrDefault();
            //if (query.Count() > 0)
            //{
            //    if (checkUrlData.urlid != urlid)
            //    {
            //        checkUrl = false;
            //    }
            //}
            return checkUrlData;
        }

        public List<press_room> GetList(int sort = 1, DateTime? searchP = null, string keyword = "")
        {
            List<press_room> list = new List<press_room>();
            var query = from item in db.press_room
                        where (searchP != null && searchP <= item.publish_date) || (searchP == null)
                        where item.title.Contains(keyword) || keyword == ""
                        select item;

            switch (sort)
            {
                case 1:
                    list = query.OrderByDescending(x => x.publish_date).ToList();
                    break;

                default:
                    list = query.ToList();
                    break;
            }
            return list;
        }

        public int SaveData(press_room update_data)
        {
            press_room data = new press_room();
            if (update_data.id == 0)
            {
                db.press_room.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.id);
                data = update_data;
            }
            return db.SaveChanges();
        }

        public Dictionary<String, object> DeleteData(long id, string save_path)
        {
            Dictionary<String, object> result = new Dictionary<String, object>();
            int i_updateRows = 0;

            press_room data = this.GetData(id);

            db.press_room.Remove(data);
            i_updateRows = db.SaveChanges();
            result.Add("affected_row", i_updateRows);

            return result;
        }

        public byte ToggleLaunch(long id = 0)
        {
            press_room item = this.GetData(id);
            if (item.launch == 1)
            {
                item.launch = 0;
            }
            else
            {
                item.launch = 1;
            }
            db.SaveChanges();
            return item.launch;
        }
    }
}
