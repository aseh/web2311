﻿using System;
using System.Collections.Generic;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class sign_model : base_model
    {
        public sign_model()
        {

        }

        public sign GetData(long id)
        {
            sign data = new sign();
            var query = from item in db.sign
                        where item.id == id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new sign();
            }
            return data;
        }

        public List<sign> GetList()
        {
            List<sign> list = new List<sign>();
            var query = from item in db.sign
                        select item;
            list = query.ToList();
            return list;
        }

        public int SaveData(sign update_data)
        {
            sign data = new sign();
            if (update_data.id == 0)
            {
                db.sign.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.id);
                data = update_data;
            }
            return db.SaveChanges();
        }
        
        public Dictionary<String, object> DeleteData(long id, string save_path)
        {
            Dictionary<String, object> result = new Dictionary<String, object>();
            int i_updateRows = 0;

            sign data = this.GetData(id);
            
            db.sign.Remove(data);
            i_updateRows = db.SaveChanges();
            result.Add("affected_row", i_updateRows);

            return result;
        }
        
    }
}
