﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class supplier_model : base_model
    {
        public supplier_model()
        {

        }

        public supplier GetData(long id)
        {
            supplier data = new supplier();
            var query = from item in db.supplier
                        where item.id == id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new supplier();
            }
            return data;
        }

        public List<supplier> GetList()
        {
            List<supplier> list = new List<supplier>();
            var query = from item in db.supplier
                        select item;
            list = query.ToList();
            return list;
        }

        public int SaveData(supplier update_data)
        {
            supplier data = new supplier();
            if (update_data.id == 0)
            {
                db.supplier.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.id);
                data = update_data;
            }
            return db.SaveChanges();
        }

        public Dictionary<String, object> DeleteData(long id, string save_path)
        {
            Dictionary<String, object> result = new Dictionary<String, object>();
            int i_updateRows = 0;

            supplier data = this.GetData(id);

            if (File.Exists(save_path + data.filename))
            {
                File.Delete(save_path + data.filename);
            }
            db.supplier.Remove(data);
            i_updateRows = db.SaveChanges();
            result.Add("affected_row", i_updateRows);

            return result;
        }
    }
}
