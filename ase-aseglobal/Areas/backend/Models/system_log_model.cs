﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using web.Models;

namespace web.Areas.backend.Models
{
    public class system_log_model : base_model
    {
        public system_log getData(long sl_id)
        {
            system_log data = new system_log();
            var query = from item in db.system_log
                        where item.sl_id == sl_id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.ae_id == 0)
            {
                data = new system_log();
            }
            return data;
        }

        public List<system_log> getList()
        {
            List<system_log> list = new List<system_log>();
            var query = from item in db.system_log
                        select item;
            list = query.ToList();
            return list;
        }

        public int saveData(system_log update_data)
        {
            system_log data = new system_log();
            if (update_data.ae_id == 0)
            {
                db.system_log.Add(update_data);
            }
            else
            {
                data = this.getData(update_data.sl_id);
                data = update_data;
            }
            return db.SaveChanges();
        }

        public void addData(long ae_id, string category, string content)
        {
            system_log data = new system_log();
            data.ae_id = ae_id;
            data.sl_type = 0;
            data.sl_category = category;
            data.sl_content = content;
            data.sl_ip = HttpContext.Current.Request.UserHostAddress;
            data.sl_create_time = DateTime.Now;
            db.system_log.Add(data);
            db.SaveChanges();
        }
    }
}
