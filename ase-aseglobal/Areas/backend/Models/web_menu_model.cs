﻿using System.Collections.Generic;
using System.Linq;
using web.Models;

namespace web.Areas.backend.Models
{
    public class web_menu_model : base_model
    {
        public web_menu getCurrentMenuData(string url_id, string controller)
        {
            web_menu data = new web_menu();
            var query = from item in db.web_menu
                        where item.wm_url_id == url_id || url_id == ""
                        where item.wm_controller == controller
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.wm_id == 0)
            {
                data = new web_menu();
            }
            return data;
        }

        public List<web_menu> GetList(int wm_category_id = 0)
        {
            List<web_menu> list = new List<web_menu>();
            var query = from item in db.web_menu
                        where item.wm_category_id == wm_category_id || wm_category_id == 0
                        where item.wm_display == 1
                        orderby item.wm_category_id, item.wm_url_id, item.wm_parent_id, item.wm_sort
                        select item;
            list = query.ToList();
            return list;
        }
    }
}