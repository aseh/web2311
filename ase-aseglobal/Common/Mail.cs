﻿using System;
using System.Net.Mail;

namespace web.Common
{
    public class Mail
    {
        private string sender_email;
        private string sender_name;
        private string smtp_host;
        private string username;
        private string password;
        private int port;
        public MailMessage mail_message;
        public Mail()
        {
            sender_email = "ct15504@dm.cloudmax.com.tw";
            sender_name = "ASE Group";
            smtp_host = "dm.cloudmax.com.tw";
            username = "ct15504@dm.cloudmax.com.tw";
            password = "Q3^@08D5";
            port = 2525;
        }

        //--------------------------------------------------------------------
        //step 1
        public void  InitMail(string sSubject, string sContent, bool isBodyHtml = true, string cust_email = "", string cust_name = "")
        {
            if (cust_email == "")
            {
                cust_email = sender_email;
            }
            if (cust_name == "")
            {
                cust_name = sender_name;
            }

            mail_message = new MailMessage();
            mail_message.From = new MailAddress(cust_email, cust_name);
            mail_message.Subject = sSubject;
            mail_message.Body = sContent;
            mail_message.IsBodyHtml = isBodyHtml;
            mail_message.BodyEncoding = System.Text.Encoding.UTF8;
        }

        //step 2
        public void AddToEmail(string email)
        {
            mail_message.To.Add(new MailAddress(email));
        }

        //step 2
        public void AddCCEmail(string email)
        {
            mail_message.CC.Add(new MailAddress(email));
        }

        //step 2
        public void AddBCCEmail(string email)
        {
            mail_message.Bcc.Add(new MailAddress(email));
        }

        //step 3
        public string SendEmail(bool EnableSsl = false)
        {
            string returnValue = "";

            using (var client = new SmtpClient(smtp_host, port))
            {
                client.Credentials = new System.Net.NetworkCredential(username, password);
                client.EnableSsl = EnableSsl;
                try
                {
                    client.Send(mail_message);
                }
                catch (SmtpException ex)
                {
                    string msg = "Mail cannot be sent:";
                    msg += ex.Message;
                    returnValue = msg;
                    throw new Exception(msg);
                }
            }
            return returnValue;
        }
    }
}