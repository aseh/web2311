﻿using System;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace web.Common
{
    public class Tool
    {
        public static string Md5Convert(string str_value)
        {
            string str_converted = "";
            byte[] data = Encoding.Default.GetBytes(str_value.Trim());
            MD5 cs_md5 = new MD5CryptoServiceProvider();
            byte[] result = cs_md5.ComputeHash(data);
            str_converted = BitConverter.ToString(result).Replace("-", "").ToLowerInvariant();
            return str_converted;
        }
        // 取得IP
        public static string GetCurrentIP()
        {
            return HttpContext.Current.Request.UserHostAddress;
        }
        // 將string 轉為 int
        public static int TryToParseInt(string value, int default_value = 0)
        {
            int number;
            bool result = int.TryParse(value, out number);
            if (!result) number = default_value;
            return number;
        }
        // 將string 轉為 long
        public static long TryToParseLong(string value, long default_value = 0)
        {
            long number;
            bool result = long.TryParse(value, out number);
            if (!result) number = default_value;
            return number;
        }
        // 將string 轉為 short
        public static short TryToParseShort(string value, short default_value = 0)
        {
            short number;
            bool result = short.TryParse(value, out number);
            if (!result) number = default_value;
            return number;
        }
        // 將string 轉為 byte
        public static byte TryToParseByte(string value, byte default_value = 0)
        {
            byte number;
            bool result = byte.TryParse(value, out number);
            if (!result) number = default_value;
            return number;
        }
        // 驗證是否為數值
        public static bool IsNumeric(string is_number)
        {
            Regex myRegex = new Regex("^[0-9]{1,}$");
            return myRegex.IsMatch(is_number);
        }
        // 驗證是否為email
        public static bool IsEmail(string str_Email)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(str_Email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }
        //檢查是否為英數字或 _-
        public static bool IsEnglish_Number_UnderLine(string str)
        {
            System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex(@"^[0-9a-zA-Z_-]*$");
            return reg1.IsMatch(str);
        }

        //取出檔案內容
        public static string GetFileToString(string filepath)
        {
            StringBuilder stringbuilder = new StringBuilder();
            try
            {
                if (System.IO.File.Exists(filepath) == false)
                    return "";

                using (System.IO.StreamReader sr = new System.IO.StreamReader(filepath))
                {
                    stringbuilder.Append(sr.ReadToEnd());
                    sr.Close();
                    sr.Dispose();
                }

                return stringbuilder.ToString();
            }
            catch
            {
                return "";
            }
        }
    }
}