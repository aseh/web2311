﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;

namespace web.Common
{
    public static class ViewHelper
    {
        static Regex LineEnding = new Regex(@"(\r\n|\r|\n)+");
        // 顯示錯誤訊息
        public static MvcHtmlString GetErrorMsg( Dictionary<string, string> error, string key)
        {
            if (error == null)
            {
                return new MvcHtmlString("");
            }
            else if (!error.ContainsKey(key))
            {
                return new MvcHtmlString("");
            }
            else
            {
                return new MvcHtmlString(error[key]);
            }
        }
        // 顯示錯誤訊息
        public static MvcHtmlString ErrorMsg(this HtmlHelper html, string key)
        {
            Dictionary<string, string> error = (Dictionary<string, string>)html.ViewContext.TempData["error"];
            if (error == null)
            {
                return new MvcHtmlString("");
            }
            else if (!error.ContainsKey(key))
            {
                return new MvcHtmlString("");
            }
            else
            {
                return new MvcHtmlString(error[key]);
            }
        }
        // nl2br
        public static MvcHtmlString Nl2br(this HtmlHelper html, string text, bool isXhtml = true)
        {
            if (text == null) 
            {
                text = "";
            }
            return MvcHtmlString.Create(Nl2br(text, isXhtml));
        }
        // nl2br
        public static string Nl2br(string text, bool isXhtml = true)
        {
            if (text == null)
            {
                return "";
            }
            var encodedText = HttpUtility.HtmlEncode(text);
            var replacement = isXhtml ? "<br />" : "<br>";
            return LineEnding.Replace(encodedText, replacement);
        }
        // Nl2li
        public static MvcHtmlString Nl2li(this HtmlHelper html, string text = "", string prefix = "")
        {
            string result_value = "";
            if (!string.IsNullOrEmpty(text))
            {
                string[] li_list = text.Split('\n');
                foreach (string li_value in li_list)
                {
                    if ( ! string.IsNullOrEmpty(li_value))
                    {
                        result_value += "<li> " + prefix + li_value + "</li>";
                    }
                }
            }

            return MvcHtmlString.Create(result_value);
        }
    }
}