﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using web.Areas.backend.Models;
using web.Common;
using web.Models;
using web.ViewModels.Blog;

namespace web.Controllers
{
    public class BlogController : FrontendController
    {

        private F_contact_model contact_model = new F_contact_model();
        // GET: Blog
        public ActionResult Index(int page = 0, int authorId = 0, int typeId = 0, string keyword = "", int pageSize = 9  )
        {
            Init();

            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            string Title = language.lang == "en" ? "All posts" : "所有文章";

            f_csr_blog_model model = new f_csr_blog_model();

            var ViewModel = new IndexView()
            {
                AuthorList = model.GetAuthorList(language.lang, 0),
                TypeList = model.GetTypeList(language.lang, 0),
                TopList = model.GetTopBlog(language.lang),
                Author = model.GetBlog_Author(authorId),
                PageList = model.GetAllBlog(language.lang, authorId, typeId, pageSize, page, keyword),
                Total = model.GetAllBlogCount(language.lang, authorId, typeId, keyword),
                AuthorId = authorId,
                TypeId = typeId,
                Keyword = keyword,
                Title = Title,
                Type = model.GetBlog_Type(typeId),
                PageSize = pageSize,
                Page = page
            };


            if (ViewModel.Author != null)
            {
                ViewModel.AuthorName = ViewModel.Author.blog_author_dtl.Where(x => x.lang == language.lang).FirstOrDefault().name;
                ViewModel.Title = ViewModel.AuthorName;
            }

            if (ViewModel.Type != null)
            {
                typename tn = JsonConvert.DeserializeObject<typename>(ViewModel.Type.name);
                ViewModel.TypeName = language.lang == "ch" ? tn.ch : tn.en;
                ViewModel.Title = ViewModel.TypeName;
            }
            return View("Index", ViewModel);
        }

        public ActionResult Author(int Id, int page = 0, int pageSize = 9, int typeId = 0, string keyword = "")
        {
            var authorId = Id;
            Init();
            f_csr_blog_model model = new f_csr_blog_model();
            blog_author data = model.GetAuthorData(Id);

            if (browser_not_support)
            {
                return View(template + "/browser", data);
            }

            Init();

            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            string Title = language.lang == "en" ? "All posts" : "所有文章";


            var ViewModel = new AuthorView()
            {
                AuthorList = model.GetAuthorList(language.lang, 0),
                TypeList = model.GetTypeList(language.lang, 0),
                TopList = model.GetTopBlog(language.lang),
                Author = model.GetBlog_Author(authorId),
                PageList = model.GetAllBlog(language.lang, authorId, typeId, pageSize, page, keyword),
                Total = model.GetAllBlogCount(language.lang, Id, typeId, keyword),
                AuthorId = authorId,
                TypeId = typeId,
                Keyword = keyword,
                Title = Title,
                Type = model.GetBlog_Type(typeId),
                PageSize = pageSize,
                Page = page,
                Pic = "/public/uploads/defaultauthor.png"
            };


            if (ViewModel.Author != null)
            {
                ViewModel.AuthorName = ViewModel.Author.blog_author_dtl.Where(x => x.lang == language.lang).FirstOrDefault().name;
                ViewModel.Title = ViewModel.AuthorName;
                if ( ViewModel.Author.picurl != null )
                {
                    ViewModel.Pic = ViewModel.Author.picurl;
                }
            }

            if (ViewModel.Type != null)
            {
                typename tn = JsonConvert.DeserializeObject<typename>(ViewModel.Type.name);
                ViewModel.TypeName = language.lang == "ch" ? tn.ch : tn.en;
                ViewModel.Title = ViewModel.TypeName;
            }
            return View(ViewModel);
        }

        public ActionResult Detail(string Id)
        {
            
            var urlid = Id;
            Init();

            if (browser_not_support)
            {
                return View(template + "/browser");
            }


            f_csr_blog_model model = new f_csr_blog_model();

            var ViewModel = new DetailView() { 
                Blog = model.GetBlogByUrlId(Id),
                AuthorList = model.GetAuthorList(language.lang, 0),
                TypeList = model.GetTypeList(language.lang, 0),
            };

            //紀錄blog_log  this.language.lang
            string ip = GetIPAddress(Request);// Request.UserHostAddress;
            if (ViewModel.Blog != null)
            {
                model.AddBlogLog(ViewModel.Blog, this.language.lang, ip);
                ViewModel.NextBlog = model.GetNextBlog(ViewModel.Blog.id, language.lang);
                ViewModel.PrevBlog = model.GetPrevBlog(ViewModel.Blog.id, language.lang);
            }

            //SEO
            if (ViewModel.Blog.isseo == 1)
            {
                ViewBag.webSiteOgImage = ViewModel.Blog.seopicurl;
                ViewBag.Title = ViewModel.Blog.blog_dtl.Where(u => u.lang == this.language.lang).FirstOrDefault().title;
                string webSiteDesc = ViewModel.Blog.blog_dtl.Where(u => u.lang == this.language.lang).FirstOrDefault().seodescription;
                ViewBag.webSiteDesc = webSiteDesc; //data.blog_dtl.Where(u => u.lang == this.language.lang).FirstOrDefault().contenttext;

            }
            var isHideLang = ViewModel.Blog.blog_dtl.Where(x => x.lang != this.language.lang).All(x => x.contenttext == "");
            if (isHideLang)
            {
                if (this.language.lang == "en")
                {
                    ViewBag.zhURL = null;
                }
                else
                {
                    ViewBag.enURL = null;
                }
            }
            //
            return View(ViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ajaxPublishmessage(long blogid, long dtlid, string name, string email, string message)
        {
            Init();
            f_csr_blog_model model = new f_csr_blog_model();
            var blog = model.GetBlogById(blogid);
            var dtl = blog.blog_dtl.First(x => x.id == dtlid);
            var Result = Json(model.SendMessage(blogid, dtlid, name, email, message));
            string tmpString = "<tr><td style='font-family: \"微軟正黑體\", Arial; font-size: 14px; line-height: 1.7em; box-sizing: border-box; padding: 10px; color:#504e4f;border-bottom:1px solid #e6e6e6;width: 150px;vertical-align: top;'>#TITLE#：</td><td style='font-family: \"微軟正黑體\", Arial; font-size: 14px; line-height: 1.7em; box-sizing: border-box; padding:10px; color:#504e4f;border-bottom:1px solid #e6e6e6;'>#CONTENT#</td></p></tr>";
            string form_content = "";
            form_content += tmpString.Replace("#TITLE#", "Blog").Replace("#CONTENT#", dtl.title);
            form_content += tmpString.Replace("#TITLE#", "From").Replace("#CONTENT#", name);
            form_content += tmpString.Replace("#TITLE#", "Email").Replace("#CONTENT#", email);
            form_content += tmpString.Replace("#TITLE#", "Message").Replace("#CONTENT#", message);

            Uri uri = Request.Url;
            string host = uri.Scheme + Uri.SchemeDelimiter + uri.Host + ":" + uri.Port;
            string mail_content = Tool.GetFileToString(Server.MapPath("~/Common/contact_mail.html"));
            mail_content = mail_content.Replace("#picture#", host + @Url.Content("~/public/frontend/images/logo-en.png"));
            mail_content = mail_content.Replace("#mail_content#", form_content);

            //Mail Object settings
            // get (supplier) email receiver
            List<contact_email> contact_list = contact_model.GetReceiverList(3);
            Mail Mail = new Mail();
            Mail.InitMail("Become ASE Supply Chain Partner", mail_content, true, "", "");
            foreach (contact_email receiver in contact_list)
            {
                Mail.AddToEmail(receiver.email);
            }
            Mail.SendEmail();//寄出              

            return Result;
        }

        public static string GetIPAddress(HttpRequestBase request)
        {
            string ip;
            try
            {
                ip = request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (!string.IsNullOrEmpty(ip))
                {
                    if (ip.IndexOf(",") > 0)
                    {
                        string[] ipRange = ip.Split(',');
                        int le = ipRange.Length - 1;
                        ip = ipRange[le];
                    }
                }
                else
                {
                    ip = request.UserHostAddress;
                }
            }
            catch { ip = null; }

            return ip;
        }
    }
}