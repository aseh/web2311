﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using web.Models;
using web.Common;
using System.Linq;

namespace web.Controllers
{
    public class ContactController : FrontendController
    {
        private F_contact_model model = new F_contact_model();
        //public ActionResult Index(contact data)
        //{
        //    Init();
        //    AddJs("vendor/hammer.min.js");
        //    AddJs("index.js");

        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }
        //    ViewBag.country_list = _getCountry();

        //    var countries = ISO3166.GetCollection();
        //    List<string> country_code_list = new List<string>();
        //    foreach (var item in countries)
        //    {
        //        if (item.DialCodes != null && country_code_list.Where(x => x == item.DialCodes.FirstOrDefault()).Count() == 0)
        //        {
        //            string temp_code = item.DialCodes.FirstOrDefault();
        //            country_code_list.Add(temp_code);
        //        }
        //    }
        //    country_code_list.Sort();
        //    ViewBag.country_code = country_code_list;

        //    return View(template + "/Contact/Index", data);
        //}

        //[HttpPost, ValidateInput(false)]
        //[ValidateAntiForgeryToken]
        //public ActionResult Form(FormCollection fc)
        //{
        //    Init();

        //    bool isValid = true;
        //    Dictionary<string, string> error = new Dictionary<string, string>();
        //    string msg = "Necessary";
        //    string type = "", name = "", email = "", country_code = "", phone = "", ext = "", company = "", contents = "", user_ip = "";

        //    #region 驗證資料/儲存資料

        //    type = fc["type"].Trim();
        //    if (string.IsNullOrEmpty(type))
        //    {
        //        isValid = false;
        //        error.Add("type", msg);
        //    }
        //    name = fc["name"].Trim();
        //    if (string.IsNullOrEmpty(name))
        //    {
        //        isValid = false;
        //        error.Add("name", msg);
        //    }

        //    email = fc["email"].Trim();
        //    if (string.IsNullOrEmpty(email))
        //    {
        //        isValid = false;
        //        error.Add("email", msg);
        //    }
        //    else if (!Tool.IsEmail(email))
        //    {
        //        isValid = false;
        //        error.Add("email", "Must be a valid email address.");
        //    }

        //    country_code = fc["country_code"].Trim();
        //    phone = fc["phone"].Trim();
        //    ext = fc["ext"].Trim();
        //    company = fc["company"].Trim();

        //    contents = fc["contents"].Trim();
        //    if (string.IsNullOrEmpty(contents))
        //    {
        //        isValid = false;
        //        error.Add("contents", msg);
        //    }
            
        //    // 使用者IP位置
        //    string HTTP_VIA = Request.ServerVariables["HTTP_VIA"];
        //    if (string.IsNullOrEmpty(HTTP_VIA))
        //    {
        //        string REMOTE_ADDR = Request.ServerVariables["REMOTE_ADDR"];
        //        user_ip = REMOTE_ADDR;
        //    }
        //    else
        //    {
        //        string HTTP_X_FORWARDED_FOR = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        //        user_ip = HTTP_X_FORWARDED_FOR;
        //    }
        //    #endregion

        //    contact data = new contact();
        //    data.create_date = DateTime.Now;
        //    data.lang = language.lang;
        //    data.type = Tool.TryToParseInt(type);
        //    data.name = name;
        //    data.email = email;
        //    data.country_code = country_code;
        //    data.phone = phone;
        //    data.ext = ext;
        //    data.company = company;
        //    data.contents = contents;
        //    data.ip = user_ip;

        //    string type_title = "";
        //    switch (data.type)
        //    {
        //        case 1:
        //            type_title = "Website Feedback";
        //            break;

        //        case 2:
        //            type_title = "Media Enquiries - PR Contacts_USA/Europe";
        //            break;

        //        case 3:
        //            type_title = "Media Enquiries - PR Contacts_Asia Pacific";
        //            break;

        //        case 4:
        //            type_title = "CSR Contacts";
        //            break;

        //        default:
        //            break;
        //    }
        //    if (isValid)
        //    {
        //        model.SaveData(data);
        //        string tmpString = "<tr><td style='font-family: \"微軟正黑體\", Arial; font-size: 14px; line-height: 1.7em; box-sizing: border-box; padding: 10px; color:#504e4f;border-bottom:1px solid #e6e6e6;width: 150px;vertical-align: top;'>#TITLE#：</td><td style='font-family: \"微軟正黑體\", Arial; font-size: 14px; line-height: 1.7em; box-sizing: border-box; padding:10px; color:#504e4f;border-bottom:1px solid #e6e6e6;'>#CONTENT#</td></p></tr>";
        //        string form_content = "";
        //        form_content += tmpString.Replace("#TITLE#", "Date").Replace("#CONTENT#", data.create_date.ToString());
        //        form_content += tmpString.Replace("#TITLE#", "Question Type").Replace("#CONTENT#", type_title);
        //        form_content += tmpString.Replace("#TITLE#", "Name").Replace("#CONTENT#", data.name);
        //        form_content += tmpString.Replace("#TITLE#", "Email").Replace("#CONTENT#", data.email);
        //        form_content += tmpString.Replace("#TITLE#", "Phone").Replace("#CONTENT#", data.country_code + data.phone + data.ext);
        //        form_content += tmpString.Replace("#TITLE#", "Company").Replace("#CONTENT#", data.company);
        //        form_content += tmpString.Replace("#TITLE#", "Content").Replace("#CONTENT#", data.contents);

        //        Uri uri = Request.Url;
        //        string host = uri.Scheme + Uri.SchemeDelimiter + uri.Host + ":" + uri.Port;
        //        string mail_content = Tool.GetFileToString(Server.MapPath("~/Common/contact_mail.html"));
        //        mail_content = mail_content.Replace("#picture#", host + @Url.Content("~/public/frontend/images/logo-en.png"));
        //        mail_content = mail_content.Replace("#mail_content#", form_content);

        //        //Mail Object settings
        //        // get (contact us) email receiver
        //        List<contact_email> contact_list = model.GetReceiverList(2);
        //        Mail Mail = new Mail();
        //        Mail.InitMail("Contact", mail_content, true, "", "");
        //        foreach (contact_email receiver in contact_list)
        //        {
        //            Mail.AddToEmail(receiver.email);
        //        }

        //        Mail.SendEmail();//寄出

        //        return RedirectToAction("Contact", new { lang = this.language.lang });
        //    }
        //    else
        //    {
        //        TempData["error"] = error;
        //        ViewBag.country_list = _getCountry();

        //        var countries = ISO3166.GetCollection();
        //        List<string> country_code_list = new List<string>();
        //        foreach (var item in countries)
        //        {
        //            if (item.DialCodes != null && country_code_list.Where(x => x == item.DialCodes.FirstOrDefault()).Count() == 0)
        //            {
        //                string temp_code = item.DialCodes.FirstOrDefault();
        //                country_code_list.Add(temp_code);
        //            }
        //        }
        //        country_code_list.Sort();
        //        ViewBag.country_code = country_code_list;

        //        return View(template + "/Contact/Index", data);
        //    }
        //}

        //private List<string> _getCountry()
        //{
        //    List<string> country_list = new List<string>();
        //    CultureInfo[] CInfoList = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
        //    foreach (CultureInfo CInfo in CInfoList)
        //    {
        //        RegionInfo R = new RegionInfo(CInfo.LCID);
        //        if (!(country_list.Contains(R.EnglishName)))
        //        {
        //            country_list.Add(R.EnglishName);
        //        }
        //    }
        //    country_list.Sort();
        //    return country_list;
        //}
    }
}