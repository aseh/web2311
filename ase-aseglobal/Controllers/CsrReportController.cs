﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web.Mvc;
using web.Common;
using web.Models;
using System.Linq;
using System.Web;

namespace web.Controllers
{
    public class CsrReportController : FrontendController
    {
        //private F_csr_report_model report_model = new F_csr_report_model();
        //private F_csr_report_count_model count_model = new F_csr_report_count_model();
        //private f_csr_report_questionnaire_model questionnaire_model = new f_csr_report_questionnaire_model();

        //public ActionResult Index()
        //{
        //    Init();

        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }
        //    List<csr_report> list = report_model.GetList(this.language.lang);

        //    return View(template + "/CsrReport/Index", list);
        //}

        //public ActionResult Download(long param1)
        //{
        //    csr_report data = report_model.GetData(param1);

        //    string file_path = Server.MapPath(Url.Content("~/" + this.media_bank_path + "/")) + data.filename;

        //    FileInfo fi = new FileInfo(file_path);
        //    string extension;

        //    extension = Path.GetExtension(file_path);
        //    Stream iStream = new FileStream(file_path, FileMode.Open, FileAccess.Read, FileShare.Read);
        //    return File(iStream, "Application/pdf", data.title + extension);
        //}

        //public ActionResult AddCountData(long param1)
        //{
        //    csr_report data = report_model.GetData(param1);

        //    csr_report_count count_data = new csr_report_count();
        //    count_data.report_id = data.id;
        //    count_data.ip = Tool.GetCurrentIP();
        //    count_data.create_date = DateTime.Now;
        //    count_model.SaveData(count_data);
            
        //    return Json(count_data.id);
        //}

        //public ActionResult Form(long param1 = 0, long param2 = 0)
        //{
        //    Init();

        //    // 產生驗證碼
        //    ImageVerificationCodeModel iv = new ImageVerificationCodeModel(HttpContext.Cache, Server.MapPath("/"));
        //    ImageVerificationCodeModel ivdata = iv.CreateIVCode();
        //    ViewData["key"] = ivdata.CacheKey;
        //    ViewData["img"] = ivdata.URL;

        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    long report_id = 0;
        //    if (param1 > 0)
        //    {
        //        csr_report report = report_model.GetData(param1);
        //        report_id = report.id;
        //    }

        //    long count_id = 0;
        //    if (param2 > 0)
        //    {
        //        csr_report_count count = count_model.GetData(param2);
        //        count_id = count.id;
        //    }

        //    csr_report_questionnaire data = new csr_report_questionnaire();
        //    string default_choice = "VeryHigh";
        //    data.q1 = default_choice;
        //    data.q2 = default_choice;
        //    data.q3 = default_choice;
        //    data.q4 = default_choice;
        //    data.q5 = default_choice;
        //    data.q6 = default_choice;
        //    data.q7 = default_choice;
        //    data.q8 = default_choice;
        //    data.q9 = default_choice;
        //    data.q10 = default_choice;
        //    data.q11 = default_choice;
        //    data.q12 = default_choice;
        //    data.q13 = default_choice;
        //    data.q14 = default_choice;
        //    data.q15 = default_choice;
        //    data.q16 = default_choice;
        //    data.q17 = default_choice;
        //    data.q18 = default_choice;
        //    data.q19 = default_choice;
        //    data.q20 = default_choice;
        //    data.q21 = default_choice;
        //    data.q22 = default_choice;
        //    data.q23 = default_choice;

        //    ViewBag.report_id = report_id;
        //    ViewBag.count_id = count_id;

        //    return View(template + "/CsrReport/Form", data);
        //}

        //[HttpPost, ValidateInput(false)]
        //[ValidateAntiForgeryToken]
        //public ActionResult Content(FormCollection fc)
        //{
        //    Init();

        //    long report_id = Tool.TryToParseLong(fc["report_id"]);
        //    long count_id = Tool.TryToParseLong(fc["count_id"]);

        //    csr_report_questionnaire data = FcToData(fc);
        //    if (UpdateData(data, fc))
        //    {
        //        ShowUpdateMessage("Thank you for your message.");

        //        // update report count
        //        csr_report_count count_data = count_model.GetData(count_id);
        //        count_data.questionnaire_id = data.id;
        //        count_model.SaveData(count_data);

        //        csr_report_questionnaire new_data = new csr_report_questionnaire();
        //        return RedirectToAction("Form", new { param1 = report_id, param2 = count_id });
        //    }

        //    Response.Cookies.Clear();
        //    Session.Abandon();
        //    //停用瀏覽器快取
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.Cache.SetNoStore();
        //    ImageVerificationCodeModel iv2 = new ImageVerificationCodeModel(HttpContext.Cache, Server.MapPath("/"));
        //    ImageVerificationCodeModel ivdata = iv2.CreateIVCode();
        //    ViewData["key"] = ivdata.CacheKey;
        //    ViewData["img"] = ivdata.URL;
            
        //    ViewBag.report_id = report_id;
        //    ViewBag.count_id = count_id;

        //    ShowUpdateFail("Error.");
        //    return View(template + "/CsrReport/Form", data);
        //}

        //private csr_report_questionnaire FcToData(FormCollection fc)
        //{
        //    csr_report_questionnaire data = new csr_report_questionnaire();
        //    string stakeholder = fc["stakeholder"] == null ? "" : fc["stakeholder"].Trim();
        //    if (stakeholder.ToLower() == "other")
        //    {
        //        data.other = fc["other"] == null ? "" : "Other-"+fc["other"].Trim();
        //    }
        //    data.stakeholder = stakeholder;
        //    data.q1 = fc["Corporate"] == null ? "" : fc["Corporate"].Trim();
        //    data.q2 = fc["Ethics"]==null? "":fc["Ethics"].Trim();
        //    data.q3 = fc["Regulatory"]==null? "":fc["Regulatory"].Trim();
        //    data.q4 = fc["Stakeholders"] ==null? "":fc["Stakeholders"].Trim();
        //    data.q5 = fc["Strategy"]==null? "":fc["Strategy"].Trim();
        //    data.q6 = fc["Customer"]==null? "":fc["Customer"].Trim();
        //    data.q7 = fc["Taxation"]==null? "":fc["Taxation"].Trim();
        //    data.q8 = fc["Supply"]==null? "":fc["Supply"].Trim();
        //    data.q9 = fc["Innovation"]==null? "":fc["Innovation"].Trim();
        //    data.q10 = fc["Risk"]==null? "":fc["Risk"].Trim();
        //    data.q11 = fc["Climate"]==null? "":fc["Climate"].Trim();
        //    data.q12 = fc["Waste"]==null? "":fc["Waste"].Trim();
        //    data.q13 = fc["Water"]==null? "":fc["Water"].Trim();
        //    data.q14 = fc["Energy"]==null? "":fc["Energy"].Trim();
        //    data.q15 = fc["Green"]==null? "":fc["Green"].Trim();
        //    data.q16 = fc["Training"]==null? "":fc["Training"].Trim();
        //    data.q17 = fc["Equality"]==null? "":fc["Equality"].Trim();
        //    data.q18 = fc["Human"]==null? "":fc["Human"].Trim();
        //    data.q19 = fc["Labor"]==null? "":fc["Labor"].Trim();
        //    data.q20 = fc["Health"]==null? "":fc["Health"].Trim();
        //    data.q21 = fc["Community"]==null? "":fc["Community"].Trim();
        //    data.q22 = fc["Charity"]==null? "":fc["Charity"].Trim();
        //    data.q23 = fc["Initiatives"]==null? "":fc["Initiatives"].Trim();
        //    data.additional = fc["additional"] == null ? "" : fc["additional"].Trim();
        //    data.ip = Tool.GetCurrentIP();
        //    data.create_date = DateTime.Now;
        //    data.update_date = DateTime.Now;

        //    return data;
        //}

        //private bool UpdateData(csr_report_questionnaire data, FormCollection fc)
        //{
        //    int affected_row = 0;

        //    //驗證
        //    //---------------------------------------------------------------------
        //    //錯誤訊息
        //    Dictionary<string, string> error = new Dictionary<string, string>();

        //    bool is_valid = true;

        //    #region 驗證
        //    string key = fc["key"] == null ? "" : Server.HtmlEncode(fc["key"]);
        //    string vcode = fc["vcode"] == null ? "" : Server.HtmlEncode(fc["vcode"]);

        //    ImageVerificationCodeModel iv = new ImageVerificationCodeModel(HttpContext.Cache);
        //    if (!iv.isVerificationCodeMatch(key, vcode))
        //    {
        //        // 驗證未通過，產生新的驗證碼
        //        is_valid = false;
        //        error.Add("vcode_img", "Necessary");
        //    }

        //    if (string.IsNullOrEmpty(data.stakeholder))
        //    {
        //        is_valid = false;
        //        error.Add("stakeholder", "Necessary");
        //    } else if(data.stakeholder.ToLower() == "other" && string.IsNullOrEmpty(data.other))
        //    {
        //        is_valid = false;
        //        error.Add("other", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q1))
        //    {
        //        is_valid = false;
        //        error.Add("q1", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q2))
        //    {
        //        is_valid = false;
        //        error.Add("q2", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q3))
        //    {
        //        is_valid = false;
        //        error.Add("q3", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q4))
        //    {
        //        is_valid = false;
        //        error.Add("q4", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q5))
        //    {
        //        is_valid = false;
        //        error.Add("q5", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q6))
        //    {
        //        is_valid = false;
        //        error.Add("q6", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q7))
        //    {
        //        is_valid = false;
        //        error.Add("q7", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q8))
        //    {
        //        is_valid = false;
        //        error.Add("q8", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q9))
        //    {
        //        is_valid = false;
        //        error.Add("q9", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q10))
        //    {
        //        is_valid = false;
        //        error.Add("q10", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q11))
        //    {
        //        is_valid = false;
        //        error.Add("q11", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q12))
        //    {
        //        is_valid = false;
        //        error.Add("q12", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q13))
        //    {
        //        is_valid = false;
        //        error.Add("q13", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q14))
        //    {
        //        is_valid = false;
        //        error.Add("q14", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q15))
        //    {
        //        is_valid = false;
        //        error.Add("q15", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q16))
        //    {
        //        is_valid = false;
        //        error.Add("q16", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q17))
        //    {
        //        is_valid = false;
        //        error.Add("q17", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q18))
        //    {
        //        is_valid = false;
        //        error.Add("q18", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q19))
        //    {
        //        is_valid = false;
        //        error.Add("q19", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q20))
        //    {
        //        is_valid = false;
        //        error.Add("q20", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q21))
        //    {
        //        is_valid = false;
        //        error.Add("q21", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q22))
        //    {
        //        is_valid = false;
        //        error.Add("q22", "Necessary");
        //    }
        //    if (string.IsNullOrEmpty(data.q23))
        //    {
        //        is_valid = false;
        //        error.Add("q23", "Necessary");
        //    }
        //    #endregion

        //    TempData["error"] = error;
        //    //---------------------------------------------------------------------
        //    if (is_valid)
        //    {
        //        affected_row += questionnaire_model.SaveData(data);
        //        if (affected_row > 0)
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

    }
}