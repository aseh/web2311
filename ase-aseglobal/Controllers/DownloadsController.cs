﻿using System.Web.Mvc;

namespace web.Controllers
{
    public class DownloadsController : FrontendController
    {
        public ActionResult Index()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Downloads/downloads");
        }
    }
}