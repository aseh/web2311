﻿using System;
using System.Web.Mvc;

namespace web.Controllers
{
    public class ErrorpageController : FrontendController
    {
        public ActionResult Index()
        {
            return View(template + "/browser");
        }
        public ActionResult General(Exception exception)
        {
            string url = Request.RawUrl;
            url = url.Substring(1, 2);
            if (url == "en")
            {
                this.Response.StatusCode = 404;
                this.Response.SubStatusCode = 1;
                return new EmptyResult();
                // return Redirect("/public/en/404.html");
            }
            else
            {
                this.Response.StatusCode = 404;
                this.Response.SubStatusCode = 2;
                return new EmptyResult();
                //return Redirect("/public/ch/404.html");
            }
        }


        public ActionResult Http403()
        {
            string url = Request.RawUrl;
            if (url == "en")
            {
                this.Response.StatusCode = 404;
                this.Response.SubStatusCode = 1;
                return new EmptyResult();
                //return Redirect("/public/en/404.html");
            }
            else
            {
                this.Response.StatusCode = 404;
                this.Response.SubStatusCode = 2;
                return new EmptyResult();
                //return Redirect("/public/ch/404.html");
            }
        }
        public ActionResult Http404()
        {
            //Init();


            //string sRedirectUrl = HttpContext.Request.Url.ToString();

            string url = Request.RawUrl;

            if (url.Equals("/ch/html/mems_sensor_solutions"))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.AppendHeader("Location", "http://ase.aseglobal.com/en/technology/mems_sensors");
                Response.AppendHeader("Cache-Control", "no-cache");
                return new EmptyResult();

            }
            if (url.Equals("/ch/html/sesub"))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.AppendHeader("Location", "http://ase.aseglobal.com/en/technology/sesub");
                Response.AppendHeader("Cache-Control", "no-cache");
                return new EmptyResult();
            }
            if (url.Equals("/ch/csr/social_involvement/environmental_conservation"))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.AppendHeader("Location", "http://ase.aseglobal.com/en/csr/social_involvement/environmental_conservation");
                Response.AppendHeader("Cache-Control", "no-cache");
                return new EmptyResult();
            }
            if (url.Equals("/en/html/cu_pillar"))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.AppendHeader("Location", "http://ase.aseglobal.com/en/technology/cu_pillar");
                Response.AppendHeader("Cache-Control", "no-cache");
                return new EmptyResult();
            }
            if (url.Equals("/ch/lab/package_design/lab_chemical_and_green"))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.AppendHeader("Location", "http://ase.aseglobal.com/en/lab/package_design/lab_chemical_and_green");
                Response.AppendHeader("Cache-Control", "no-cache");
                return new EmptyResult();
            }
            if (url.Equals("/en/html/fan_out"))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.AppendHeader("Location", "http://ase.aseglobal.com/en/technology/fan_out");
                Response.AppendHeader("Cache-Control", "no-cache");
                return new EmptyResult();
            }
            //if (url.Equals("/en/html/embedded_die_technology"))
            //{
            //    Response.StatusCode = 301;
            //    Response.Status = "301 Moved Permanently";
            //    Response.AppendHeader("Location", "http://ase.aseglobal.com/en/technology/embedded_die");
            //    Response.AppendHeader("Cache-Control", "no-cache");
            //    return new EmptyResult();
            //}
            if (url.Equals("/en/products/262"))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.AppendHeader("Location", "http://ase.aseglobal.com/en/about");
                Response.AppendHeader("Cache-Control", "no-cache");
                return new EmptyResult();
            }
            if (url.Equals("/en/products/39"))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.AppendHeader("Location", "http://ase.aseglobal.com/en/about");
                Response.AppendHeader("Cache-Control", "no-cache");
                return new EmptyResult();
            }
            if (url.Equals("/ch/html/trademarks"))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.AppendHeader("Location", "http://ase.aseglobal.com/ch/technology/ase_trademarks");
                Response.AppendHeader("Cache-Control", "no-cache");
                return new EmptyResult();
            }
            if (url.Equals("/en/html/aeasi"))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.AppendHeader("Location", "http://ase.aseglobal.com/en/technology/aeasi");
                Response.AppendHeader("Cache-Control", "no-cache");
                return new EmptyResult();
            }
            if (url.Equals("/en/technology/a-easi"))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.AppendHeader("Location", "http://ase.aseglobal.com/en/technology/aeasi");
                Response.AppendHeader("Cache-Control", "no-cache");
                return new EmptyResult();
            }
            if (url.Equals("/ch/technology/a-easi"))
            {
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.AppendHeader("Location", "http://ase.aseglobal.com/ch/technology/aeasi");
                Response.AppendHeader("Cache-Control", "no-cache");
                return new EmptyResult();
            }
            //

            url = (url.Length < 3) ? "en" : url.Substring(1, 2);
            if (url == "en")
            {
                this.Response.StatusCode = 404;
                this.Response.SubStatusCode = 1;
                return new EmptyResult();
            }
            else
            {
                this.Response.StatusCode = 404;
                this.Response.SubStatusCode = 2;
                return new EmptyResult();
            }
        }
    }
}