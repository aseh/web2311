﻿using System.Collections.Generic;
using System.Web.Mvc;
using web.Models;

namespace web.Controllers
{
    public class EventsController : FrontendController
    {
        private F_event_model model = new F_event_model();
        public ActionResult Index(int month = 0, string title = "")
        {
            Init();

            if (browser_not_support)
            {
                return View(template + "/browser");
            }
            List<events> list = model.GetList(this.language.lang, false, month, title);

            //record parameters
            ViewBag.month = month;
            ViewBag.title = title;

            //year of data
            List<int> month_list = model.GetEventMonth(this.language.lang);
            ViewBag.month_list = month_list;

            return View(template + "/Events/Index", list);
        }
    }
}