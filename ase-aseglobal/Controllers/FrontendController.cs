﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using web.Models;
using web.ViewModels;

namespace web.Controllers
{
    //TODO: Kenny: Devlopment 中 關閉SSL轉址
    //[RequireHttps]
    public class FrontendController : Controller
    {
        public web_menu node = null;    //單元資訊
        public String media_bank_path = "https://2311cdn.r.worldssl.net/public/media/";  //媒體庫路徑 
        public String local_media_bank_path = "public/media/";  //媒體庫路徑 
        protected bool browser_not_support = false;

        protected F_language_model m_lang = new F_language_model();
        protected language language = null;

        protected string template = "Default"; // 顯示的View
        public FrontendController()
        {
            //取得實體路徑&虛擬路徑
            ViewBag.media_bank_path = media_bank_path;
        }

        public void Init()
        {
            //瀏覽器版本過低顯示不支援頁面
            System.Web.HttpBrowserCapabilitiesBase browser = Request.Browser;
            float tmp_version = float.Parse(browser.Version);
            int version = (int)tmp_version;
            if (browser.Browser.ToLower() == "internetexplorer" && version <= 9)
            {
                browser_not_support = true;
            }

            //取得語系
            GetLanguage();

            string controller = RouteData.Values["controller"].ToString();
            string action = RouteData.Values["action"].ToString();
            string param1 = RouteData.Values["param1"] != null ? RouteData.Values["param1"].ToString() : "";
            string param2 = RouteData.Values["param2"] != null ? RouteData.Values["param2"].ToString() : "";
          
            ViewBag.controller = controller;
            ViewBag.action = action;
            //if (param1 != null || param2 == null)
            //{
                ViewBag.param1 = param1;
                ViewBag.param2 = param2;
            //}

            ViewBag.webSiteOgImage = "~/public/frontend/images/og.png";
            if ( this.language.lang == "en" )
            {
                
                ViewBag.webSiteDesc = "ASE is the world’s leading provider of independent semiconductor manufacturing services in assembly and test. ASE develops and offers complete turnkey solutions covering IC packaging, design and production of interconnect materials, front-end engineering test, wafer probing and final test.";
                ViewBag.webSiteKeywords = "ASE, ASE Group, Advanced Semiconductor Engineering, ASE Industrial Holding, SiP & Module Solutions, SiP, Fan Out, Assembly, Test";
                ViewBag.Title = "ASE Group";
                ViewBag.enURL = this.Request.Url.PathAndQuery;
                ViewBag.zhURL = this.Request.Url.PathAndQuery == "/" ? "/ch" : new Regex("/en").Replace(this.Request.Url.PathAndQuery, "/ch", 1);
            } else
            {
                ViewBag.Title = "日月光集團";
                ViewBag.webSiteDesc = "日月光為全球領先半導體封裝與測試製造服務公司，提供半導體客戶包括晶片前段測試及晶圓針測至後段之封裝、材料及成品測試的一元化服務。";
                ViewBag.webSiteKeywords = "日月光, 日月光集團, 日月光半導體, 日月光投資控股, ASE, 系統級封裝 , SiP, Fan Out, 封裝, 測試";
                ViewBag.zhURL = this.Request.Url.PathAndQuery;
                ViewBag.enURL = new Regex("/ch").Replace(this.Request.Url.PathAndQuery, "/en", 1);
            }
            

            string JsonString = System.IO.File.ReadAllText(Server.MapPath(String.Format("~/App_Data/SiteMap/{0}.json", this.language.lang)));
            List<SiteNav> SiteNavList = JsonConvert.DeserializeObject<List<SiteNav>>(JsonString);
            ViewBag.site_nav_list = SiteNavList;

        }

        public void GetLanguage()
        {
            string default_language = ConfigurationManager.AppSettings["default_language"];
            //string lang = default_language;
            string lang = RouteData.Values["lang"].ToString();
            lang = string.IsNullOrEmpty(lang) ? default_language : lang;

            this.language = m_lang.GetData(lang);

            ViewBag.language = this.language;
            ViewBag.lang = this.language.lang;

            template = language.lang;
        }

        public void GenerateVCode()
        {
            ImageVerificationCodeModel iv = new ImageVerificationCodeModel(HttpContext.Cache, Server.MapPath("/"));
            ImageVerificationCodeModel ivdata = iv.CreateIVCode();
            ViewData["key"] = ivdata.CacheKey;
            ViewData["img"] = ivdata.URL;
        }

        public bool ValidVCode(string key, string code)
        {
            ImageVerificationCodeModel iv = new ImageVerificationCodeModel(HttpContext.Cache);
            return iv.isVerificationCodeMatch(key, code);
        }

        //---------------------------------------------------------------------------------------------------------------
        // Show Message
        //---------------------------------------------------------------------------------------------------------------
        #region
        public void ShowUpdateMessage(string msg = "Update Success!!")
        {
            ShowMessage(msg, "success");
        }

        public void ShowUpdateFail(string msg = "Update Failed!!")
        {
            ShowMessage(msg, "error");
        }

        public void ShowMessage(string message, string type)
        {
            string msg = "<script>swal({title:\"" + message + "\", timer: 3000, type:'" + type + "'})</script>";
            TempData["mesage"] = msg;
        }
        #endregion

        //---------------------------------------------------------------------------------------------------------------
        // Add Css/JS
        //---------------------------------------------------------------------------------------------------------------
        #region
        public void AddJs(string js_path = "", bool default_path = true)
        {
            if (default_path)
            {
                ViewBag.js += "<script type=\"text/javascript\" src=\"https://2311cdn.r.worldssl.net/js" + "/public/frontend/js/" + js_path + "\"></script>\r";
            }
            else
            {
                ViewBag.js += "<script type=\"text/javascript\" src=\"https://2311cdn.r.worldssl.net/js/" + js_path + "\"></script>\r";
            }
        }

        public void AddCss(string css_path = "")
        {
            ViewBag.css += "<link href=\"" + "https://2311cdn.r.worldssl.net/css/public/frontend/css/" + css_path + "\" rel=\"stylesheet\" type=\"text/css\" />\r";
        }
        #endregion

        //---------------------------------------------------------------------------------------------------------------
        // Ajax
        //---------------------------------------------------------------------------------------------------------------
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxCheckNumVcode(string vcode)
        {
            if (vcode != Session["sc_validate_code"].ToString())
            {
                return Json(false);
            }
            else
            {
                return Json(true);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AjaxCheckVcode(string key, string vcode)
        {
            ImageVerificationCodeModel iv = new ImageVerificationCodeModel(HttpContext.Cache);
            if (!iv.isVerificationCodeMatch(key, vcode, false))
            {
                return Json(false);
            }
            else
            {
                return Json(true);
            }
        }

        #endregion

    }
}
