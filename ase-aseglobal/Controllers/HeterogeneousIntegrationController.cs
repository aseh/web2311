﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace web.Controllers
{
    //HeterogeneousIntegration
    public class Heterogeneous_IntegrationController : FrontendController
    {
        // GET: HeterogeneousIntegration
        public ActionResult Index()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            AddJs("ir.js");
            return View(template + "/Solution/solutions");
        }
    }
}