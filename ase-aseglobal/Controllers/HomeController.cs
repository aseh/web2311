﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web.Mvc;

using web.Models;
using web.ViewModels.GoogleSearch;


namespace web.Controllers
{
    //[RequireHttps]
    public class HomeController : FrontendController
    {
        private F_home_page_model home_model = new F_home_page_model();
        private F_event_model event_model = new F_event_model();
        private f_press_room_model press_room_model = new f_press_room_model();
        private F_ir_news_model ir_news_model = new F_ir_news_model();


        public ActionResult Index()
        {
            Init();
            string sLang = ControllerContext.RouteData.Values["lang"].ToString();

            if (this.language.lang != sLang)
            {
                if (this.language.lang == "en")
                {
                    this.Response.StatusCode = 404;
                    this.Response.SubStatusCode = 1;
                    return new EmptyResult();
                }
                else
                {
                    this.Response.StatusCode = 404;
                    this.Response.SubStatusCode = 2;
                    return new EmptyResult();                  
                }
            }        
            AddJs("vendor/hammer.min.js");
            AddJs("index.js");

            if (browser_not_support)
            {
                return View(template + "/browser");
            }
            List<home_page> home_page_data = home_model.GetList(this.language.lang);

            List<events> event_list = event_model.GetList(this.language.lang, true);
            ViewBag.event_list = event_list;

            List<press_room> press_room_list = press_room_model.GetList(this.language.lang, true);
            ViewBag.press_room_list = press_room_list;

            List<ir_news> ir_news_list = ir_news_model.GetList(this.language.lang);
            ViewBag.ir_news_list = ir_news_list;
            
            return View(template + "/Home/Index", home_page_data);

        }

        public ActionResult Search(string q = "", int page = 1)
        {
            Init();

            string searchUrl = String.Format("https://www.googleapis.com/customsearch/v1?key=AIzaSyCoIG9dTAyeNTxVWaAPZ2leVdt8f3MZm0c&cx=007871386038473342643:76sy3htapdk&q={0}&start={1}", q, ((page - 1) * 10) + 1);
            WebRequest req = HttpWebRequest.Create(searchUrl);

            req.ContentType = "application/json";


            GoogleSearchResult result;
            string body;
            // 取得回應資料
            using (HttpWebResponse response = req.GetResponse() as HttpWebResponse)
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    body = sr.ReadToEnd();
                    result = GoogleSearchResult.FromJson(body);
                }
            }

            ViewBag.SearchQuery = q;
            ViewBag.SearchPage = page;
            ViewBag.SearchBody = body;

            return View("../Search/Index", result);
        }
    }
}