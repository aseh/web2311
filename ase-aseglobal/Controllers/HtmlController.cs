﻿using System.Collections.Generic;
using System.Web.Mvc;
using web.Models;

namespace web.Controllers
{
    public class HtmlController : FrontendController
    {
        private F_ir_news_model ir_news_model = new F_ir_news_model();
        public ActionResult Index(string param1)
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }
            if (string.IsNullOrEmpty(param1))
            {
                AddJs("vendor/hammer.min.js");
                AddJs("index.js");
                return RedirectToAction("Index", "Home", new { lang = language.lang });
            }
            switch (param1)
            {
                case "ir":
                    List<ir_news> event_list = ir_news_model.GetList(this.language.lang, 1);
                    ViewBag.event_list = event_list;
                    List<ir_news> financial_list = ir_news_model.GetList(this.language.lang, 2);
                    ViewBag.financial_list = financial_list;
                    break;

                //case "award":
                //case "milestones":
                //case "ASE_Turnkey_Services":
                //case "community_engagement":
                    //AddJs("vendor/imagesloaded.pkgd.min.js");
                    //AddJs("vendor/masonry.pkgd.min.js");
                    //AddCss("vendor/ase_csr.css");
                    //break;
                    
                //case "sustainable_manufacturing":
                //    AddJs("vendor/jquery.jmap.min.js");
                //    AddJs("vendor/jquery.rwdImageMaps.js");
                //    break;

                case "products":
                //case "supplier_management":
                //case "solutions":
                    AddJs("ir.js");
                    break;
                    
                default:
                    break;
            }
            return View(template + "/Html/" + param1);
        }
        
    }
}