﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using web.Common;
using web.Models;

namespace web.Controllers
{
    public class labController : FrontendController
    {
        [ActionName("acoustic-lab")]
        public ActionResult AcousticLab()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Lab/acoustic-lab");
        }

        public ActionResult Index()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Lab/lab");
        }
        //public ActionResult eService()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/Lab/lab_eservices");
        //}
        public ActionResult Substrate_Design()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Lab/lab_substrate");
        }
        
        public ActionResult package_design(string param1 = "")
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }
            if (string.IsNullOrEmpty(param1))
            {
               
                return View(template + "/Lab/lab_package_design_services");
            }
            return View(template + "/Lab/package_design/" + param1);
        }
        
        
        
        
        
        //public ActionResult design(string param1 = "")
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }
        //    if (string.IsNullOrEmpty(param1))
        //    {
        //        string querysting = HttpContext.Request.QueryString.ToString().Trim();

        //        if (!string.IsNullOrEmpty(querysting))
        //        {
        //            string urls = HttpContext.Request.Url.ToString();
        //            string queryString = "?" + HttpContext.Request.QueryString.ToString().Trim();
        //            urls = urls.Replace(queryString, "");
        //            Response.StatusCode = 301;
        //            Response.AppendHeader("Location", urls);
        //        }
            
        //        return View(template + "/Lab/lab_package_design_services");
        //    }
        //    return View(template + "/Lab/design/" + param1);
        //}
    }
}