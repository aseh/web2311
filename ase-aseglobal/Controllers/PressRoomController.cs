﻿using System.Collections.Generic;
using System.Web.Mvc;
using web.Models;

namespace web.Controllers
{
    public class PressRoomController : FrontendController
    {
        private f_press_room_model model = new f_press_room_model();
        private F_sign_model sign_model = new F_sign_model();
        public ActionResult List(int year = 0, string title = "")
        {
            Init();

            if (browser_not_support)
            {
                return View(template + "/browser");
            }
            List<press_room> list = model.GetList(this.language.lang, false, year, title);

            //record parameters
            ViewBag.year = year;
            ViewBag.title = title;

            //year of data
            List<int> year_list = model.GetNewsYear(this.language.lang);
            ViewBag.year_list = year_list;

            return View(template + "/PressRoom/List", list);
        }
        //public ActionResult Content(long param1)
        public ActionResult Content(string param1)
        {
            Init();

            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            press_room data = model.GetData(param1);

            List<press_room> list = model.GetList(this.language.lang);
            press_room prev = null, next = null;
            bool find = false;

            if (list.Count > 0)
            {
                foreach (press_room item in list)
                {
                    if (find)
                    {
                        next = item;
                        break;
                    }

                    if (item.id == data.id)
                    {
                        find = true;
                    }
                    else
                    {
                        prev = item;
                    }
                }
            }
            ViewBag.prev = prev;
            ViewBag.next = next;

            sign sign_data = sign_model.GetData();
            ViewBag.sign_data = sign_data;

            return View(template + "/PressRoom/Content", data);
        }
    }
}