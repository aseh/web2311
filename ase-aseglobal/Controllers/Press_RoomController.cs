﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using web.Common;
using web.Models;

namespace web.Controllers
{
    public class Press_RoomController : FrontendController
    {
        private f_press_room_model model = new f_press_room_model();
        private F_sign_model sign_model = new F_sign_model();
        public ActionResult Index(int year = 0, string title = "")
        {
            Init();

            if (browser_not_support)
            {
                return View(template + "/browser");
            }
            List<press_room> list;
            if (year == 0)
            {
                year = DateTime.Now.Year;
                list = model.GetList(this.language.lang, false, year, title);
                list.AddRange(model.GetList(this.language.lang, false, year - 1, title));
            } else
            {
                list = model.GetList(this.language.lang, false, year, title);
            }
            

            //record parameters
            ViewBag.year = year;
            ViewBag.query = title;

            //year of data
            List<int> year_list = model.GetNewsYear(this.language.lang);
            ViewBag.year_list = year_list;
            foreach (press_room item in list)
            {
                item.urlid = HttpUtility.UrlEncode(item.urlid.Replace(" ", "-"));
            }
            
            return View(template + "/PressRoom/List", list);
        }
        public new ActionResult Content(string param1 = "")
        {
            Init(); 
            press_room data = model.GetData(param1);
            List<press_room> list = model.GetList(this.language.lang);
            press_room prev = null, next = null;
            bool find = false;

            if (list.Count > 0)
            {
                foreach (press_room item in list)
                {
                    item.urlid = HttpUtility.UrlEncode(item.urlid.Replace(" ", "-"));
                    if (find)
                    {
                        next = item;
                        break;
                    }

                    if (item.id == data.id)
                    {
                        find = true;
                    }
                    else
                    {
                        prev = item;
                    }
                }
            }
            ViewBag.prev = prev;
            ViewBag.next = next;

            sign sign_data = sign_model.GetData();
            ViewBag.sign_data = sign_data;
            ViewBag.Title = data.title;
            ViewBag.webSiteDesc = data.brief;

            if ( data.urlid2 != null && data.urlid2 != "" )
            {
                if (language.lang == "en")
                {
                    ViewBag.zhURL = "/ch/press_room/content/"+ data.urlid2;
                }
                else
                {
                    ViewBag.enURL = "/en/press_room/content/"+ data.urlid2;
                }
            } else
            {
                if (language.lang == "en")
                {
                    ViewBag.zhURL = "/ch/press_room";
                }
                else
                {
                    ViewBag.enURL = "/en/press_room";
                }
            }

            if ( data.og_image != null && data.og_image != "" )
            {
                ViewBag.webSiteOgImage = "~" + data.og_image;
            }
            

            
            
            

            return View(template + "/PressRoom/Content", data);
        }
        public ActionResult Latest()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/PressRoom/Latest");
        }
    }
}