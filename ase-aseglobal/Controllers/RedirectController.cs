﻿using System.Linq;
using System.Web.Mvc;
using web.Models;

namespace Ams.Controllers
{
    public class RedirectController : Controller
    {
        // URL重新導向
        public ActionResult Index()
        {
            F_language_model m_lang = new F_language_model();
            string lang = "en";

            //取得瀏覽器語系字串
            string[] browser_values = Request.UserLanguages;
            if (browser_values != null && browser_values.Count() > 0)
            {
                lang = browser_values.FirstOrDefault().ToLower();
            }

            //檢查是否是網站語系
            if (!m_lang.CheckLang(lang))
            {
                //取得預設的語系
                language default_data = m_lang.GetDefaultData();
                lang = default_data.lang;
            }

            //return RedirectToRoute("content", new { lang = lang, controller = "Errorpage", id = "Index", action = "NotFound" });
            return RedirectToRoute("default", new { lang = lang, controller = "Home", id = "Index", action = "Index" });
        }
    }
}