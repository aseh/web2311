﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace web.Controllers
{
    public class SESHAController : FrontendController
    {
        // GET: SESHA
        public ActionResult Index()
        {
            Init();

            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/technology/sip-enabling-smart-hearable-applications");
        }
    }
}