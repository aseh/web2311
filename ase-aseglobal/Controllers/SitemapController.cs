﻿using System.Web.Mvc;

namespace web.Controllers
{
    public class SiteMapController: FrontendController
    {
        public ActionResult Index()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }
            
            ViewBag.page_title = "網站導覽";
            return View();
        }
        
    }
}