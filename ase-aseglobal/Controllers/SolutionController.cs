﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using web.Common;
using web.Models;

namespace web.Controllers
{
    public class SolutionController : FrontendController
    {
        public ActionResult Index()
        {
            
            Init();
            return Redirect("/" + template + "/heterogeneous_integration");
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            AddJs("ir.js");
            return View(template + "/Solution/solutions");
            
        }
        public ActionResult industry4point0()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Solution/industry4point0");
        }
        public ActionResult smart_automotive()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Solution/smart_automotive");
        }
        public ActionResult smart_bike()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Solution/smart_bike");
        }
        public ActionResult smart_city()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Solution/smart_city");
        }
        public ActionResult smart_living()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Solution/smart_living");
        }

        public ActionResult smart_factory()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Solution/smart_factory");
        }

        public ActionResult smart_factory_5g() {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Solution/5g_smart_factory");
        }

        [ActionName("5g_smart_factory")]
        public ActionResult _5g_smart_factory()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }
            return Redirect("/" + template + "/solution/smart_factory_5g");
        }

        [ActionName("semiconductor-supply-chain")]
        public ActionResult semiconductor_supply_chain()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }
            return View(template + "/Solution/semiconductor-supply-chain");
        }
    }
}