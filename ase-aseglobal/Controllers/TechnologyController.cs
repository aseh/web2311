﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using web.Common;
using web.Models;

namespace web.Controllers
{
    //[RequireHttps]
    public class TechnologyController : FrontendController
    {
        //public ActionResult Index()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/Technology/technology");
        //}

        [ActionName("fopop")]
        public ActionResult fopop()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Technology/technology_innovation/fopop");
        }

        [ActionName("3d-system-in-package")]
        public ActionResult D3()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Technology/technology_innovation/3d-system-in-package");
        }

        [ActionName("double-side-molding")]
        public ActionResult DSM()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Technology/technology_innovation/double-side-molding");
        }
        public ActionResult Technology_Innovation(string param1 = "", string param2 = "")
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            if (string.IsNullOrEmpty(param1))
            {
                string querysting = HttpContext.Request.QueryString.ToString().Trim();

                if (!string.IsNullOrEmpty(querysting))
                {
                    string urls = HttpContext.Request.Url.ToString();
                    string queryString = "?" + HttpContext.Request.QueryString.ToString().Trim();
                    urls = urls.Replace(queryString, "");
                    Response.StatusCode = 301;
                    Response.AppendHeader("Location", urls);
                }
                return View(template + "/Technology/technology_innovation");
            }
            else
            {
                if (string.IsNullOrEmpty(param2))
                {
                    return View(template + "/Technology/Technology_Innovation/" + param1);
                }
                else
                {
                    return View(template + "/Technology/Technology_Innovation/" + param1 + "/" + param2);
                }
            }
        }
        public ActionResult Intellectual_Property()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Technology/technology_intellectual_property");
        }
        //public ActionResult ASE_Trademarks()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/Technology/technology_trademarks");
        //}

        //----
        public ActionResult advanced_25DIC()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Technology/technology_innovation/technology_25d_ic");
        }
        public ActionResult fan_out()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Technology/technology_innovation/technology_fan_out");
        }
        //public ActionResult sip_id()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/Technology/technology_innovation/technology_sip_id");
        //}
        public ActionResult sip_module()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Technology/technology_innovation/technology_sip_module_solutions");
        }
        public ActionResult flip_chip()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Technology/technology_innovation/technology_flip_chip");
        }
        //public ActionResult tsv_interposer_integration()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/Technology/technology_innovation/technology_tsv_interposer_integration");
        //}
        //public ActionResult wafer_level_ipd()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/Technology/technology_innovation/technology_ipd");
        //}
        //public ActionResult wafer_level_mems()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/Technology/technology_innovation/technology_wafer_level_mems");
        //}
        public ActionResult mems_sensors()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Technology/technology_innovation/technology_mems_sensor_solutions");
        }
        //public ActionResult embedded_die_technology()
        public ActionResult embedded_die()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Technology/technology_innovation/technology_embedded_die_technology");
        }
        //public ActionResult test()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/Technology/technology_innovation/technology_core_test");
        //}
        //----

        //public ActionResult flip_chip_csp()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/Technology/technology_innovation/flip_chip/technology_flip_chip_csp");
        //}
        //public ActionResult flip_chip_bga()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/Technology/technology_innovation/flip_chip/technology_flip_chip_bga");
        //}
        //public ActionResult hp_flip_chip_bga()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/Technology/technology_innovation/flip_chip/technology_hp_flip_chip_bga");
        //}
        //public ActionResult cu_pillar()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/Technology/technology_innovation/flip_chip/technology_cu_pillar");
        //}
        //public ActionResult solder_bumping()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/Technology/technology_innovation/flip_chip/technology_solder_bumping");
        //}
        //----
        //public ActionResult embedded_die(string param1 = "")
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    if (string.IsNullOrEmpty(param1))
        //    {
        //        return View(template + "/Technology/embedded_die");
        //    }
        //    return View(template + "/Technology/embedded_die/" + param1);
        //}

        //public ActionResult embedded_die()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    //if (string.IsNullOrEmpty(param1))
        //    //{
        //    //    return View(template + "/Technology/embedded_die");
        //    //}
        //    return View(template + "/HTML/embedded_die_technology");
        //}

        //public ActionResult wl_mems()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    AddJs("ir.js");
        //    return View(template + "/Technology/technology_innovation/wl_mems");
        //}


        //public ActionResult wl_ipd()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    AddJs("ir.js");
        //    return View(template + "/Technology/technology_innovation/technology_ipd");
        //}

        //public ActionResult aeasi()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    if (string.IsNullOrEmpty(param1))
        //    {
        //        return View(template + "/Technology/embedded_die_technology/technology_aeasi");
        //    }
        //    return View(template + "/Technology/technology_innovation/embedded_die_technology/technology_aeasi");
        //}

        //public ActionResult sesub()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    if (string.IsNullOrEmpty(param1))
        //    {
        //        return View(template + "/Technology/embedded_die_technology/technology_aeasi");
        //    }
        //    return View(template + "/Technology/technology_innovation/embedded_die_technology/technology_sesub");
        //}

        public ActionResult antenna_in_package()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Technology/technology_innovation/antenna_in_package");
        }
        [ActionName("silicon-photonics")]
        public ActionResult silicon_photonics()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Technology/technology_innovation/silicon-photonics");
        }

        [ActionName("fan-out-sip")]
        public ActionResult fan_out_sip()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Technology/technology_innovation/fan-out-sip");
        }
    }
}