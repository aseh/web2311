﻿using System.Web.Mvc;

namespace web.Controllers
{
    public class Terms_of_UseController : FrontendController
    {
        public ActionResult Index()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Terms_of_Use/term_of_use");
        }
    }
}