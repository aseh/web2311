﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using web.Areas.backend.Models;
using web.Common;
using web.Models;
using web.ViewModels;

namespace web.Controllers
{
    public class csrController : FrontendController
    {
        private f_csr_model model = new f_csr_model();
        private F_csr_report_model report_model = new F_csr_report_model();
        private F_csr_report_count_model count_model = new F_csr_report_count_model();
        private f_csr_report_questionnaire_model questionnaire_model = new f_csr_report_questionnaire_model();
        private resevation_model resevation_model = new resevation_model();

        public ActionResult green_technology_education_center()
        {
            Init();

            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            var Setting = resevation_model.getSetting();

            return View(template + "/CSR/green_technology_education_center", new ResevationHome()
            {
                EnrollCount = this.resevation_model.getNowEnrollCount(),
                MaxCount = Setting.limite_people,
                MinCount = Setting.min_people,
                Times = this.resevation_model.getTimes(),
                Mail = Setting.mailCC
            });
        }

        [ActionName("questionnaire")]
        public ActionResult green_technology_education_center_feedback()
        {
            Init();

            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            GenerateVCode();

            return View(template + "/CSR/green_technology_education_center_feedback", new ResevationQuest());
        }

        [HttpPost]
        [ActionName("questionnaire")]
        public ActionResult green_technology_education_center_feedback(ResevationQuest Form)
        {
            Init();

            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            if (!ValidVCode(Form._VCodeKey, Form._VCode))
            {
                // 驗證未通過，產生新的驗證碼
                GenerateVCode();
                this.ShowMessage("驗證碼錯誤", "error");
                return View(template + "/CSR/green_technology_education_center_feedback", Form);
            } else
            {
                resevation_model.addQuest(Form);

                this.ShowMessage("成功", "success");
                return RedirectToAction("green_technology_education_center");
            }


        }


        public ActionResult Index()
        {
            Init();
            AddJs("vendor/hammer.min.js");
            AddJs("csr_index.js");

            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            csr_news data = model.GetLastNews(this.language.lang);
            return View(template + "/CSR/csr", data);
        }
        //public ActionResult news(int year = 0, string title = "")
        //{
        //    Init();

        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }
        //    List<csr_news> list = model.GetList(this.language.lang, year, title);

        //    //record parameters
        //    ViewBag.year = year;
        //    ViewBag.title = title;

        //    //year of data
        //    List<int> year_list = model.GetNewsYear(this.language.lang);
        //    ViewBag.year_list = year_list;

        //    return View(template + "/CSR/news", list);
        //}
        //public ActionResult csr_news_details(long param1)
        //{
        //    Init();

        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    csr_news data = model.GetData(param1);

        //    List<csr_news> list = model.GetList(this.language.lang);
        //    csr_news prev = null, next = null;
        //    bool find = false;

        //    if (list.Count > 0)
        //    {
        //        foreach (csr_news item in list)
        //        {
        //            if (find)
        //            {
        //                next = item;
        //                break;
        //            }

        //            if (item.id == data.id)
        //            {
        //                find = true;
        //            }
        //            else
        //            {
        //                prev = item;
        //            }
        //        }
        //    }
        //    ViewBag.prev = prev;
        //    ViewBag.next = next;
        //    return View(template + "/CSR/csr_news_details", data);
        //}


        public ActionResult downloads(string param1 = "", string param2 = "")
        {
            Init();

            if (browser_not_support)
            {
                return View(template + "/browser");
            }
            if (string.IsNullOrEmpty(param1))
            {
                return View(template + "/CSR/Downloads");
            }
            else if (!string.IsNullOrEmpty(param2))
            {
                string rRedirect = string.Format("/public/{0}/404.html", this.language.lang);
                return Redirect(rRedirect);
            }
            else
            {
                List<csr_report> list = report_model.GetList(this.language.lang);
                if (param1 == "report")
                {
                    return View(template + "/CSR/csr_reports", list);
                }
                else if (param1 == "video")
                {
                    return View(template + "/CSR/csr_video");
                }
                else
                {
                    Response.StatusCode = 301;
                    Response.Status = "301 Moved Permanently";
                    Response.AppendHeader("Location", string.Format("http://ase.aseglobal.com/{0}/csr/downloads", this.language.lang));
                    Response.AppendHeader("Cache-Control", "no-cache");
                    return new EmptyResult();
                }
            }
        }
        public ActionResult download(long param1)
        {
            csr_report data = report_model.GetData(param1);

            string file_path = Server.MapPath(Url.Content("~/" + this.local_media_bank_path + "/")) + data.filename;

            FileInfo fi = new FileInfo(file_path);
            string extension;

            extension = Path.GetExtension(file_path);
            Stream iStream = new FileStream(file_path, FileMode.Open, FileAccess.Read, FileShare.Read);
            return File(iStream, "Application/pdf", data.title + extension);
        }
        public ActionResult addcountdata(long param1)
        {
            csr_report data = report_model.GetData(param1);

            csr_report_count count_data = new csr_report_count();
            count_data.report_id = data.id;
            count_data.ip = Tool.GetCurrentIP();
            count_data.create_date = DateTime.Now;
            count_model.SaveData(count_data);

            return Json(count_data.id);
        }
        //public ActionResult csr_report_questionnaire(long param1 = 0, long param2 = 0)
        //{
        //    Init();

        //    // 產生驗證碼
        //    ImageVerificationCodeModel iv = new ImageVerificationCodeModel(HttpContext.Cache, Server.MapPath("/"));
        //    ImageVerificationCodeModel ivdata = iv.CreateIVCode();
        //    ViewData["key"] = ivdata.CacheKey;
        //    ViewData["img"] = ivdata.URL;

        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    long report_id = 0;
        //    if (param1 > 0)
        //    {
        //        csr_report report = report_model.GetData(param1);
        //        report_id = report.id;
        //    }

        //    long count_id = 0;
        //    if (param2 > 0)
        //    {
        //        csr_report_count count = count_model.GetData(param2);
        //        count_id = count.id;
        //    }

        //    csr_report_questionnaire data = new csr_report_questionnaire();
        //    string default_choice = "VeryHigh";
        //    data.q1 = default_choice;
        //    data.q2 = default_choice;
        //    data.q3 = default_choice;
        //    data.q4 = default_choice;
        //    data.q5 = default_choice;
        //    data.q6 = default_choice;
        //    data.q7 = default_choice;
        //    data.q8 = default_choice;
        //    data.q9 = default_choice;
        //    data.q10 = default_choice;
        //    data.q11 = default_choice;
        //    data.q12 = default_choice;
        //    data.q13 = default_choice;
        //    data.q14 = default_choice;
        //    data.q15 = default_choice;
        //    data.q16 = default_choice;
        //    data.q17 = default_choice;
        //    data.q18 = default_choice;
        //    data.q19 = default_choice;
        //    data.q20 = default_choice;
        //    data.q21 = default_choice;
        //    data.q22 = default_choice;
        //    data.q23 = default_choice;

        //    ViewBag.report_id = report_id;
        //    ViewBag.count_id = count_id;

        //    return View(template + "/CSR/csr_report_questionnaire", data);
        //}
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Content(FormCollection fc)
        {
            Init();

            long report_id = Tool.TryToParseLong(fc["report_id"]);
            long count_id = Tool.TryToParseLong(fc["count_id"]);

            csr_report_questionnaire data = FcToData(fc);
            if (UpdateData(data, fc))
            {
                if (this.language.lang == "en")
                {
                    ShowUpdateMessage("Thanks for your valuable comments.");
                }
                else
                {
                    ShowUpdateMessage("謝謝您寶貴的意見!");
                }

                // update report count
                if (count_id > 0)
                {
                    csr_report_count count_data = count_model.GetData(count_id);
                    count_data.questionnaire_id = data.id;
                    count_model.SaveData(count_data);
                }

                if (count_id == 0)
                {
                    return RedirectToAction("csr_report_questionnaire");
                }
                return RedirectToAction("csr_report_questionnaire", new { param1 = report_id, param2 = count_id });
            }

            Response.Cookies.Clear();
            Session.Abandon();
            //停用瀏覽器快取
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            ImageVerificationCodeModel iv2 = new ImageVerificationCodeModel(HttpContext.Cache, Server.MapPath("/"));
            ImageVerificationCodeModel ivdata = iv2.CreateIVCode();
            ViewData["key"] = ivdata.CacheKey;
            ViewData["img"] = ivdata.URL;

            ViewBag.report_id = report_id;
            ViewBag.count_id = count_id;

            ShowUpdateFail("Error.");
            return View(template + "/CSR/csr_report_questionnaire", data);
        }
        private csr_report_questionnaire FcToData(FormCollection fc)
        {
            csr_report_questionnaire data = new csr_report_questionnaire();
            string stakeholder = fc["stakeholder"] == null ? "" : fc["stakeholder"].Trim();
            if (stakeholder.ToLower() == "other")
            {
                data.other = fc["other"] == null ? "" : "Other-" + fc["other"].Trim();
            }
            data.stakeholder = stakeholder;
            data.q1 = fc["Corporate"] == null ? "" : fc["Corporate"].Trim();
            data.q2 = fc["Ethics"] == null ? "" : fc["Ethics"].Trim();
            data.q3 = fc["Regulatory"] == null ? "" : fc["Regulatory"].Trim();
            data.q4 = fc["Stakeholders"] == null ? "" : fc["Stakeholders"].Trim();
            data.q5 = fc["Strategy"] == null ? "" : fc["Strategy"].Trim();
            data.q6 = fc["Customer"] == null ? "" : fc["Customer"].Trim();
            data.q7 = fc["Taxation"] == null ? "" : fc["Taxation"].Trim();
            data.q8 = fc["Supply"] == null ? "" : fc["Supply"].Trim();
            data.q9 = fc["Innovation"] == null ? "" : fc["Innovation"].Trim();
            data.q10 = fc["Risk"] == null ? "" : fc["Risk"].Trim();
            data.q11 = fc["Climate"] == null ? "" : fc["Climate"].Trim();
            data.q12 = fc["Waste"] == null ? "" : fc["Waste"].Trim();
            data.q13 = fc["Water"] == null ? "" : fc["Water"].Trim();
            data.q14 = fc["Energy"] == null ? "" : fc["Energy"].Trim();
            data.q15 = fc["Green"] == null ? "" : fc["Green"].Trim();
            data.q16 = fc["Training"] == null ? "" : fc["Training"].Trim();
            data.q17 = fc["Equality"] == null ? "" : fc["Equality"].Trim();
            data.q18 = fc["Human"] == null ? "" : fc["Human"].Trim();
            data.q19 = fc["Labor"] == null ? "" : fc["Labor"].Trim();
            data.q20 = fc["Health"] == null ? "" : fc["Health"].Trim();
            data.q21 = fc["Community"] == null ? "" : fc["Community"].Trim();
            data.q22 = fc["Charity"] == null ? "" : fc["Charity"].Trim();
            data.q23 = fc["Initiatives"] == null ? "" : fc["Initiatives"].Trim();
            data.additional = fc["additional"] == null ? "" : fc["additional"].Trim();
            data.ip = Tool.GetCurrentIP();
            data.create_date = DateTime.Now;
            data.update_date = DateTime.Now;

            return data;
        }
        private bool UpdateData(csr_report_questionnaire data, FormCollection fc)
        {
            int affected_row = 0;

            //驗證
            //---------------------------------------------------------------------
            //錯誤訊息
            Dictionary<string, string> error = new Dictionary<string, string>();

            bool is_valid = true;

            #region 驗證
            string key = fc["key"] == null ? "" : Server.HtmlEncode(fc["key"]);
            string vcode = fc["vcode"] == null ? "" : Server.HtmlEncode(fc["vcode"]);

            ImageVerificationCodeModel iv = new ImageVerificationCodeModel(HttpContext.Cache);
            if (!iv.isVerificationCodeMatch(key, vcode))
            {
                // 驗證未通過，產生新的驗證碼
                is_valid = false;
                error.Add("vcode_img", "Necessary");
            }

            if (string.IsNullOrEmpty(data.stakeholder))
            {
                is_valid = false;
                error.Add("stakeholder", "Necessary");
            }
            else if (data.stakeholder.ToLower() == "other" && string.IsNullOrEmpty(data.other))
            {
                is_valid = false;
                error.Add("other", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q1))
            {
                is_valid = false;
                error.Add("q1", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q2))
            {
                is_valid = false;
                error.Add("q2", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q3))
            {
                is_valid = false;
                error.Add("q3", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q4))
            {
                is_valid = false;
                error.Add("q4", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q5))
            {
                is_valid = false;
                error.Add("q5", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q6))
            {
                is_valid = false;
                error.Add("q6", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q7))
            {
                is_valid = false;
                error.Add("q7", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q8))
            {
                is_valid = false;
                error.Add("q8", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q9))
            {
                is_valid = false;
                error.Add("q9", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q10))
            {
                is_valid = false;
                error.Add("q10", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q11))
            {
                is_valid = false;
                error.Add("q11", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q12))
            {
                is_valid = false;
                error.Add("q12", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q13))
            {
                is_valid = false;
                error.Add("q13", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q14))
            {
                is_valid = false;
                error.Add("q14", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q15))
            {
                is_valid = false;
                error.Add("q15", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q16))
            {
                is_valid = false;
                error.Add("q16", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q17))
            {
                is_valid = false;
                error.Add("q17", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q18))
            {
                is_valid = false;
                error.Add("q18", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q19))
            {
                is_valid = false;
                error.Add("q19", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q20))
            {
                is_valid = false;
                error.Add("q20", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q21))
            {
                is_valid = false;
                error.Add("q21", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q22))
            {
                is_valid = false;
                error.Add("q22", "Necessary");
            }
            if (string.IsNullOrEmpty(data.q23))
            {
                is_valid = false;
                error.Add("q23", "Necessary");
            }
            #endregion

            TempData["error"] = error;
            //---------------------------------------------------------------------
            if (is_valid)
            {
                affected_row += questionnaire_model.SaveData(data);
                if (affected_row > 0)
                {
                    return true;
                }
            }
            return false;
        }


        public ActionResult csr_video()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/CSR/csr_video");
        }
        public ActionResult sustainability_highlights(string param1 = "")
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }
            if (string.IsNullOrEmpty(param1))
            {
                string querysting = HttpContext.Request.QueryString.ToString().Trim();

                if (!string.IsNullOrEmpty(querysting))
                {
                    string urls = HttpContext.Request.Url.ToString();
                    string queryString = "?" + HttpContext.Request.QueryString.ToString().Trim();
                    urls = urls.Replace(queryString, "");
                    Response.StatusCode = 301;
                    Response.AppendHeader("Location", urls);

                    //return View(template + "/CSR/sustainability_highlights");
                }
                return View(template + "/CSR/sustainability_highlights");
            }
            //string sParam = HttpContext.Request.Url.ToString().Replace("?param1=", "//");

            //Response.StatusCode = 301;
            //return;
            //Response.Redirect(sParam);
            //return Response.AppendHeader("Location", sParam);


            return View(template + "/CSR/sustainability_highlights/" + param1);

        }
        //public ActionResult Letter_From_The_Chairman()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/CSR/Letter_From_The_Chairman");
        //}
        public ActionResult Sustainability_Governance()
        {

            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            string querysting = HttpContext.Request.QueryString.ToString().Trim();

            if (!string.IsNullOrEmpty(querysting))
            {
                string urls = HttpContext.Request.Url.ToString();
                string queryString = "?" + HttpContext.Request.QueryString.ToString().Trim();
                urls = urls.Replace(queryString, "");
                Response.StatusCode = 301;
                Response.AppendHeader("Location", urls);
            }
            return View(template + "/CSR/Sustainability_Governance");
        }
        //public ActionResult governance(string param1)
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }
        //    if (string.IsNullOrEmpty(param1))
        //    {
        //        return View(template + "/CSR/Sustainability_Governance");
        //    }
        //    return View(template + "/CSR/governance/" + param1);
        //}
        //public ActionResult stakeholder_engagement(string param1 = "")
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }
        //    if (string.IsNullOrEmpty(param1))
        //    {
        //        string querysting = HttpContext.Request.QueryString.ToString().Trim();

        //        if (!string.IsNullOrEmpty(querysting))
        //        {
        //            string urls = HttpContext.Request.Url.ToString();
        //            string queryString = "?" + HttpContext.Request.QueryString.ToString().Trim();
        //            urls = urls.Replace(queryString, "");
        //            Response.StatusCode = 301;
        //            Response.AppendHeader("Location", urls);
        //        }
        //        return View(template + "/CSR/Stakeholder_Engagement");
        //    }
        //    return View(template + "/CSR/Stakeholder_Engagement/" + param1);
        //}
        public ActionResult environmental_sustainability(string param1 = "", string param2 = "")
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }
            if (string.IsNullOrEmpty(param1))
            {
                string querysting = HttpContext.Request.QueryString.ToString().Trim();

                if (!string.IsNullOrEmpty(querysting))
                {
                    string urls = HttpContext.Request.Url.ToString();
                    string queryString = "?" + HttpContext.Request.QueryString.ToString().Trim();
                    urls = urls.Replace(queryString, "");
                    Response.StatusCode = 301;
                    Response.AppendHeader("Location", urls);
                }
                return View(template + "/CSR/environmental_sustainability");
            }
            else
            {
                if (string.IsNullOrEmpty(param2))
                {
                    if (param1 == "sustainable_manufacturing")
                    {
                        AddJs("vendor/jquery.jmap.min.js");
                        AddJs("vendor/jquery.rwdImageMaps.js");
                    }
                    return View(template + "/CSR/environmental_sustainability/" + param1);
                }
                else
                {
                    return View(template + "/CSR/environmental_sustainability/" + param1 + "/" + param2);
                }
            }
        }
        public ActionResult employee_care_development(string param1 = "")
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            if (string.IsNullOrEmpty(param1))
            {
                string querysting = HttpContext.Request.QueryString.ToString().Trim();

                if (!string.IsNullOrEmpty(querysting))
                {
                    string urls = HttpContext.Request.Url.ToString();
                    string queryString = "?" + HttpContext.Request.QueryString.ToString().Trim();
                    urls = urls.Replace(queryString, "");
                    Response.StatusCode = 301;
                    Response.AppendHeader("Location", urls);
                }
                return View(template + "/CSR/Employee_Care_Development");
            }
            return View(template + "/CSR/employee_care/" + param1);
        }

        
        //public ActionResult employee_care_and_development(string param1 = "")
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    if (string.IsNullOrEmpty(param1))
        //    {
        //        string querysting = HttpContext.Request.QueryString.ToString().Trim();

        //        if (!string.IsNullOrEmpty(querysting))
        //        {
        //            string urls = HttpContext.Request.Url.ToString();
        //            string queryString = "?" + HttpContext.Request.QueryString.ToString().Trim();
        //            urls = urls.Replace(queryString, "");
        //            Response.StatusCode = 301;
        //            Response.AppendHeader("Location", urls);
        //        }
        //        return View(template + "/CSR/Employee_Care_Development");
        //    }
        //    return View(template + "/CSR/employee_care/" + param1);
        //}

        public ActionResult supply_chain_development(string param1 = "", string param2 = "")
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            if (string.IsNullOrEmpty(param1))
            {
                //string querysting = HttpContext.Request.QueryString.ToString().Trim();

                //if (!string.IsNullOrEmpty(querysting))
                //{
                //    string urls = HttpContext.Request.Url.ToString();
                //    string queryString = "?" + HttpContext.Request.QueryString.ToString().Trim();
                //    urls = urls.Replace(queryString, "");
                //    Response.StatusCode = 301;
                //    Response.AppendHeader("Location", urls);
                //}
                return View(template + "/CSR/csr_supply_chain_development");
            }
            if (!string.IsNullOrEmpty(param2))
            {
                string rRedirect = string.Format("/public/{0}/404.html", this.language.lang);
                return Redirect(rRedirect);
            }
            return View(template + "/CSR/Supply_Chain_Development/" + param1);
        }
        public ActionResult corporate_citizenship_social_Involvement(string param1 = "")
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            if (string.IsNullOrEmpty(param1))
            {
                string querysting = HttpContext.Request.QueryString.ToString().Trim();

                if (!string.IsNullOrEmpty(querysting))
                {
                    string urls = HttpContext.Request.Url.ToString();
                    string queryString = "?" + HttpContext.Request.QueryString.ToString().Trim();
                    urls = urls.Replace(queryString, "");
                    Response.StatusCode = 301;
                    Response.AppendHeader("Location", urls);
                }
                return View(template + "/CSR/Corporate_Citizenship_Social_Involvement");
            }

            if (param1 == "community_engagement")
            {
                AddJs("vendor/imagesloaded.pkgd.min.js");
                AddJs("vendor/masonry.pkgd.min.js");
                AddCss("vendor/ase_csr.css");
            }
            return View(template + "/CSR/Corporate_Citizenship_Social_Involvement/" + param1);
        }
        public ActionResult corporate_sustainability_policy_and_management(string param1)
        {

            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            if (string.IsNullOrEmpty(param1))
            {
                return View(template + "/CSR/corporate_sustainability_policy_and_management");
            }
            return View(template + "/CSR/corporate_sustainability_policy_and_management/" + param1);
        }
        public ActionResult social_involvement(string param1 = "")
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            if (string.IsNullOrEmpty(param1))
            {
                string querysting = HttpContext.Request.QueryString.ToString().Trim();

                if (!string.IsNullOrEmpty(querysting))
                {
                    string urls = HttpContext.Request.Url.ToString();
                    string queryString = "?" + HttpContext.Request.QueryString.ToString().Trim();
                    urls = urls.Replace(queryString, "");
                    Response.StatusCode = 301;
                    Response.AppendHeader("Location", urls);
                }
                return View(template + "/CSR/social_involvement");
            }
            return View(template + "/CSR/social_involvement/" + param1);
        }
        //public ActionResult Foundation()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/CSR/Foundation");
        //}
        //public ActionResult key_performance()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/CSR/key_performance");
        //}




        public ActionResult csr_blog(long authorid = 0, long typeid = 0, string keyword = "")//long authorid = 0, long typeid = 0, string keyword = ""
        {
            Init();
            f_csr_blog_model model = new f_csr_blog_model();
            List<blog_dtl> data = model.GetTopBlog(ViewBag.language.lang);
            if (browser_not_support)
            {
                return View(template + "/browser",data);
            }
            ViewBag.Authorlist = model.GetAuthorList(ViewBag.language.lang, 0);
            ViewBag.Typelist = model.GetTypeList(ViewBag.language.lang, 0);
            ViewBag.keyword = keyword;
            ViewBag.authorid = authorid;
            ViewBag.typeid = typeid;
            ViewBag.titletext = (this.language.lang == "en")? "All posts":"所有文章";
            blog_type blogtype = model.GetBlog_Type(typeid);
            if (blogtype != null)
            {
                typename tn = JsonConvert.DeserializeObject<typename>(blogtype.name);
                ViewBag.titletext = (ViewBag.language.lang == "ch") ? tn.ch : tn.en;
            }
            else
            {
                blog_author author = model.GetBlog_Author(authorid);
                if (author != null)
                {
                    ViewBag.titletext = author.blog_author_dtl.Where(u => u.lang == ViewBag.language.lang).FirstOrDefault().name;
                }
            }
            return View(template + "/CSR/csr_blog",data);
        }

       
        //public ActionResult blogcontent(string urlid)
        //{
        //    return csr_blog_content_url(urlid);
        //}

        //取得GetAllBlog的第一次資料的時候，以防Server取得的是Cache，必須即時更新
        //[OutputCache(NoStore = true, Duration = 0)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ajaxGetAllBlog(string lang="ch", long authorid = 0, long typeid = 0, int pagesize = 10, int pageno = 0, string keyword = "")
        {
            Init();
            f_csr_blog_model model = new f_csr_blog_model();
            List<blog_dtl> allblog = model.GetAllBlog(lang, authorid, typeid, pagesize, pageno, keyword);
            int allblogcnt = model.GetAllBlogCount(lang, authorid, typeid, keyword);
            ViewBag.isbottom = (pageno > 0 && allblogcnt == 0);
            ViewBag.allblogcnt = allblogcnt;
            
            return PartialView(template + "/CSR/_csr_blog_all", allblog);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ajaxPublishmessage(long blogid,long dtlid,string name,string email,string message)
        {
            Init();
            f_csr_blog_model model = new f_csr_blog_model();
            F_contact_model contact_model = new F_contact_model();
            var blog = model.GetBlogById(blogid);
            var dtl = blog.blog_dtl.First(x => x.id == dtlid);
            var Result = Json(model.SendMessage(blogid, dtlid, name, email, message));
            string tmpString = "<tr><td style='font-family: \"微軟正黑體\", Arial; font-size: 14px; line-height: 1.7em; box-sizing: border-box; padding: 10px; color:#504e4f;border-bottom:1px solid #e6e6e6;width: 150px;vertical-align: top;'>#TITLE#：</td><td style='font-family: \"微軟正黑體\", Arial; font-size: 14px; line-height: 1.7em; box-sizing: border-box; padding:10px; color:#504e4f;border-bottom:1px solid #e6e6e6;'>#CONTENT#</td></p></tr>";
            string form_content = "";
            form_content += tmpString.Replace("#TITLE#", "Blog").Replace("#CONTENT#", dtl.title);
            form_content += tmpString.Replace("#TITLE#", "From").Replace("#CONTENT#", name);
            form_content += tmpString.Replace("#TITLE#", "Email").Replace("#CONTENT#", email);
            form_content += tmpString.Replace("#TITLE#", "Message").Replace("#CONTENT#", message);

            Uri uri = Request.Url;
            string host = uri.Scheme + Uri.SchemeDelimiter + uri.Host + ":" + uri.Port;
            string mail_content = Tool.GetFileToString(Server.MapPath("~/Common/contact_mail.html"));
            mail_content = mail_content.Replace("#picture#", host + @Url.Content("~/public/frontend/images/logo-en.png"));
            mail_content = mail_content.Replace("#mail_content#", form_content);

            //Mail Object settings
            // get (supplier) email receiver
            List<contact_email> contact_list = contact_model.GetReceiverList(3);
            Mail Mail = new Mail();
            Mail.InitMail("有新的Blog留言", mail_content, true, "", "");
            foreach (contact_email receiver in contact_list)
            {
                Mail.AddToEmail(receiver.email);
            }
            Mail.SendEmail();//寄出              

            return Result;
        }

        public ActionResult csr_blog_author(long authorid = 0)//long authorid = 0, long typeid = 0, string keyword = ""
        {
            Init();
            f_csr_blog_model model = new f_csr_blog_model();
            blog_author data = model.GetAuthorData(authorid);
            if (data.picurl == null) data.picurl = "/public/uploads/defaultauthor.png";
            ViewBag.Authorlist = model.GetAuthorList(ViewBag.language.lang, 0);
            ViewBag.Typelist = model.GetTypeList(ViewBag.language.lang, 0);

            if (browser_not_support)
            {
                return View(template + "/browser", data);
            }
            
            return View(template + "/CSR/csr_blog_author", data);
        }

        public ActionResult csr_blog_content(long blogid = 0)//long authorid = 0, long typeid = 0, string keyword = ""
        {
            Init();
            f_csr_blog_model model = new f_csr_blog_model();
            blog data = model.GetBlogById(blogid);
            //紀錄blog_log  this.language.lang
            string ip = GetIPAddress(Request);// Request.UserHostAddress;
            if (data!=null)
                model.AddBlogLog(data, this.language.lang, ip);
            //
            if (browser_not_support)
            {
                return View(template + "/browser", data);
            }
            ViewBag.Authorlist = model.GetAuthorList(ViewBag.language.lang, 0);
            ViewBag.Typelist = model.GetTypeList(ViewBag.language.lang, 0);
            ViewData["prevblog"] = model.GetPrevBlog(blogid, ViewBag.language.lang);
            ViewData["nextblog"] = model.GetNextBlog(blogid, ViewBag.language.lang);

            
            //SEO
            if (data.isseo == 1)
            {
                ViewBag.webSiteOgImage = data.seopicurl;
                ViewBag.Title = data.blog_dtl.Where(u => u.lang == this.language.lang).FirstOrDefault().title;
                string webSiteDesc = data.blog_dtl.Where(u => u.lang == this.language.lang).FirstOrDefault().seodescription;
                ViewBag.webSiteDesc = webSiteDesc; //data.blog_dtl.Where(u => u.lang == this.language.lang).FirstOrDefault().contenttext;
                
            }
            var isHideLang = data.blog_dtl.Where(x => x.lang != this.language.lang).All(x => x.contenttext == "");
            if (isHideLang )
            {
                if ( this.language.lang == "en")
                {
                    ViewBag.zhURL = null;
                } else
                {
                    ViewBag.enURL = null;
                }
            }
            return View(template + "/CSR/csr_blog_content", data);
        }

        public ActionResult blogcontent(string param2)
        {
            return csr_blog_content_url(param2);
        }
        public ActionResult csr_blog_content_url(string urlid)//long authorid = 0, long typeid = 0, string keyword = ""
        {
            Init();
            f_csr_blog_model model = new f_csr_blog_model();
            blog data = model.GetBlogByUrlId(urlid);
            //紀錄blog_log  this.language.lang
            string ip = GetIPAddress(Request);// Request.UserHostAddress;
            if (data != null)
                model.AddBlogLog(data, this.language.lang, ip);
            //
            if (browser_not_support)
            {
                return View(template + "/browser", data);
            }
            ViewBag.Authorlist = model.GetAuthorList(ViewBag.language.lang, 0);
            ViewBag.Typelist = model.GetTypeList(ViewBag.language.lang, 0);
            ViewData["prevblog"] = model.GetPrevBlog(data.id, ViewBag.language.lang);
            ViewData["nextblog"] = model.GetNextBlog(data.id, ViewBag.language.lang);

            //SEO
            if (data.isseo == 1)
            {
                ViewBag.webSiteOgImage = data.seopicurl;
                ViewBag.Title = data.blog_dtl.Where(u => u.lang == this.language.lang).FirstOrDefault().title;
                string webSiteDesc = data.blog_dtl.Where(u => u.lang == this.language.lang).FirstOrDefault().seodescription;
                ViewBag.webSiteDesc = webSiteDesc; //data.blog_dtl.Where(u => u.lang == this.language.lang).FirstOrDefault().contenttext;
                
            }
            var isHideLang = data.blog_dtl.Where(x => x.lang != this.language.lang).All(x => x.contenttext == "");
            if (isHideLang)
            {
                if (this.language.lang == "en")
                {
                    ViewBag.zhURL = null;
                }
                else
                {
                    ViewBag.enURL = null;
                }
            }
            //
            return View(template + "/CSR/csr_blog_content", data);
        }

        public static string GetIPAddress(HttpRequestBase request)
        {
            string ip;
            try
            {
                ip = request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (!string.IsNullOrEmpty(ip))
                {
                    if (ip.IndexOf(",") > 0)
                    {
                        string[] ipRange = ip.Split(',');
                        int le = ipRange.Length - 1;
                        ip = ipRange[le];
                    }
                }
                else
                {
                    ip = request.UserHostAddress;
                }
            }
            catch { ip = null; }

            return ip;
        }
    }
}