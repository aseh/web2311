﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace web.Controllers
{
    public class csr_blogController : FrontendController
    {
        // GET: csr_blog
        public ActionResult Index()
        {
            Init();
            AddJs("vendor/hammer.min.js");

            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/CSR/csr_blog");
        }
    }
}