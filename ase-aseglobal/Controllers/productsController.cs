﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using web.Common;
using web.Models;

namespace web.Controllers
{
    public class productsController : FrontendController
    {
        public ActionResult Test()
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            return View(template + "/Products/Test");
        }
        //public ActionResult Tray()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    return View(template + "/Products/Tray");
        //}
        public ActionResult material(string param1 = "")
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            if (string.IsNullOrEmpty(param1))
            {
                return View(template + "/Products/Material");
            }
            return View(template + "/Products/Material/" + param1);
        }
        //public ActionResult Index()
        //{
        //    Init();
        //    if (browser_not_support)
        //    {
        //        return View(template + "/browser");
        //    }

        //    AddJs("ir.js");
        //    return View(template + "/Products/products");
        //}

        public ActionResult assembly(string param1 = "", string param2 = "")
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            if (string.IsNullOrEmpty(param1))
            {
                string querysting = HttpContext.Request.QueryString.ToString().Trim();

                if (!string.IsNullOrEmpty(querysting))
                {
                    string urls = HttpContext.Request.Url.ToString();
                    string queryString = "?" + HttpContext.Request.QueryString.ToString().Trim();
                    urls = urls.Replace(queryString, "");
                    Response.StatusCode = 301;
                    Response.AppendHeader("Location", urls);
                }
                return View(template + "/Products/Assembly");
            }
            else
            {
                if (string.IsNullOrEmpty(param2))
                {
                    return View(template + "/Products/Assembly/" + param1);
                }
                else
                {
                    return View(template + "/Products/Assembly/" + param1 + "/" + param2);
                }
            }
        }


        public ActionResult assembly_offerings(string param1 = "", string param2 = "")
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            if (string.IsNullOrEmpty(param2))
            {
                return View(template + "/Products/assembly_offerings/" + param1);
            }
            else
            {
                return View(template + "/Products/assembly_offerings/" + param1 + "/" + param2);
            }
        }
        public ActionResult package_design(string param1 = "")
        {
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }

            if (string.IsNullOrEmpty(param1))
            {
                //string querysting = HttpContext.Request.QueryString.ToString().Trim();

                //if (!string.IsNullOrEmpty(querysting))
                //{
                //    string urls = HttpContext.Request.Url.ToString();
                //    string queryString = "?" + HttpContext.Request.QueryString.ToString().Trim();
                //    urls = urls.Replace(queryString, "");
                //    Response.StatusCode = 301;
                //    Response.AppendHeader("Location", urls);
                //}
                return View(template + "/Products/package_design");
            }
            return View(template + "/Products/package_design/" + param1);
        }
    }
}