﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace web.Controllers
{
    public class vipackController : FrontendController
    {
        // GET: vipack
        public ActionResult Index()
        {
            if ( DateTime.Now < DateTime.Parse("2022-06-01 21:00:00", new CultureInfo("zh-TW")))
            {
                throw new Exception("Not Found");
            }
            Init();
            if (browser_not_support)
            {
                return View(template + "/browser");
            }
            return View(template + "/technology/vipack");
        }
    }
}