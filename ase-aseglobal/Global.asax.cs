﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ELFinder.Connector.ASPNet.ModelBinders;
using ELFinder.Connector.Config;
using web.Common;
using web.Config;
using web.Controllers;

namespace web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Use custom model binder
            ModelBinders.Binders.DefaultBinder = new ELFinderModelBinder();

            // Initialize ELFinder configuration
            InitELFinderConfiguration();
        }

        protected void Application_BeginRequest()
        {

            Context.Items["IIS_WasUrlRewritten"] = false;
            string[] PathArrray;//轉址使用
            string default_language = ConfigurationManager.AppSettings["default_language"];
            //ViewBag.Lang = lang;
            string path = Request.RawUrl;
            bool sQueryString = ((path.IndexOf("?param1=") > 0) || (path.IndexOf("&param2=") > 0)) ? true : false;

            //判斷是否須轉址 (以使用者的瀏覽器語言設定為主)
            //if (path.Equals("/"))
            //{
            //    string redirectUri = default_language;
            //    if (!string.IsNullOrEmpty(redirectUri))
            //    {
            //        Response.StatusCode = 301;
            //        Response.AppendHeader("Location", redirectUri);
            //        //Response.Redirect(redirectUri);
            //    }
            //}
            // ?param1= 轉址
            if (sQueryString)
            {
                path = path.Replace("?param1=", "/").Replace("&param2=", "/");

                if (path.Contains("embedded_die_technology"))
                {
                    if (path.Equals("/en/html/embedded_die_technology"))
                    {
                        path = "/en/technology/embedded_die";
                    }
                    else if (path.Equals("/ch/html/embedded_die_technology"))
                    {
                        path = "/ch/technology/embedded_die";
                    }
                }
                Response.StatusCode = 301;
                Response.Status = "301 Moved Permanently";
                Response.AppendHeader("Location", path);
            }


            ////URL 301 || path.Contains("?l=f96944e7-b295-41cc-bcab-611c54b39dff")
            if (path.Contains("index") || path.Contains("home") || path.Contains("assembly_offerings") || path.Contains("/?l=f96944e7-b295-41cc-bcab-611c54b39dff") || path.Contains("/?l=56a3e72c-e1be-42ee-b569-0f3f0ab90b10"))
            {
                string sAppendHeaderValue = "", sKeyIndex = "";
                PathArrray = path.Split('/');
                if (PathArrray.Length > 3)
                    sKeyIndex = PathArrray[3].ToString().ToLower();
                //
                if (sKeyIndex == "index")
                {
                    sAppendHeaderValue = string.Format("/{0}/{1}", PathArrray[1].ToString(), PathArrray[2].ToString());
                    Response.StatusCode = 301;
                    Response.Status = "301 Moved Permanently";
                    Response.AppendHeader("Location", sAppendHeaderValue);
                }
                else if (path.Equals("/?l=f96944e7-b295-41cc-bcab-611c54b39dff"))
                {
                    Response.StatusCode = 301;
                    Response.Status = "301 Moved Permanently";
                    Response.AppendHeader("Location", "/en");
                }
                else if (path.Equals("/?l=56a3e72c-e1be-42ee-b569-0f3f0ab90b10"))
                {
                    Response.StatusCode = 301;
                    Response.Status = "301 Moved Permanently";
                    Response.AppendHeader("Location", "/en");
                }
                else if (path.Equals("/en/home") || path.Equals("/ch/home"))
                {
                    sAppendHeaderValue = string.Format("/{0}", PathArrray[1].ToString());
                    Response.StatusCode = 301;
                    Response.Status = "301 Moved Permanently";
                    Response.AppendHeader("Location", sAppendHeaderValue);
                }
                else if (path.Equals("/en/products/assembly_offerings") || path.Equals("/ch/products/assembly_offerings"))
                {
                    sAppendHeaderValue = string.Format("/{0}/products/assembly", PathArrray[1].ToString());
                    Response.StatusCode = 301;
                    Response.Status = "301 Moved Permanently";
                    Response.AppendHeader("Location", sAppendHeaderValue);
                }
                //else if (path.Equals("/en/html/embedded_die_technology") || path.Equals("/ch/html/embedded_die_technology"))
                //{
                //    sAppendHeaderValue = string.Format("/{0}/technology/embedded_die", PathArrray[1].ToString());
                //    Response.StatusCode = 301;
                //    Response.Status = "301 Moved Permanently";
                //    Response.AppendHeader("Location", sAppendHeaderValue);
                //}

                //    if (path.Equals("/en/about/index"))
                //    {
                //        Response.StatusCode = 301;
                //        Response.Status = "301 Moved Permanently";
                //        Response.AppendHeader("Location", "/en/about");
                //    }else if (path.Equals("/en/downloads/index"))
                //    {
                //        Response.StatusCode = 301;
                //        Response.Status = "301 Moved Permanently";
                //        Response.AppendHeader("Location", "/en/downloads");
                //    }
                //    else if (path.Equals("/en/events/index"))
                //    {
                //        Response.StatusCode = 301;
                //        Response.Status = "301 Moved Permanently";
                //        Response.AppendHeader("Location", "/en/events");
                //    }
                //    else if (path.Equals("/en/terms_of_use/index"))
                //    {
                //        Response.StatusCode = 301;
                //        Response.Status = "301 Moved Permanently";
                //        Response.AppendHeader("Location", "/en/terms_of_use");

                //    }
                //    else if (path.Equals("/en/solution/index"))
                //    {
                //        Response.StatusCode = 301;
                //        Response.Status = "301 Moved Permanently";
                //        Response.AppendHeader("Location", "/en/solution");
                //    }
                //    else if (path.Equals("/en/home/index")|| path.Equals("/en/home"))
                //    {
                //        Response.StatusCode = 301;
                //        Response.Status = "301 Moved Permanently";
                //        Response.AppendHeader("Location", "/en");
                //    }
                //    else if (path.Equals("/ch/home/index") || path.Equals("/ch/home"))
                //    {
                //        Response.StatusCode = 301;
                //        Response.Status = "301 Moved Permanently";
                //        Response.AppendHeader("Location", "/ch");
                //    }
                //    else if (path.Equals("/?l=f96944e7-b295-41cc-bcab-611c54b39dff"))
                //    {
                //        Response.StatusCode = 301;
                //        Response.Status = "301 Moved Permanently";
                //        Response.AppendHeader("Location", "/en");
                //    }
                //    else if (path.Equals("/?l=56a3e72c-e1be-42ee-b569-0f3f0ab90b10"))
                //    {
                //        Response.StatusCode = 301;
                //        Response.Status = "301 Moved Permanently";
                //        Response.AppendHeader("Location", "/en");
                //    }
                //    else if (path.Equals("/?l=56a3e72c-e1be-42ee-b569-0f3f0ab90b10"))
                //    {
                //        Response.StatusCode = 301;
                //        Response.Status = "301 Moved Permanently";
                //        Response.AppendHeader("Location", "/en");
                //    }
                //    else if (path.Equals("/ch/products/index"))
                //    {
                //        Response.StatusCode = 301;
                //        Response.Status = "301 Moved Permanently";
                //        Response.AppendHeader("Location", "/ch/products");
                //    }
                //    else if (path.Equals("/ch/technology/index"))
                //    {
                //        Response.StatusCode = 301;
                //        Response.Status = "301 Moved Permanently";
                //        Response.AppendHeader("Location", "/ch/technology");
                //    }
            }
        }



        /// <summary>
        /// Initialize ELFinder configuration
        /// </summary>
        protected void InitELFinderConfiguration()
        {

            SharedConfig.ELFinder = new ELFinderConfig(
                Server.MapPath("~/public/uploads"),
                thumbnailsUrl: "~/public/uploads/Thumbnails/"
                );

            SharedConfig.ELFinder.RootVolumes.Add(
                new ELFinderRootVolumeConfigEntry(
                    Server.MapPath("~/public/uploads"),
                    isLocked: false,
                    isReadOnly: false,
                    isShowOnly: false,
                    maxUploadSizeKb: 10240,      // null = Unlimited upload size
                    uploadOverwrite: true,
                    startDirectory: ""));

        }

    protected void Application_Error()
    {
      var exception = Server.GetLastError();
      var httpException = exception as HttpException;

      Response.Clear();
      Server.ClearError();
      var routeData = new RouteData();
      routeData.Values["controller"] = "Errors";
      routeData.Values["action"] = "General";
      routeData.Values["exception"] = exception;
      Response.StatusCode = 500;
      if (httpException != null)
      {
        Response.StatusCode = httpException.GetHttpCode();
        switch (Response.StatusCode)
        {
          case 403:
            routeData.Values["action"] = "Http403";
            break;
          case 404:
            routeData.Values["action"] = "Http404";
            break;
        }
      }
      IController errorsController = new ErrorpageController();
      var rc = new RequestContext(new HttpContextWrapper(Context), routeData);
      errorsController.Execute(rc);

    }
    protected void Application_EndRequest()
        {
            if (Context.Response.StatusCode == 404)
            {
                if ((!Request.RawUrl.Contains("style")) && (!Request.RawUrl.Contains("images")))
                {
                    Response.Clear();
                    if (Response.StatusCode == 404)
                    {
                        var routeData = new RouteData();
                        routeData.Values["controller"] = "Errors";
                        routeData.Values["action"] = "Http404";

                        IController errorsController = new ErrorpageController();
                        var rc = new RequestContext(new HttpContextWrapper(Context), routeData);
                        errorsController.Execute(rc);
                    }
                }
            }
        }
    }
}
