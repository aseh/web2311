﻿using System.Linq;
using System.Collections.Generic;

namespace web.Models
{
    public class F_contact_model : base_model
    {
        public List<contact> GetList(string lang)
        {
            List<contact> list = new List<contact>();
            var query = from item in db.contact
                        select item;
            list = query.ToList();
            return list;
        }

        public List<contact_email> GetReceiverList(int? type)
        {
            List<contact_email> list = new List<contact_email>();
            var query = from item in db.contact_email
                        where item.type == type || type == null
                        where item.launch == 1
                        select item;
            list = query.ToList();
            return list;
        }

        public int SaveData(contact update_data)
        {
            contact data = new contact();
            db.contact.Add(update_data);
            return db.SaveChanges();
        }

    }
}
