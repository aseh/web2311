﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using web.Areas.backend.Models;

namespace web.Models
{
    public class f_csr_blog_model : base_model
    {

        private long _alltypeid
        {
            get
            {
                return this.GetAllTypeId();
            }
           
        }
        public List<blog_dtl> GetTopBlog(string lang)
        {
            List<blog_dtl> list = new List<blog_dtl>();
            list = db.blog_dtl.Where(u => u.lang == lang && u.blog.launch==1 && u.contenttext != "" && u.blog.toporder > 0).OrderBy(u => u.blog.toporder).Take(3).ToList();
            return list;
        }

        public blog GetBlogById(long id)
        {
            blog list = db.blog.Where(u => u.id == id).FirstOrDefault();
            return list;
        }
        public blog GetBlogByUrlId(string urlid)
        {
            blog list = db.blog.Where(u => u.urlid == urlid).FirstOrDefault();
            return list;
        }
        public List<blog_dtl> GetAllBlog(string lang,long authorid=0,long typeid=0,int pagesize=10,int pageno=0, string keyword = "")
        {
            keyword = HttpUtility.UrlDecode(keyword);
            List<blog_dtl> list = new List<blog_dtl>();
            list = db.blog_dtl.Where(u => u.lang == lang && u.blog.launch== 1 && u.contenttext != "").ToList()
                .Where(u => u.blog.authorid == authorid || authorid == 0)
                .Where(u => typeid == 0 || typeid == _alltypeid ||  u.blog.typeid == typeid)
                .Where(u => u.title.IndexOf(keyword) > -1 || u.contenttext.IndexOf(keyword) > -1 || keyword == "")
                .OrderByDescending(u => u.blog.apply_date).Skip(pageno * pagesize).Take(pagesize).ToList();
            //改發布日期20201127
            //.OrderByDescending(u => u.blog.create_date).Skip(pageno * pagesize).Take(pagesize).ToList();
            return list;
        }

        public int GetAllBlogCount(string lang, long authorid = 0, long typeid = 0, string keyword = "")
        {
            int res = 0;
            res = db.blog_dtl.Where(u => u.lang == lang && u.blog.launch == 1 && u.contenttext != "")
                .Where(u => u.blog.authorid == authorid || authorid == 0)
                .Where(u => typeid == 0 || typeid == _alltypeid || u.blog.typeid == typeid)
                .Where(u => u.title.IndexOf(keyword) > -1 || u.contenttext.IndexOf(keyword) > -1 || keyword == "")
                .Count();
            return res;
        }
        public blog GetPrevBlog(long id, string lang)
        {
            blog thisblog = GetBlogById(id);
            blog res = (thisblog == null) ? new blog() : db.blog.Where(u => u.apply_date < thisblog.apply_date && u.launch == 1 ).OrderByDescending(u => u.apply_date).FirstOrDefault();
            //20201202內容空白的不選取
            if (res!=null && string.IsNullOrEmpty(res.blog_dtl.FirstOrDefault(u => u.lang == lang).contenttext))
            {
                res = GetPrevBlog(res.id, lang);
            }
            //改發布日期20201127
            //blog res =(thisblog==null)?new blog(): db.blog.Where(u => u.create_date < thisblog.create_date && u.launch == 1).OrderByDescending(u => u.create_date).FirstOrDefault();
            return res;
        }
        public blog GetNextBlog(long id, string lang)
        {
            blog thisblog = GetBlogById(id);
            blog res = (thisblog == null) ? new blog() : db.blog.Where(u => u.apply_date > thisblog.apply_date && u.launch == 1).OrderBy(u => u.apply_date).FirstOrDefault();
            //20201202內容空白的不選取
            if (res != null && string.IsNullOrEmpty(res.blog_dtl.FirstOrDefault(u => u.lang == lang).contenttext))
            {
                res = GetNextBlog(res.id, lang);
            }
            //改發布日期20201127
            //blog res = (thisblog == null) ? new blog() : db.blog.Where(u => u.create_date > thisblog.create_date && u.launch == 1).OrderBy(u => u.create_date).FirstOrDefault();
            return res;
        }

        public List<SelectListItem> GetAuthorList(string lang, long authorid)
        {
            List<SelectListItem> Authorlist = new List<SelectListItem>();
            List<blog_author_dtl> authordtl = db.blog_author_dtl.Where(u => u.lang == lang).ToList();
            //排除掉未設定的author ==>先不排除
            //blog_author_dtl tmp = db.blog_author_dtl.Where(u => u.name == "(none)").FirstOrDefault();
            //if (tmp != null)
            //{
            //    authordtl = authordtl.Where(u => u.blog_author.id != tmp.blog_author.id).ToList();
            //}
            if (authordtl.Count > 0)
            {
                foreach (var a in authordtl)
                {
                    SelectListItem li = new SelectListItem();
                    li.Text = a.name;
                    li.Value = a.authorid.ToString();
                    //li.Selected = (a.authorid == authorid);
                    Authorlist.Add(li);
                }
            }
            SelectListItem al = new SelectListItem(); al.Text =(lang=="ch")? "選擇作者":"Author"; al.Value = "0";
            //SelectListItem al = new SelectListItem(); al.Text = (lang == "ch") ? "" : ""; al.Value = "0";
            if (authorid == 0) al.Selected = true;
            Authorlist.Insert(0, al);
            return Authorlist;
        }

        public blog_type GetBlog_Type(long typeid)
        {
            return db.blog_type.Where(u => u.id == typeid).FirstOrDefault();
        }
        public blog_author GetBlog_Author(long authorid)
        {
            return db.blog_author.Where(u => u.id == authorid).FirstOrDefault();
        }

        public List<SelectListItem> GetTypeList(string lang, long typeid)
        {
            List<SelectListItem> Typelist = new List<SelectListItem>();
            List<blog_type> types = db.blog_type.ToList();
            if (types.Count > 0)
            {
                foreach (var t in types)
                {
                    typename tn = JsonConvert.DeserializeObject<typename>(t.name);
                    SelectListItem li = new SelectListItem();
                    li.Text = (lang == "ch") ? tn.ch : tn.en;
                    li.Value = t.id.ToString();
                    //li.Selected = (t.id == typeid);
                    Typelist.Add(li);
                }
            }
            SelectListItem al = new SelectListItem(); al.Text = (lang == "ch") ? "選擇分類" : "Categories"; al.Value = "0";
            //SelectListItem al = new SelectListItem(); al.Text = (lang == "ch") ? "" : ""; al.Value = "0";
            if (typeid == 0) al.Selected = true;
            Typelist.Insert(0, al);
            return Typelist;
        }

        private long GetAllTypeId()
        {
            blog_type type = db.blog_type.Where(u => u.name.ToLower().IndexOf(":\"未設定\"") > -1).FirstOrDefault();
            return (type == null) ? 0 : type.id;
        }

        public blog_author GetAuthorData(long id)
        {
            return db.blog_author.Where(u => u.id == id).FirstOrDefault();
        }

        public void AddBlogLog(blog blog, string lang, string ip)
        {
            blog_log log = new blog_log();
            log.ipaddress = ip;
            log.blogid = blog.id;
            log.blogdtlid = blog.blog_dtl.Where(u => u.lang == lang).FirstOrDefault().id;
            db.blog_log.Add(log);
            db.SaveChanges();
        }
        public BaseResponse SendMessage(long id,long dtlid, string name, string email, string message)
        {
            BaseResponse res = new BaseResponse();
            blog_message msg = new blog_message();
            msg.blogid = id;
            msg.blogdtlid = dtlid;
            msg.name = name;
            msg.email = email;
            msg.message = message;
            msg.create_date = DateTime.Now;
            db.blog_message.Add(msg);
            db.SaveChanges();
            return res;
        }
    }
}