﻿using System.Linq;
using System.Collections.Generic;

namespace web.Models
{
    public class f_csr_model : base_model
    {
        public List<csr_news> GetList(string lang, int year = 0, string title = "")
        {
            List<csr_news> list = new List<csr_news>();
            var query = from item in db.csr_news
                        where item.lang == lang
                        where item.launch == 1
                        where item.publish_date.Value.Year == year || year == 0
                        where item.title.Contains(title) || title == ""
                        orderby item.publish_date descending
                        select item;
            list = query.ToList();
            return list;
        }

        public csr_news GetLastNews(string lang)
        {
            csr_news data = new csr_news();
            var query = from item in db.csr_news
                        where item.lang == lang
                        where item.launch == 1
                        orderby item.publish_date descending
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new csr_news();
            }
            return data;
        }

        public List<int> GetNewsYear(string lang)
        {
            List<int> list = new List<int>();
            var query = from item in db.csr_news
                        where item.lang == lang
                        where item.launch == 1
                        group item by item.publish_date.Value.Year;
            foreach (var item in query)
            {
                foreach (var q in item)
                {
                    int year = q.publish_date.Value.Year;
                    if (list.Where(x=>x == year).Count() == 0)
                    {
                        list.Add(year);
                    }
                }
            }
            list = list.OrderByDescending(x => x).ToList();
            return list;
        }
        public csr_news GetData(long id)
        {
            csr_news data = new csr_news();
            var query = from item in db.csr_news
                        where item.id == id
                        where item.launch == 1
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new csr_news();
            }
            return data;
        }
    }
}
