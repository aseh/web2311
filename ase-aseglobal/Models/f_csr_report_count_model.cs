﻿using System.Linq;

namespace web.Models
{
    public class F_csr_report_count_model : base_model
    {
        public int SaveData(csr_report_count update_data)
        {
            csr_report_count data = new csr_report_count();
            if (update_data.id == 0)
            {
                db.csr_report_count.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.id);
                data = update_data;
            }
            return db.SaveChanges();
        }

        public csr_report_count GetData(long id)
        {
            csr_report_count data = new csr_report_count();
            var query = from item in db.csr_report_count
                        where item.id == id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new csr_report_count();
            }
            return data;
        }

    }
}
