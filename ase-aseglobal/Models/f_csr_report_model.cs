﻿using System.Linq;
using System.Collections.Generic;

namespace web.Models
{
    public class F_csr_report_model : base_model
    {
        public List<csr_report> GetList(string lang)
        {
            List<csr_report> list = new List<csr_report>();
            var query = from item in db.csr_report
                        where item.lang == lang
                        where item.launch == 1
                        orderby item.id descending
                        select item;
          
            list = query.ToList();
            return list;
        }

        public csr_report GetData(long id)
        {
            csr_report data = new csr_report();
            var query = from item in db.csr_report
                        where item.id == id
                        where item.launch == 1
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new csr_report();
            }
            return data;
        }
    }
}
