﻿namespace web.Models
{
    public class f_csr_report_questionnaire_model : base_model
    {
        public int SaveData(csr_report_questionnaire update_data)
        {
            csr_report_questionnaire data = new csr_report_questionnaire();
            db.csr_report_questionnaire.Add(update_data);
            return db.SaveChanges();
        }

    }
}
