﻿using System.Linq;
using System.Collections.Generic;
using System;

namespace web.Models
{
    public class F_event_model : base_model
    {
        public List<events> GetList(string lang, bool for_home = false, int month = 0, string title = "")
        {
            var date1 = DateTime.Now;
            List<events> list = new List<events>();
            var query = from item in db.events
                        where item.lang == lang
                        where item.launch == 1
                       where item.end_date.Value >= date1 || item.end_date.Value == null
                        where item.end_date.Value.Month == month || month == 0
                        where item.title.Contains(title) || title == ""
                        orderby item.start_date ascending
                        select item;
            list = query.ToList();
            if (for_home)
            {
                list = list.Count() > 3 ? list.Take(3).ToList() : list.Take(list.Count()).ToList();
            }
            return list;
        }

        public List<events> GetIrNews(string lang)
        {
            List<events> list = new List<events>();
            var query = from item in db.events
                        where item.lang == lang
                        where item.launch == 1
                        orderby item.start_date descending
                        select item;
            list = query.ToList();
            list = list.Count > 3 ? list.Take(3).ToList() : list.Take(list.Count).ToList();

            return list;
        }

        public events GetLastNews(string lang)
        {
            events data = new events();
            var query = from item in db.events
                        where item.lang == lang
                        where item.launch == 1
                        orderby item.start_date descending
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new events();
            }
            return data;
        }

        public List<int> GetEventMonth(string lang)
        {
            var date1 = DateTime.Now;
            List<int> list = new List<int>();
            var query = from item in db.events
                        where item.lang == lang
                        where item.launch == 1
                        where item.start_date.Value >= date1
                        group item by item.start_date.Value.Month;

            foreach (var item in query)
            {
                foreach (var q in item)
                {
                    int month = q.start_date.Value.Month;
                    if (list.Where(x=>x == month).Count() == 0)
                    {
                        list.Add(month);
                    }
                }
            }
            return list;
        }
    }
}
