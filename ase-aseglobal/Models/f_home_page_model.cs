﻿using System.Linq;
using System.Collections.Generic;

namespace web.Models
{
    public class F_home_page_model : base_model
    {
        public List<home_page> GetList(string lang)
        {
            List<home_page> list = new List<home_page>();
            var query = from item in db.home_page
                        where item.lang == lang
                        select item;
            list = query.OrderByDescending(o => o.update_date).ToList();
            return list;
        }
    }
}
