﻿using System.Linq;
using System.Collections.Generic;

namespace web.Models
{
    public class F_ir_news_model : base_model
    {
        public List<ir_news> GetList(string lang, int type = 0)
        {
            List<ir_news> list = new List<ir_news>();
            var query = from item in db.ir_news
                        where item.lang == lang
                        where item.type == type || type == 0
                        orderby item.start_date descending, item.end_date descending
                        select item;
            list = query.ToList();
            list = list.Count() > 3 ? list.Take(3).ToList() : list.Take(list.Count()).ToList();
            return list;
        }
    }
}
