﻿using System.Linq;
using System.Collections.Generic;

namespace web.Models
{
    public class F_language_model : base_model
    {
        public List<language> GetList(string lang)
        {
            List<language> list = new List<language>();
            var query = from item in db.language
                        where item.lang == lang
                        select item;
            list = query.ToList();
            return list;
        }

        public bool CheckLang(string lang)
        {
            language data = new language();
            var query = from item in db.language
                        where item.lang == lang
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                return false;
            }
            return true;
        }

        public language GetDefaultData()
        {
            language data = new language();
            var query = from item in db.language
                        where item.is_default == 1
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                query = from item in db.language
                        select item;
                data = query.FirstOrDefault();
            }
            return data;
        }
        public language GetData(string lang)
        {
            language data = new language();
            var query = from item in db.language
                        where item.lang == lang
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = GetDefaultData();
            }
            return data;
        }
    }
}
