﻿using System.Linq;
using System.Collections.Generic;

namespace web.Models
{
    public class f_press_room_model : base_model
    {
        public List<press_room> GetList(string lang, bool for_home = false, int year = 0, string title = "")
        {
            List<press_room> list = new List<press_room>();
            var query = from item in db.press_room
                        where item.lang == lang
                        where item.launch == 1
                        where item.publish_date.Value.Year == year || year == 0
                        where item.title.Contains(title) || title == ""
                        orderby item.publish_date descending
                        select item;
            list = query.ToList();
            if (for_home)
            {
                list = list.Count() > 3 ? list.Take(3).ToList() : list.Take(list.Count()).ToList();
            }
            return list;
        }

        public press_room GetLastNews(string lang)
        {
            press_room data = new press_room();
            var query = from item in db.press_room
                        where item.lang == lang
                        where item.launch == 1
                        orderby item.publish_date descending
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new press_room();
            }
            return data;
        }

        public List<int> GetNewsYear(string lang)
        {
            List<int> list = new List<int>();
            var query = from item in db.press_room
                        where item.lang == lang
                        where item.launch == 1
                        group item by item.publish_date.Value.Year;
            foreach (var item in query)
            {
                foreach (var q in item)
                {
                    int year = q.publish_date.Value.Year;
                    if (list.Where(x => x == year).Count() == 0)
                    {
                        list.Add(year);
                    }
                }
            }
            list = list.OrderByDescending(x => x).ToList();
            return list;
        }

        //public press_room GetData(long id)
        public press_room GetData(string urlid)
        {
            //press_room data = new press_room();
            //var query = from item in db.press_room
            //            where item.id == id
            //            where item.launch == 1
            //            select item;
            //data = query.FirstOrDefault();
            //if (data == null || data.id == 0)
            //{
            //    data = new press_room();
            //}
            //return data;
            press_room data = new press_room();
            var query = from item in db.press_room
                        where (item.urlid.Replace(" ", "-") == urlid || item.urlid == urlid)
                        where item.launch == 1
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new press_room();
            }

            return data;
        }
    }
}
