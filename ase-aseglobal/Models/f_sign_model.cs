﻿using System.Linq;
using System.Collections.Generic;

namespace web.Models
{
    public class F_sign_model : base_model
    {
        public sign GetData()
        {
            sign data = new sign();
            var query = from item in db.sign
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new sign();
            }
            return data;
        }

    }
}
