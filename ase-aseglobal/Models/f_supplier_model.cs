﻿using System.Linq;
using System.Collections.Generic;

namespace web.Models
{
    public class f_supplier_model : base_model
    {
        public List<supplier> GetList(string title = "")
        {
            List<supplier> list = new List<supplier>();
            var query = from item in db.supplier
                        where item.title.Contains(title) || title == ""
                        select item;
            list = query.ToList();
            return list;
        }
        
        public supplier GetData(long id)
        {
            supplier data = new supplier();
            var query = from item in db.supplier
                        where item.id == id
                        select item;
            data = query.FirstOrDefault();
            if (data == null || data.id == 0)
            {
                data = new supplier();
            }
            return data;
        }

        public int SaveData(supplier update_data)
        {
            supplier data = new supplier();
            if (update_data.id == 0)
            {
                db.supplier.Add(update_data);
            }
            else
            {
                data = this.GetData(update_data.id);
                data = update_data;
            }
            return db.SaveChanges();
        }

    }
}
