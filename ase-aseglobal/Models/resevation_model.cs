﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using web.ViewModels;
using Newtonsoft.Json;

namespace web.Models
{
    public class resevation_model: base_model
    {
        //取得當前已報名人數
        public int getNowEnrollCount()
        {
            var initialCount = Convert.ToInt32(ConfigurationManager.AppSettings["ResevationEnrollInitialStartCount"]);

            var dbSumCount = this.db.resevation.Where(x => x.date < DateTime.Now && x.status == 1).Sum(x => x.people_number);

            return initialCount + dbSumCount;
        }

        public setting getSetting()
        {
            return this.db.setting.FirstOrDefault();
        }

        public Dictionary<string, resevation_time_interval> getTimes()
        {
            var DbList = this.db.resevation_time_interval.Take(2).ToList();
            return new Dictionary<string, resevation_time_interval>() {
                { "上午", DbList[0] },
                { "下午", DbList[1] },
            };
        }

        public void addQuest(ResevationQuest Form)
        {
            var Obj = JsonConvert.SerializeObject(Form);
            var Quest = new resevation_quest();
            Quest.form = Obj;
            Quest.ip = HttpContext.Current.Request.UserHostAddress;
            Quest.createdAt = DateTime.Now;
            Quest.type = "GREEN_CENTER";

            db.resevation_quest.Add(Quest);
            db.SaveChanges();
        }

        public List<resevation_quest> GetQuestList()
        {
            return db.resevation_quest.ToList();
        }

        public ResevationQuest Parse(resevation_quest model)
        {
            return JsonConvert.DeserializeObject<ResevationQuest>(model.form);
        }
    }
}