//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class setting
    {
        public int id { get; set; }
        public int min_people { get; set; }
        public int limite_people { get; set; }
        public string resevation_desc { get; set; }
        public string mailContent_success { get; set; }
        public string mailCotent_fail { get; set; }
        public string mailCC { get; set; }
        public string mailContent_footer { get; set; }
        public System.DateTime create_date { get; set; }
        public System.DateTime update_date { get; set; }
    }
}
