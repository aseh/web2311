﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using web.Models;

using System.Web.Mvc;

namespace web.ViewModels.Blog
{
    public class DetailView: SearchForm
    {
        public blog Blog { get; set; }

        public blog NextBlog { get; set; }
        public blog PrevBlog { get; set; }
        public string Title { get; set; }
    }
}