﻿using System.Collections.Generic;
using System.Web.Mvc;
using web.Models;

namespace web.ViewModels.Blog
{
    public class IndexView: SearchForm
    {
        public string Title { get; set; }

        public blog_author Author { get; set; }

        public List<blog_dtl> TopList { get; set; }
        public List<blog_dtl> PageList { get; set; }

        public blog_type Type { get; set; }

        public string TypeName { get; set; }
        public string AuthorName { get; set; }
    }
}