﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace web.ViewModels.Blog
{
    public class SearchForm: Pageable
    {
        public string Keyword { get; set; }
        public int AuthorId { get; set; }
        public int TypeId { get; set; }

        public List<SelectListItem> AuthorList { get; set; }
        public List<SelectListItem> TypeList { get; set; }
    }
}