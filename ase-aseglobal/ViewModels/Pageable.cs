﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.ViewModels
{
    public class Pageable
    {
        public int Total { get; set; }
        public int PageSize { get; set;  }
        public int Page { get; set; }
    }
}