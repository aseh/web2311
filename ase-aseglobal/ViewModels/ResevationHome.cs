﻿using System;
using System.Collections.Generic;
using web.Models;

namespace web.ViewModels
{
    [Serializable]
    public class ResevationHome
    {
        public int EnrollCount { get; set; }
        public int MinCount { get; set; }
        public int MaxCount { get; set; }
        public string Mail { get; set; }
        public Dictionary<string, resevation_time_interval> Times { get; set; }

        public ResevationHome() { }
    }
}