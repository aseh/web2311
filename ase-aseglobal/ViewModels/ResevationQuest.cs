﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.ViewModels
{
    [Serializable]
    public class ResevationQuest
    {
        public string Name { get; set; }
        public string Org { get; set; }
        public string HowKnow { get; set; }
        public string HowKnowSite { get; set; }
        public string HowKnowNote { get; set; }
        public string Count { get; set; }
        public string Project { get; set; }
        public string ProjectOther { get; set; }
        public string ProjectA { get; set; }
        public string ProjectB { get; set; }
        public string ProjectC { get; set; }
        public string WaterMark { get; set; }
        public string ClassRank { get; set; }
        public string TeacherRank { get; set; }
        public List<string> Recycle { get; set; }
        public string RecycleOther { get; set; }
        public string RecycleNote { get; set; }
        public string Impression { get; set; }
        public string Recommend { get; set; }
        public string RecommendOther { get; set; }
        public string Contact { get; set; }
        public string Suggest { get; set; }

        public string _VCode { get; set; }
        public string _VCodeKey { get; set; }
    }
}