﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.ViewModels
{
    public class SiteNav
    {
        public SiteNav()
        {

        }
        
        public string Title { get; set; }
        public List<SiteNav> Children { get; set;  }
        public string Url { get; set; }
        public int No { get; set; }

    }

}