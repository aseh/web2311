﻿$(function () {

    function getModal(injectContentFunction) {
        var modalWrap = $('<div class="modal fade" tabindex="-1" role="dialog" />');
        var modalDialog = $('<div class="modal-dialog  modal-lg" role="document" style="top:50%; transform: translateY(-50%);" />');
        var modalContent = $('<div class="modal-content" />');
        var modalHeader = $('<div class="modal-header" />');
        var modalBody = $('<div class="modal-body" style="padding:0px" />');
        var modalTitle = document.createElement("h4");

        if (injectContentFunction != undefined && typeof (injectContentFunction) == "function") {
            injectContentFunction({
                wrap: modalWrap,
                dialog: modalDialog,
                content: modalContent,
                header: modalHeader,
                body: modalBody,
                title: modalTitle
            })
        }
        $(modalWrap).append(modalDialog);
        $(modalDialog).append(modalContent);
        //$(modalContent).append(modalHeader);
        $(modalContent).append(modalBody);

        return modalWrap;
    }


    $(".popupVideo").on("click", function (e) {
        e.preventDefault();
        var videoHref = $(this).attr("href");
        if (videoHref == undefined || videoHref == "" || videoHref == null) {
            return;
        }

        var modal = getModal(function (obj) {
            var videoFrame = $("<video width='100%' controls autoplay controlsList='nodownload' />");
            $(videoFrame).attr("src", videoHref);
            $(obj.body).append(videoFrame)
        });
        console.log(modal)
        $(document.body).append(modal)
        
        $(modal).modal({})
        $(modal).on("hidden.bs.modal", function () {
            $(modal).remove()
        })
    })


    document.body.oncontextmenu = function(e) {
        return e.target.tagName != "VIDEO"
    }
});