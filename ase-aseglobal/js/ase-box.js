﻿$(window).on("load", function () {
    
    if ($("#JssorSlider").length > 0) {
        var options = {
            $AutoPlay: true,
            $Idle: 9000,
            $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: [
                    [{ b: 0, d: 1000, x: 0, e: { x: 6 } }],
                    [{ b: 500, d: 1000, x: 0, e: { x: 6 } }],
                    [{ b: 1000, d: 1000, x: 0, e: { x: 6 } }],
                    [{ b: 0, d: 1000, o: 1 }]
                ]
            },
            $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: [
                    { $Duration: 800, x: 0.25, $Zoom: 1.5, $Easing: { $Left: $Jease$.$InWave, $Zoom: $Jease$.$InCubic }, $Opacity: 2, $ZIndex: -10, $Brother: { $Duration: 800, x: -0.25, $Zoom: 1.5, $Easing: { $Left: $Jease$.$InWave, $Zoom: $Jease$.$InCubic }, $Opacity: 2, $ZIndex: -10 } },
                    { $Duration: 1200, x: 0.6, $Zoom: 1, $Rotate: 1, $During: { $Left: [0.2, 0.8], $Zoom: [0.2, 0.8], $Rotate: [0.2, 0.8] }, $Opacity: 2, $Round: { $Rotate: 0.5 } },
                    { $Duration: 1200, x: 0.2, y: -0.1, $Delay: 16, $Cols: 10, $Rows: 5, $Clip: 15, $During: { $Left: [0.3, 0.7], $Top: [0.3, 0.7] }, $Formation: $JssorSlideshowFormations$.$FormationStraightStairs, $Easing: { $Left: $Jease$.$InWave, $Top: $Jease$.$InWave, $Clip: $Jease$.$OutQuad }, $Opacity: 2, $Assembly: 260, $Round: { $Left: 1.3, $Top: 2.5 } }
                ],

                $TransitionsOrder: 1,
                // $ShowLink: true
            },
            $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
            },
        };


        function resetSize() {
            var width = $(window).width()
            var maxHeight = $(window).height() - 100
            $(".JssorRwdSize").css({
                width: width,
                height: Math.min(width <= 768 ? width * (1024 / 900) : width * (900 / 1920), maxHeight)
            })

        }

        function getImgUrl(img) {
            var width = $(window).width()
            var pcUrl = $(img).attr('data-pc-img')
            var mobUrl = $(img).attr('data-mobile-img')
            var pcThumb = $(img).attr('data-pc-thumb')
            var mobThumb = $(img).attr('data-mobile-thumb')
            if (width <= 767) {
                return {
                    url: mobUrl,
                    thumb: mobThumb
                }
            } else {
                return {
                    url: pcUrl,
                    thumb: pcThumb
                }
            }
        }

        function resetImages() {
            var imgs = $("#JssorSlider img")
            imgs.each(function (idx) {
                var img = imgs[idx]
                var urls = getImgUrl(img)
                if (urls.url.indexOf('.mp4') !== -1) {
                    $(img).attr("src", urls.thumb)
                } else {
                    $(img).attr("src", urls.url)
                }
            })
        }

        resetImages();
        resetSize();

        var slideing = false
        function createSlideVideo(index) {
            var videos = $("#JssorSlider video")
            if (videos.length > 0) {
                $("#JssorSlider video").remove()
            }
            var img = $("#banner-img-" + index)
            var urls = getImgUrl(img)
            if (urls.thumb !== '') {
                var parentContainer = $(img).parent()
                var video = $("<video muted loop playsinline autoplay />")
                $(video).attr({
                    src: urls.url,
                })
                $(video).css({
                    width: '100%'
                })
                $(video).on('canplaythrough', function () {
                    setTimeout(function(){
                        this.play()
                    }, 100)
                })
                $(parentContainer).append(video)
            }
        }


        var currentWidth = $(window).width()
        function resetSlider() {
            const newWidth = $(window).width()
            if (jssor_1_slider !== null && currentWidth !== newWidth) {
                jssor_1_slider.$Destroy()
                jssor_1_slider = null
                $("#JssorSlider").html(originalHtml)
                resetSize();
                jssor_1_slider = new $JssorSlider$("JssorSlider", options);
                currentWidth = newWidth
                createSlideVideo(0)
            }
        }
        var originalHtml = $("#JssorSlider").html()
        var jssor_1_slider = new $JssorSlider$("JssorSlider", options);
        createSlideVideo(0)

        jssor_1_slider.$On($JssorSlider$.$EVT_PARK, function (slideIndex, fromIndex) {
            if (!slideing) {
                createSlideVideo(slideIndex)
            }
        });
        jssor_1_slider.$On($JssorSlider$.$EVT_SLIDESHOW_START, function (slideIndex, progress, progressBegin, slideshowBegin, slideshowEnd, progressEnd) {
            slideing = true
        });
        jssor_1_slider.$On($JssorSlider$.$EVT_SLIDESHOW_END, function (slideIndex, progress, progressBegin, slideshowBegin, slideshowEnd, progressEnd) {
            slideing = false
            createSlideVideo(slideIndex)
        });
        //Scale slider after document ready
        // ScaleSlider();

        //Scale slider while window load/resize/orientationchange.
        $(window).bind("resize", resetSlider);
    // $(window).bind("orientationchange", resetSlider);
    //Set All Link Active
    }
    
    function applyElements(eles, text) {
        console.log(eles)
        eles.each(function (idx) {
            
            var eleHref = $(eles[idx]).attr('href')
            var eleTitle = $(eles[idx]).attr('data-title')
            
            if (eleHref == text && eleHref !== '') {
                $(eles[idx]).addClass('active')
            } else if (eleTitle === text && eleTitle !== '' && eleHref === undefined) {
                $(eles[idx]).addClass('active')
            }
        })
    }
    function addLinkActive(href) {
        applyElements($('a'), href)
        applyElements($('div'), href)
    }

    var pathname = window.location.pathname+window.location.search
    addLinkActive(pathname)
    var breadcrumbs = $('#breadcrumbs')
    if (breadcrumbs.length > 0) {
        var links = $('#breadcrumbs a')
        if (links.length > 0) {
            links.each(function (idx) {
                addLinkActive($(links[idx]).attr('href'))
                addLinkActive($(links[idx]).text())
            })
        }
    }

    var currentScrollTop = 0
    $(window).on('scroll', function () {
        var header = $('.ASEHeader')[0]
        var scrollY = window.pageYOffset
        if (scrollY > currentScrollTop + 10 && scrollY > 60) {
            currentScrollTop = scrollY
            $(header).addClass('scrollHide');
        } else if (scrollY < currentScrollTop - 10) {
            currentScrollTop = scrollY
            $(header).removeClass('scrollHide');
        }

        //var bread = $(breadcrumbs)[0]
        //var rect = bread.parentElement.getBoundingClientRect()

        //if (rect.y < 60) {
        //  $(bread).addClass('sticky')
        //} else {
        //  $(bread).removeClass('sticky')
        //}
    })

})