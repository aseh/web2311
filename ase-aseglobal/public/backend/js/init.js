﻿$(function () {
    // $(document).pjax('a', 'body');
    // $(document).on('pjax:start', function() {
    //     NProgress.start();
    // });
    // $(document).on('pjax:end', function() {
    //     NProgress.done();
    // });
    //防止表單重覆送出
    $("form").submit(function () {
        $(this).find("[type=submit]").prop("disabled", true);
    });

    //側邊選單
    $(".treeview-menu").find("li.active > a").addClass("text-orange").parents("li.treeview").addClass("active");
    //開關按鈕初始化
    if ($(".switch").length > 0) {
        $(".switch").find("input[type='checkbox']").bootstrapSwitch();
    }
    
    //刪除資料特效
    $(".delete_btn").on("click", function (e, state) {
        var _self = $(this);
        if (!state) {
            e.preventDefault();
            swal({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#008d4c",
                cancelButtonColor: "#dd4b39",
                confirmButtonText: "Yes"
            }).then(function () {
                _self.trigger("click", [true]);
            });
        }
    });
    //reset password
    $(".confirm_btn").on("click", function (e, state) {
        var _self = $(this);
        if (!state) {
            e.preventDefault();
            swal({
                title: "Are you sure?",
                // text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#008d4c",
                cancelButtonColor: "#dd4b39",
                confirmButtonText: "Yes"
            }).then(function () {
                _self.trigger("click", [true]);
            });
        }
    });

    //日期選擇
    if ($(".date_picker").length > 0) {
        $(".date_picker").datetimepicker({
            format: 'YYYY-MM-DD',
        });
    }

    //iCheck for checkbox and radio inputs
    if ($(".iCheck").length > 0) {
        $(".iCheck").find("input[type='checkbox'], input[type='radio']").iCheck({
            checkboxClass: "icheckbox_flat-red",
            radioClass: "iradio_flat-red",
            increaseArea: "20%" // optional
        });
    }

    if ($(".jqueryFileTree").length > 0) {
        $('.jqueryFileTree').on('click', 'a', function (e) {
            e.preventDefault();
        });
    }

    
    // window.addEventListener('resize', onResize, false);
    // onResize();
});

// function onResize() {
//     var w = window.innerWidth;
//     var h = window.innerHeight;
//     var sourceWidth = 640;
//     var sourceHeight = 400;
//     stage.scaleX = w /sourceWidth;
//     stage.scaleY = stage.scaleX;
//     stage.canvas.width = w;
//     stage.canvas.height = h;
// }

function areyousure(str) {
    if (confirm("Are you sure that you want to " + str + " ?")) {
        return true;
    }
    return state;
}

var urlobj;
function browseServer(obj) {
    urlobj = obj;
    OpenServerBrowser("/filemanager/index.html", screen.width * 0.7, screen.height * 0.7);
}

function OpenServerBrowser(url, width, height) {
    var i_left = (screen.width - width) / 2;
    var i_top = (screen.height - height) / 2;
    var options = "toolbar=no,status=no,resizable=yes,dependent=yes";
    options += ",width=" + width;
    options += ",height=" + height;
    options += ",left=" + i_left;
    options += ",top=" + i_top;
    var oWindow = window.open(url, "BrowseWindow", options);
}

function SetUrl(url, width, height, alt) {
    if ($("#" + urlobj + "_file").length > 0) {
        var arr = url.split("/");
        $("#" + urlobj + "_file").text(arr[arr.length - 1]).prop("href", url).show();
    } else {
        $("#" + urlobj + "_img").prop("src", url).show();
    }
    $("#delete-" + urlobj).css("display", "inline-block");
    document.getElementById(urlobj).value = url;
    oWindow = null;
}
// $(window).load(function(){
//     NProgress.done();
// });

tinymce.init({
    selector: '.tinymce',
    height: 300,
    menubar: false,
    plugins: 'visualblocks',
    plugins: [
        'advlist autolink lists link image charmap print preview anchor textcolor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code help wordcount'
    ],
    toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
    toolbar2: "cut copy paste | bullist numlist | outdent indent | link anchor image media code | insertdatetime | removeformat | subscript superscript | help",
    content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'],
    style_formats: [
        {
            title: 'Headers', items: [
                { title: 'h1', block: 'h1' },
                { title: 'h2', block: 'h2' },
                { title: 'h3', block: 'h3' },
                { title: 'h4', block: 'h4' },
                { title: 'h5', block: 'h5' },
                { title: 'h6', block: 'h6' }
            ]
        },

        {
            title: 'Blocks', items: [
                { title: 'p', block: 'p' },
                { title: 'div', block: 'div' },
                { title: 'pre', block: 'pre' }
            ]
        },

        {
            title: 'Containers', items: [
                { title: 'section', block: 'section', wrapper: true, merge_siblings: false },
                { title: 'article', block: 'article', wrapper: true, merge_siblings: false },
                { title: 'blockquote', block: 'blockquote', wrapper: true },
                { title: 'hgroup', block: 'hgroup', wrapper: true },
                { title: 'aside', block: 'aside', wrapper: true },
                { title: 'figure', block: 'figure', wrapper: true }
            ]
        }
    ],
    visualblocks_default_state: false,
    end_container_on_empty_block: true,
    font_formats: '微軟正黑體=Microsoft JhengHei;Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;calibri=calibri;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats'
});